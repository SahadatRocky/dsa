#include<bits/stdc++.h>
using namespace std;

bool divisorGame(int n) {
    return n%2==0;
}


int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n = 2;
    cout<<divisorGame(n)<<endl;

    return 0; 
}

/*

Input: n = 2
Output: true
Explanation: Alice chooses 1, and Bob has no more moves.

*/