#include<bits/stdc++.h>
using namespace std;

string solve(int n,int a,int b, vector<int> A)
{
    int cnt1=0,cnt2=0,cnt3=0;
    for(int i=0;i<n;i++)
    {
        if(A[i]%a==0 && A[i]%b==0)
        {
            cnt1++;
        }
        else
        if(A[i]%a==0){
            cnt2++;
        }
        else
        if(A[i]%b==0){
            cnt3++;
        }
    }
    if(cnt1)
    {
        cnt2++;
    }
    if(cnt2>cnt3)
    {
        return "Bob";
    }
    else{
        return "Alice";
    }
}

int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n = 5,a = 3,b = 2;
    vector<int> A{1, 2, 3, 4, 5};

    cout<<solve(n,a,b,A)<<endl;

    return 0; 
}

/*

Input

n = 5, a = 3, b = 2 
A= [1, 2, 3, 4, 5]
Output

Alice
Explanation:

Bob removes 3 and the sequence becomes [1,2,4,5]. Then, Alice removes 2 and the sequence becomes [1,4,5]. Now, Bob is left with no moves, so Alice is the winner.

*/