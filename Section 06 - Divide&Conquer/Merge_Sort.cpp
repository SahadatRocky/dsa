
#include<bits/stdc++.h>
using namespace std;

void merge(int arr[],int s,int e){
      
      int i = s;
      int mid = (s+e)/2;
      int j = mid + 1;
      
      vector<int> v;

      while(i<=mid && j<=e){
          
          if(arr[i] < arr[j]){
            v.push_back(arr[i++]);
          }else{
            v.push_back(arr[j++]);
          }
      }

      while(i<=mid){
        v.push_back(arr[i++]);
      }

      while(j<=e){
        v.push_back(arr[j++]);
      }

     int k =0;
     for(int i=s; i<=e; i++){
        arr[i] = v[k];
        k++;
     }
}


void mergeSort(int arr[],int s,int e){
     
     if(s>=e){
        return;
     }

     int mid = (s+e)/2;
     mergeSort(arr,s,mid);
     mergeSort(arr,mid+1,e);

     merge(arr,s,e);
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int arr[] = {10,5,2,0,7,6,4};
    int n = sizeof(arr)/sizeof(int);
    
    int s = 0;
    int e = n - 1;

    mergeSort(arr,s,e);

    for(int i=0; i<n; i++){
    	cout<<arr[i]<<" ";
    }
    cout<<endl;


	return 0;
}
