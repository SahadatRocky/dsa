#include<bits/stdc++.h>
using namespace std;

vector<int> solve(int n,int m, vector<vector<int>> arr)
{
    vector<vector<int>>v(n+1);
    vector<int>in(n+1);
    int x,y;
    for(int i=0;i<m;i++)
    {
        x=arr[i][0],y=arr[i][1];
        v[x].push_back(y);
        v[y].push_back(x);
        in[y]++;
    }

    set<int>s;
    vector<int>topo;

    for(int i=1;i<=n;i++)
    {
        if(in[i]==0)
        {
            s.insert(i);
        }
    }

    while(s.size())
    {
        x = *s.begin();
        s.erase(x);
        topo.push_back(x);

        for(auto itr : v[x])
        {
            in[itr]--;
            if(in[itr] == 0)
            {
                s.insert(itr);
            }
        }
    }

    vector<int>ans;

    if(topo.size()==n)
    {
        ans=topo;
    }
    return ans;
}


int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n = 4 , m =3;
    vector<vector<int>> arr = {{2, 1}, {3, 4}, {2, 4}};

    vector<int> output = solve( n, m, arr);

    for(auto x: output){
    	cout<<x<<" ";
    }
    cout<<endl;


    return 0; 
}


/*

Input

n = 4 , m =3, arr={{2, 1}, {3, 4}, {2, 4}}
Output

{2, 1, 3, 4}
Explanation

The following five permutations P satisfy the condition: (2, 1, 3, 4), (2, 3, 1, 4), (2, 3, 4, 1), (3, 2, 1, 4), (3, 2, 4, 1). The lexicographically smallest among them is (2, 1, 3, 4).

Example 2:

Input

n = 2 , m =3, arr={{1, 2}, {1, 2}, {2, 1}}
Output

{ }
Explanation

No permutations P satisfy the condition.

*/