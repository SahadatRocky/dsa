#include<bits/stdc++.h>
using namespace std;

#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007 

int gameRoutes(int n, vector<vector<int>> teleporters)
{
    long long x,y;
    vector<vector<int>>v(n+1);
    vector<int>ind(n+1);
    vector<long long>dp(n+1);
    for(int i=0;i<teleporters.size();i++)
    {
        x=teleporters[i][0],y=teleporters[i][1];
        v[x].push_back(y);
        ind[y]++;
    }
    queue<int>q;
    vector<int>topo;
    for(int i=1;i<=n;i++)
    {
        if(ind[i]==0)
        {
            q.push(i);
        }
    }
    while(q.size())
    {
        x=q.front();
        q.pop();
        topo.push_back(x);
        for(auto itr:v[x])
        {
            ind[itr]--;
            if(ind[itr]==0)
            {
                q.push(itr);
            }
        }
    }
    dp[1]=1;
    for(auto itr:topo)
    {
        for(auto it:v[itr])
        {
            dp[it]=(dp[it]+dp[itr])%mod;
        }
    }
    return dp[n];
}


int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n = 5;
    vector<vector<int> > teleporters = {{1,  2}, {1, 3}, {2, 3}, {1,  4}, {4, 5}};
    
    cout<<gameRoutes(n,teleporters)<<endl;

    return 0; 
}

/*

Input : n = 5, teleporters = {{1,  2}, {1, 3}, {2, 3}, {1,  4}, {4, 5}}
Output : 1

*/