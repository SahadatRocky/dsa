
#include<bits/stdc++.h>
#define ll long long int
using namespace std;

void powerOfFour(int n){
    if((n & (n - 1)) == 0 && n%4 == 0){
        cout<<"it's a power of 4"<<endl;
    }else{
        cout<<"it's not a power of 4"<<endl;
    }
}

 bool isPowerOfFour(int n) {
        if(n==1)
            return true;
        if(n==0 || n%4 != 0){
            return false;
        }
        return isPowerOfFour(n/4);
    }


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    

    ll n;
    cin>>n;
    powerOfFour(n);
    
	return 0;
}