

#include<bits/stdc++.h>
#define ll long long int
using namespace std;


int concatenatedBinary(int n){
   
   int mod = 1e9+7;
   int len = 0;
   ll ans = 0;
   for(int i=1; i<=n; i++){
       
       if( (i & (i-1)) == 0){
           len++;
       }

      ans = ((ans<<len) + i)% mod;
   } 
   
   return ans;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    

    int n;
    cin>>n;

    cout<<"concatenatedBinary: "<<concatenatedBinary(n)<<endl;

	return 0;
}

/*

Input: n = 3
Output: 27
Explanation: In binary, 1, 2, and 3 corresponds to "1", "10", and "11".
After concatenating them, we have "11011", which corresponds to the decimal value 27.

*/