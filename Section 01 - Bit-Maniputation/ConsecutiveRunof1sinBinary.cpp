#include<bits/stdc++.h>
#define ll long long int
using namespace std;


int longestConsecutive1sinBinary(int n){
     
     int count = 0;

     while(n>0){
        
        n = n & (n>>1); 
        count++; 
     }

     return count;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    

    ll n;
    cin>>n;
    cout<<"longestConsecutive1sinBinary: "<<longestConsecutive1sinBinary(n)<<endl;

	return 0;
}

/*
input
11

output
2

*/