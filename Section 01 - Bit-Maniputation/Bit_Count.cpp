

#include<bits/stdc++.h>
#define ll long long int
using namespace std;

int bitCount(int n){
    int count = 0;
    while(n>0){
       
       int lastSetBit = (n&1);
       if(lastSetBit){
          count++;
       }
       n = n>>1;
    }

    return count;
}


int bitHack(int n){
    int count = 0;
    while(n>0){
       n = (n & (n - 1)); // Remove the last set bit
       count++;
    }
    
    return count;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    ll n;
    cin>>n;
    cout<<"bitCount:"<<bitCount(n)<<endl;
    cout<<"bithack:"<<bitHack(n)<<endl;
    

	return 0;
}


/*

input 
15

output 4

*/