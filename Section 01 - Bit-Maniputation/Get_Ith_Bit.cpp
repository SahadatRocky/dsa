
#include<bits/stdc++.h>
#define ll long long int
using namespace std;

int getIthBit(int n,int i){
    
    int mask = (1<<i);
    n = (n&mask);
    return (n> 0) ? 1 : 0;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    

    ll n;
    int i;
    cin>>n>>i;
    cout<<"getIthBit: "<<getIthBit(n,i)<<endl;
	return 0;
}

/*
input
15 2
output
1 


n = 100  
4
*/