
#include<bits/stdc++.h>
#define ll long long int
using namespace std;

void powerOfTwo(int n){
    if((n & (n - 1)) == 0){
        cout<<"it's a power of 2"<<endl;
    }else{
        cout<<"it's not a power of 2"<<endl;
    }
}

 bool isPowerOfTwo(int n) {
        if(n==1)
            return true;
        if(n==0 || n%2 != 0){
            return false;
        }
        return isPowerOfFour(n/2);
    }

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    

    ll n;
    cin>>n;
    powerOfTwo(n);

	return 0;
}
