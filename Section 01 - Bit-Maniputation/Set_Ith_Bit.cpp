
#include<bits/stdc++.h>
#define ll long long int
using namespace std;

int setIthBit(int n, int i){
   
   int mask = (1<<i);
   n = (n|mask);
   return n; 
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    

    ll n;
    int i;
    cin>>n>>i;
    cout<<"setIthBit: "<<setIthBit(n,i)<<endl;
	return 0;
}

/*
input
8 2
output
12
*/