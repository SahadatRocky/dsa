

#include<bits/stdc++.h>
#define ll long long int
using namespace std;


int decimalToConvertBinary(int n){
    
    int p = 1;
    int ans = 0;
    while(n>0){
        
      int lastSetBit = n&1;
      ans = ans + p * lastSetBit;
      p = p * 10;
      n = n>>1;
    }

   return ans;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    ll n;
    cin>>n;
    cout<<"decimalToConvertBinary:"<<decimalToConvertBinary(n)<<endl;

	return 0;
}

/*

input
15

output
1111

*/