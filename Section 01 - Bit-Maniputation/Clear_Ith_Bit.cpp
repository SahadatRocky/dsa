
#include<bits/stdc++.h>
#define ll long long int
using namespace std;

int clearIthBit(int n,int i){
    
    int mask = ~(1<<i);
    n = n&mask;
    return n;
}



int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    

    ll n;
    int i;
    cin>>n>>i;
    cout<<"clearIthBit: "<<clearIthBit(n,i)<<endl;
	return 0;
}

/*

input
13 2
output
9

*/