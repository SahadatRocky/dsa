
#include<bits/stdc++.h>
#define ll long long int
using namespace std;


void overlay(char arr[],int number){
     int j = 0;
     while(number > 0){
          
          int setBit = number&1;
          if(setBit){
             cout<<arr[j]; 
          }

          number = number>>1;
          j++; 
     }
     cout<<endl;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    

    char arr[100];
    cin>>arr;
    int n = strlen(arr);
    for(int i=0; i<(1<<n); i++){
        overlay(arr,i);
    }

	return 0;
}