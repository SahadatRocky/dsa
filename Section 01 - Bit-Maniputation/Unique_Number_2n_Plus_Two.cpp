

#include<bits/stdc++.h>
#define ll long long int
using namespace std;

pair<int,int> uniqueNumber2nPlusTwo(vector<int>& arr){
    pair<int,int> p;
    int xorr = 0;

    for(int i=0; i<arr.size(); i++){
        xorr = xorr ^ arr[i];
    }
    cout<<xorr<<endl; 
    int setBit = (xorr & ~(xorr - 1));
    int x = 0;
    int y = 0;
    for(int i=0; i<arr.size(); i++){
        if(arr[i] & setBit){
            x = x ^ arr[i];
        }else{
            y = y ^ arr[i];
        } 
    }

    p.first = x;
    p.second = y;

    return p;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> arr;
    int n;
    cin>>n;
    for(int i=0; i<n; i++){
        int data;
        cin>>data;
        arr.push_back(data);
    }

    pair<int,int> p = uniqueNumber2nPlusTwo(arr);


    
    cout<<"uniqueNumber2nPlusTwo:"<<p.first<<" "<<p.second<<endl;

	return 0;
}


/*
8
2 3 7 9 11 2 3 11

*/