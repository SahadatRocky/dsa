

#include<bits/stdc++.h>
#define ll long long int
using namespace std;

int binaryToConvertDecimal(int n){
    
    int p = 1;
    int ans = 0;
    
    while(n>0){
        int rem = n%10;
        ans = ans + p*rem;
        p = p * 2; 
        n = n / 10;
    }
    return ans;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    ll n = 1111;
    cin>>n;
    cout<<"binaryToConvertDecimal:"<<binaryToConvertDecimal(n)<<endl;

	return 0;
}

/*

input
1111
output
15

*/