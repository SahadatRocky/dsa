
/*
 you are given 32-bit numbers, N & M and two bit positions i and j.
 write a method to set all the bits between i and j in N equal to M.

Example:
N = 10000000000
M = 10101
i = 2
j = 6 

output: 1001010100
*/


#include<bits/stdc++.h>
#define ll long long int
using namespace std;

int clearBitInRange(int n,int i,int j){
    
    int a = (-1<<j+1);
    int b = (1<<i) - 1;

    int mask  = a|b;
    n = n & mask;
    return n;

}


int replaceIthBit(int n,int m,int i,int j){
     
    n =  clearBitInRange(n,i,j);

    int mask = (m<<i);

    n = n | mask;

    return n;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    

    ll n,m;
    int i,j;
    cin>>n>>m>>i>>j;
    cout<<"replaceIthBit: "<<replaceIthBit(n,m,i,j)<<endl;

	return 0;
}

/*

Example:
N = 10000000000
M = 10101
i = 2
j = 6 

output: 1001010100
*/

*/