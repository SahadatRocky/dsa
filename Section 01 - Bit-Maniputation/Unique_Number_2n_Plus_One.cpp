

#include<bits/stdc++.h>
#define ll long long int
using namespace std;

int uniqueNumber2nPlusOne(vector<int>& arr){
    
    int xorr = 0;

    for(int i=0; i<arr.size(); i++){
        xorr = xorr ^ arr[i];
    } 

    return xorr;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> arr;
    int n;
    cin>>n;
    for(int i=0; i<n; i++){
        int data;
        cin>>data;
        arr.push_back(data);
    }
    
    cout<<"uniqueNumber2nPlusOne:"<<uniqueNumber2nPlusOne(arr)<<endl;

	return 0;
}


/*
9
1 3 5 2 4 5 3 1 2
*/