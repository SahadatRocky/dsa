#include<bits/stdc++.h>
using namespace std;

vector<int> sortByBits(vector<int>& arr) {
    
    priority_queue<pair<int,int>> pq;
    
    for(int i=0; i<arr.size(); i++){
        int n = arr[i];
        int temp = arr[i];
        int cnt =0;

        while(n!=0){
        	int  lastSetBit = n&1;
        	cnt += lastSetBit;
        	n = n >> 1;
        }

        pq.push({cnt, temp}); 
    }

    vector<int> v;
    while(!pq.empty()){
    	int f = pq.top().second;
    	pq.pop();
    	v.push_back(f);
    }

    reverse(v.begin(), v.end());
    return v;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> arr{0,1,2,3,4,5,6,7,8};

    vector<int> output = sortByBits(arr);
    
    for(auto x: output){
    	cout<<x<<" ";
    }
    cout<<endl;

	return 0;
}


/*

Input: arr = [0,1,2,3,4,5,6,7,8]
 
Output: [0,1,2,4,8,3,5,6,7]
 
Explantion: [0] is the only integer with 0 bits.
[1,2,4,8] all have 1 bit.
[3,5,6] have 2 bits.
[7] has 3 bits.
The sorted array by bits is [0,1,2,4,8,3,5,6,7]


*/