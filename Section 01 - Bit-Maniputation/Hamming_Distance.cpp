#include<bits/stdc++.h>
using namespace std;

int hammingDistance(int x, int y){
   
   int result = x^y;
   int cnt = 0;

   while(result!=0){
      int lastSetBit = result&1;
      cnt += lastSetBit;
      result = result >> 1;
   }

   return cnt;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int x = 1, y = 4;
    cout<<hammingDistance(x,y)<<endl;

	return 0;
}


/*

Input: x = 1, y = 4
Output: 2
Explanation:
1   (0 0 0 1)
4   (0 1 0 0)
       ↑   ↑
The above arrows point to positions where the corresponding bits are different.

*/