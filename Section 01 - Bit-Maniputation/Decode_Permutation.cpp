#include<bits/stdc++.h>
using namespace std;

vector<int> decode(vector<int> encoded) {
    int n=encoded.size()+1, x=0;
    vector<int> ans;
    for(int i=1; i<=n; i++){
    	x = x ^ i;
    }
            
    for(int i=1; i<encoded.size(); i+=2){
    	x= x ^ encoded[i];
    }
            
    ans.push_back(x);
    for(int i=0; i<encoded.size(); i++){
    	ans.push_back(ans.back()^encoded[i]);
    }        
    return ans;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<int> encoded{3,1};
    vector<int> output = decode(encoded);
    
    for(auto x : output){
    	cout<<x<<" ";
    }
    cout<<endl;
    

	return 0;
}

/*

Input: encoded = [3,1]
 
Output: [1,2,3]
 
Explanation: If perm = [1,2,3], then encoded = [1 XOR 2,2 XOR 3] = [3,1]

*/