
#include<bits/stdc++.h>
#define ll long long int
using namespace std;

int clearIthBit(int n,int i){
    
    int mask = ~(1<<i);
    n = n & mask;
    return n;

}


int updateIthBit(int n,int i,int val){
   
   n = clearIthBit(n,i);

   int mask = (val<<i);

   n = n |mask;

   return n;

}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    ll n,i,val;
    cin>>n>>i>>val;
    cout<<"updateIthBit:"<<updateIthBit(n,i,val)<<endl;

	return 0;
}
