#include<bits/stdc++.h>
using namespace std;

int solve(int n) {
   
   int cnt = 0;

   while(n!=0){
       
      n = n & n>>1;
      cnt++; 
   }

   return cnt++;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int n = 156;
    cout<<solve(n)<<endl;

	return 0;
}


/*

Input

n = 156
Output

3
Explanation

156 is 10011100 in binary and there's a run of length 3.

*/