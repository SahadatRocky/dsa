
#include<bits/stdc++.h>
#define ll long long int
using namespace std;

int clearLastIthBit(int n,int i){
    
    int mask = (-1<<i);

    n = n & mask;

    return n;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    ll n,i;
    cin>>n>>i;
    cout<<"clearLastIthBit:"<<clearLastIthBit(n,i)<<endl;

	return 0;
}


/*

input
15 2

output
12

*/