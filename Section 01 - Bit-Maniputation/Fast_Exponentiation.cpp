

#include<bits/stdc++.h>
#define ll long long int
using namespace std;


int fastExponentiation(int a,int n){
     
     int ans = 1;
     
     while(n>0){
        
        int lastSetBit = n&1;

        if(lastSetBit){
           ans = ans * a;
        } 

        a = a*a;
        n = n>>1;
     }

     return ans;
}

int fastExponentiationRecurtion(int a,int n){
   
   if(n == 0){
    return 1;
   }
   return a * fastExponentiationRecurtion(a, n-1);
}



int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    ll a,n;
    cin>>n>>a;
    cout<<"fastExponentiation:"<<fastExponentiation(a,n)<<endl;
    cout<<"fastExponentiationRecurtion:"<<fastExponentiationRecurtion(a,n)<<endl;
	return 0;
}

/*

input
2 3

output 
8

*/