#include<bits/stdc++.h>
using namespace std;

int countTriplets(vector<int>& A){
   
    unordered_map<int,int> m;

    for(auto x : A){
    	for(auto y : A){
    		m[x&y]++;
    	}
    }

    int cnt = 0;
    for(auto a : A){
    	for(auto t : m){
           // If bitwise AND of triplet zero, increment cnt
           if( (t.first & a) == 0){
           	cnt += t.second;
           }
    	}
    }
   return cnt;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> A = { 2, 1, 3 };
    cout<<countTriplets(A)<<endl;

	return 0;
}

/*

Input: nums = [2,1,3]
 
Output: 12
 
Explanation: We could choose the following i, j, k triples:
(i=0, j=0, k=1) : 2 & 2 & 1
(i=0, j=1, k=0) : 2 & 1 & 2
(i=0, j=1, k=1) : 2 & 1 & 1
(i=0, j=1, k=2) : 2 & 1 & 3
(i=0, j=2, k=1) : 2 & 3 & 1
(i=1, j=0, k=0) : 1 & 2 & 2
(i=1, j=0, k=1) : 1 & 2 & 1
(i=1, j=0, k=2) : 1 & 2 & 3
(i=1, j=1, k=0) : 1 & 1 & 2
(i=1, j=2, k=0) : 1 & 3 & 2
(i=2, j=0, k=1) : 3 & 2 & 1
(i=2, j=1, k=0) : 3 & 1 & 2
*/