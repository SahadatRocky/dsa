#include<bits/stdc++.h>
using namespace std;

int minTaps(int n, vector<int>& ranges) {
    vector<pair<int,int>>v;
    for(int i=0;i<=n;i++)
    {
        v.push_back({i-ranges[i],i+ranges[i]});
    }
    sort(v.begin(),v.end());
    int s=0,e=0,ind=0;
    int ans=0;
    while(s<n){
        while(ind<v.size() && v[ind].first<=s){
            e=max(e,v[ind].second);
            ind++;
        }
        if(s==e){
            return -1;
        }
        s=e;
        ans++;
    }
    return ans;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n= 5;
    vector<int> ranges{3,4,1,1,0,0};

    cout<<minTaps(n, ranges)<<endl;
    
	return 0;
}

/*

Input: n = 5, ranges = [3,4,1,1,0,0]
 
Output: 1
 
Explanation: The tap at point 0 can cover the interval [-3,3]
The tap at point 1 can cover the interval [-3,5]
The tap at point 2 can cover the interval [1,3]
The tap at point 3 can cover the interval [2,4]
The tap at point 4 can cover the interval [4,4]


*/