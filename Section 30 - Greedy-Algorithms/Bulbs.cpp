#include<bits/stdc++.h>
using namespace std;

int solve(int n,vector<int>& arr){
    
    int turn = 0;
    int ans = 0; 
    
    for(int i=0; i<n; i++){

    	if(turn == 0 && arr[i] == 0){
    		turn ^= 1;
    		ans++;
    	}
    	if(turn == 1 && arr[i] == 1){
    		turn ^= 1;
    		ans++; 
    	}
    }
    return ans; 
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n= 4;
    vector<int> arr{0,1,0,1};

    cout<<solve(n,arr)<<endl;
    
	return 0;
}

/*

Input: n= 4, a= [0, 1, 0, 1]
 
Output: 4
 
Explanation:
press switch 0 : [1 0 1 0]
press switch 1 : [1 1 0 1]
press switch 2 : [1 1 1 0]
press switch 3 : [1 1 1 1]


*/
