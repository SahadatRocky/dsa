#include<bits/stdc++.h>
using namespace std;

vector<pair<int,int>> gr[100];
int N,E;
int dijkstra(int src, int dest){
    
    priority_queue<
        pair<int,int>, 
        vector<pair<int,int> >, 
        greater<pair<int,int> > > pq;
    
    vector<int> dist(N, INT_MAX);

    dist[src] = 0;
    pq.push({0, src});


    while (!pq.empty()) {

        int node = pq.top().second;
        int initial_weight = pq.top().first; 
        pq.pop();

        for(auto nbrPair : gr[node]){
            int nbr = nbrPair.second;
            int currentWeight = nbrPair.first;

             if(initial_weight + currentWeight < dist[nbr]){
                dist[nbr] = initial_weight + currentWeight;
                pq.push({dist[nbr], nbr});
             }
        }
    }
 
    //Single Source Shortest Dist to all other nodes
    for(int i=0;i<N;i++){
        cout<<"Node i "<<i <<" Dist "<<dist[i] <<endl;
    }

    return dist[dest];
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    
    cin>>N>>E;
    
    int u,v,w;
    for(int i=0; i<E; i++){
       cin>>u>>v>>w;
       gr[u].push_back({w,v});
       gr[v].push_back({w,u});
    }
    
    cout<<"Dijkstra shortest path:"<<endl;
    cout<<dijkstra(0,4)<<endl;


	return 0;
}

/*
5 6
0 1 1
1 2 1
0 2 4
0 3 7
3 2 2
3 4 3
*/