#include<bits/stdc++.h>
using namespace std;


int N,E;
vector<pair<int,int>> gr[100];

int dijkstra(int src,int dest){

        set<pair<int,int> >  s;
        vector<int> dist(N,INT_MAX);
        dist[src] = 0;
        s.insert({0,src});

        while(!s.empty()){

            auto it = s.begin();
            int node = it->second;
            int initial_weight = it->first; 
            s.erase(it); //Pop 

            //Iterate over the nbrs of node
            for(auto nbrPair : gr[node]){

                int nbr = nbrPair.second;
                int currentWeight = nbrPair.first;

                if(initial_weight + currentWeight < dist[nbr]){
                    //remove if nbr already exist in the set

                    auto f = s.find({dist[nbr],nbr});
                    if(f!=s.end()){
                        s.erase(f);
                    }
                    //insert the updated value with the new dist
                    dist[nbr] = initial_weight + currentWeight;
                    s.insert({dist[nbr],nbr});
                }
            }
        }

        //Single Source Shortest Dist to all other nodes
        for(int i=0;i<N;i++){
            cout<<"Node i "<<i <<" Dist "<<dist[i] <<endl;
        }
        return dist[dest];

    }

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    
    cin>>N>>E;
    
    int u,v,w;
    for(int i=0; i<E; i++){
       cin>>u>>v>>w;
       gr[u].push_back(make_pair(w,v));
       gr[v].push_back(make_pair(w,u));
    }
    cout<<"Dijkstra Shotest Path: "<<endl;
    cout<<dijkstra(0,4)<<endl;


	return 0;
}


/*

5 6
0 1 1
1 2 1
0 2 4
0 3 7
3 2 2
3 4 3

//u w v

*/