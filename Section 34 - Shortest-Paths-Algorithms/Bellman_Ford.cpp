#include<bits/stdc++.h>
using namespace std;

vector<int> bellman_ford(int V,int src,vector<vector<int> > edges){
    vector<int> dist(V+1, INT_MAX);
    dist[src] = 0;

    //relax all edges v-1 times
    for(int i=0; i<V-1; i++){
        for(auto edge : edges){
            int u = edge[0];
            int v = edge[1];
            int wt = edge[2];

            if(dist[u] != INT_MAX && dist[u] + wt < dist[v]){
                dist[v] = dist[u] + wt;
            }
        }
    }

    // negative wt cycle 

    for(auto edge : edges){
        int u = edge[0];
        int v = edge[1];
        int wt = edge[2];

        if(dist[u] != INT_MAX && dist[u] + wt < dist[v]){
            cout<<"negative Wt cycle found"<<endl;
            exit(0);
        }
    }
    return dist;
}

int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n,m;
    cin>>n>>m;

    vector<vector<int> >  edges;       // (a,b,3) (c,d,5)

    for(int i=0; i<n; i++){
        int u,v,wt;
        cin>>u>>v>>wt;
        edges.push_back({u,v,wt});
    }

    vector<int> distances = bellman_ford(n,1,edges);

    for(int i=1;i<=n;i++){
        cout<<"Node "<<i<<" is at dist "<<distances[i]<<endl;
    }

    return 0; 
}

/*

example 1:

input:

3 3
1 2 3
2 3
4
3 1 -10 

output:
negative Wt cycle found

example 2:


input:

3 3
1 2 3
2 3 4
1 3 -10

output:

node 1 is at dist 0
node 2 is at dist 3
node 3 is at dist -10

*/