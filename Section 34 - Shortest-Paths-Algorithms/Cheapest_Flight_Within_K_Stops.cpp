#include<bits/stdc++.h>
using namespace std;

int findCheapestPrice(int n, vector<vector<int>> flights, int src, int dst, int k) {
    vector<int> dist( n, 1000000000 );
    dist[src] = 0;
    
    for( int i=0; i <= k; i++ ) {
        vector<int> tmp( dist );
        for( auto flight : flights ) {
            if( dist[ flight[0] ] != INT_MAX ) {
                tmp[ flight[1] ] = min( tmp[flight[1]], dist[ flight[0] ] + flight[2] );
            }
        }
        dist = tmp;
    }
    return dist[dst] == INT_MAX ? -1 : dist[dst];
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    int n= 3, src= 0,dst = 2, k = 1; 
    vector<vector<int>> flights = {
    	{0,1,100},
    	{1,2,100},
    	{0,2,500}
    };
    cout<<findCheapestPrice(n, flights, src, dst, k)<<endl;

	return 0;
}

/*
Input: n = 3, flights = [[0,1,100],[1,2,100],[0,2,500]], src = 0, dst = 2, k = 1
Output: 200
Explanation: The graph is shown.
The cheapest price from city 0 to city 2 with at most 1 stop costs 200, as marked red in the picture.

*/