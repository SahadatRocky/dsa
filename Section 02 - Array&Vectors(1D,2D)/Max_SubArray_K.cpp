#include<bits/stdc++.h>
using namespace std;

void maxSubArrayK(vector<int>& arr,int n,int k){
   
   int max = 0;
   
   for(int i=0; i<n-k; i++){
   	 max = arr[i];
   	 for(int j = i; j<k; j++){
   	 	 if(arr[i+j] > max){
   	 	 	max = arr[i+j];
   	 	 }
   	 }
   	 cout<<max<<" ";
   }
   cout<<endl;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<int> arr{  1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    int n = arr.size();
    int k = 3;
    maxSubArrayK(arr,n,k);

	return 0;
}


/*

3 4 5 4 5 6 7 

*/