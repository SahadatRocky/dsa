

#include<bits/stdc++.h>
using namespace std;


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    //Demo Vector
	vector<int> arr =  {1,2,10,12,15};

	//filled constructor
	vector<int> arr1(5,10);
    

	vector<int> visited;

	visited.resize(arr.size()+1, -1);
    
    // sort
    // sort(arr.begin(),arr.end());
    
    // reverse
    // reverse(arr.begin(),arr.end()); 
    
    // assign
    // arr1.assign(5,10); 
    
    //pop back 
	arr.pop_back();
    
    //push Back
	arr.push_back(20);
    
    //insert
    arr.insert(arr.begin(),25);
    arr.insert(arr.begin()+3,100);
    
    //erase the first element
    arr.erase(arr.begin());
    
    //swap the arr to arr1
    // arr.swap(arr1);

     //clear
    arr1.clear();
     
    arr1.empty() == 1 ? cout<<"Empty"<<endl : cout<<"not Empty"<<endl; 
    cout<<"vector arr at :"<<arr.at(3)<<endl;
	cout<<"vector arr size :"<<arr.size()<<endl;
    cout<<"vector arr front :"<<arr.front()<<endl; 
    cout<<"vector arr back :"<<arr.back()<<endl; 


    cout<<"arr-----"<<endl;
	for(auto x: arr){
		cout<<x<<" ";
	}
	cout<<endl;

    cout<<"arr1-----"<<endl;
    for(auto x: arr1){
		cout<<x<<" ";
	}
	cout<<endl;
    
    cout<<"visited-----"<<endl;
	 for(auto x: visited){
		cout<<x<<" ";
	}
	cout<<endl;


    return 0;
}