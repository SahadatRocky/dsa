
#include<bits/stdc++.h>
#include<string>
using namespace std;

vector<int> subarraySort(vector<int>& arr){
     
     vector<int> b(arr);
     sort(arr.begin(), arr.end());

     int i = 0;
     int n = arr.size();

     while(i<n && arr[i] == b[i]){
        i++;
     }
     
     int j = arr.size() - 1; 
     while(j>=0 && arr[j] == b[j]){
        j--;
     }

     if(i == arr.size()){
        return {-1,-1};
     }
     
    return {i,j};
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

  vector<int> arr = {1, 2, 3, 4, 5, 8, 6, 7, 9, 10, 11};
    auto p = subarraySort(arr);
    for(auto x : p){
        cout<<x<<" ";
    }
    cout<<endl;
    // cout<< p.first <<" and "<<p.second <<endl;

    return 0;
}


//output 5,7


/*
  bool outOfOrder(vector<int> arr,int i){
    int x = arr[i];
    if(i==0){
        return x > arr[1];
    }
    if(i==arr.size()-1){
        return x < arr[i-1];
    }
    return x > arr[i+1] or x < arr[i-1];

}

pair<int,int> subarraySort(vector<int> arr) {

    int smallest = INT_MAX;
    int largest = INT_MIN;

    for(int i=0;i<arr.size();i++){
        int x = arr[i];
        
        if(outOfOrder(arr,i)){
            smallest = min(smallest,x);
            largest = max(largest,x);
        }
    }

    //next step find the right index where smallest and largest lie (subarray) for out solution
    if(smallest==INT_MAX){
        return {-1,-1};
    }

    int left = 0;
    while(smallest >= arr[left]){
        left++;
    }
    int right = arr.size() - 1;
    while(largest <= arr[right]){
        right--;
    }

    return {left,right};

} 

*/