
#include<bits/stdc++.h>
using namespace std;

vector<int> kRotate(vector<int> a, int k){
    int n = a.size();
    vector<int> v(n); 
    
    for(int i= 0;i<n; i++){
        v[(i+k)%n] = a[i];
    }

    return v;
    
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

    vector<int> arr{1, 3, 5, 7, 9};
    int x = 2;
    vector<int> output =  kRotate(arr,x);

    for(auto x: output){
        cout<<x<<" ";
    }
   

    return 0;
}

/*

Sample Input

{1, 3, 5, 7, 9}, x = 2


Sample Output

{7, 9, 1, 3, 5}

*/