
#include<bits/stdc++.h>
using namespace std;

 vector<vector<int>> generateMatrix(int n) {
        
        vector<vector<int>> matrix(n, vector<int>(n));
        int startRow = 0;
        int endRow = n - 1;
        int startCol = 0;
        int endCol = n - 1;
        
        int cnt = 1;
        
        while(startRow <= endRow && startCol <=endCol){
            
            //startRow
            for(int col = startRow; col<=endCol; col++){
                matrix[startRow][col] = cnt;
                cnt++;
            }
            
            //endCol
            for(int row = startRow + 1; row<=endRow; row++){
                matrix[row][endCol] = cnt;
                cnt++;
            }
            
            //endRow
            for(int col = endCol - 1; col>=startCol; col--){
                if(startRow == endRow){
                    break;
                }
                matrix[endRow][col] = cnt;
                cnt++;
            }
            
            //startCol
            for(int row = endRow - 1; row> startRow; row--){
                 if(startCol == endCol){
                     break;
                 }
                 matrix[row][startCol] = cnt;
                cnt++;
            }
            
            startRow++;
            endRow--;
            startCol++;
            endCol--;
        }
        
        return matrix;
    }

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    int n = 3;
    vector<vector<int>> output = generateMatrix(n);
    
    for(auto x: output){
       for(auto o: x){
           cout<<o<<" ";
       }
       cout<<endl;
    }
    cout<<endl;

    return 0; 
}