#include<bits/stdc++.h>
using namespace std;


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    vector<vector<int>> arr = {
        {1,2,3},
		{4,5,6},
		{7,8,9,10},
		{11,12} 
    };

    arr[0][0] += 10; //update


    for(int i=0; i<arr.size(); i++){
    	for(auto x : arr[i]){
    		cout<<x<<" ";
    	}
    	cout<<endl;
    }
    cout<<endl;
    
    ///

    for (vector<int> vect1D : arr)
    {
        for (int x : vect1D)
        {
            cout << x << " ";
        }    
        cout << endl;
    }
    cout<<endl;

    
    ///

    for (int i = 0; i < arr.size(); i++)
    {
        for (int j = 0; j < arr[i].size(); j++)
        {
            cout << arr[i][j] << " ";
        }    
        cout << endl;
    }

    return 0;
}