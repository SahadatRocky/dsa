#include<bits/stdc++.h>
using namespace std;

vector<vector<int>> makeZeroes(vector<vector<int>> arr){
    vector<int> r;
    vector<int> c;
    int rows = arr.size();
    int cols = arr[0].size();

    for(int i = 0; i< rows; i++){
        for(int j = 0; j<cols; j++){
            if(arr[i][j] == 0){
                r.push_back(i);
                c.push_back(j);
            }
        }
    }


    for(auto x: r){
        for(int i=0; i<rows; i++){
            arr[x][i] = 0;
        }
    }


   for(auto x: c){
        for(int i=0; i<rows; i++){
            arr[i][x] = 0;
        }
    }

    return arr;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<vector<int>> arr{ {5, 4, 3, 9}, 
    						{2, 0, 7, 6}, 
    						{1, 3, 4, 0}, 
    						{9, 8, 3, 4} };


    	vector<vector<int>> output = makeZeroes(arr); 					

        for(auto x: output){
        	for(auto d : x){
        		cout<<d<<" ";
        	}
        	cout<<endl;
        }

    return 0;
}


/*

Sample Input

{ {5, 4, 3, 9}, {2, 0, 7, 6}, {1, 3, 4, 0}, {9, 8, 3, 4} }


Sample Output

{ {5, 0, 3, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {9, 0, 3, 0} }


*/