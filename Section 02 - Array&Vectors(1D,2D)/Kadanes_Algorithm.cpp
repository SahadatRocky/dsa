
#include<bits/stdc++.h>
using namespace std;

int maximum_subarray_sum(int arr[],int n){
     
     int current_sum = arr[0];
     int maxSum = arr[0];

     for(int i=1; i<n; i++){
        current_sum = max(arr[i],current_sum + arr[i]);
        maxSum = max(maxSum, current_sum);
     }

     return maxSum;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

    int arr[] = {-2,3,4,-1,5,-12,6,1,3};
    int n = sizeof(arr)/sizeof(int);

    cout<< maximum_subarray_sum(arr,n) <<endl;

    return 0;
}
