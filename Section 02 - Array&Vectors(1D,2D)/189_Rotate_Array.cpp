
#include<bits/stdc++.h>
using namespace std;

vector<int> rotateArray(vector<int>& nums,int k){
    
    int n = nums.size();
    vector<int> ans(n);
    for(int i=0; i<n; i++){
        ans[(i+k)%n] = nums[i];
    }

    return ans;

}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
   vector<int> nums{1,2,3,4,5,6,7};
   int k = 3;
   
   vector<int> output = rotateArray(nums,k);

   for(auto x : output){
       cout<<x<<" ";
   }

    return 0;
}

/*

Input: nums = [1,2,3,4,5,6,7], k = 3
Output: [5,6,7,1,2,3,4]
Explanation:
rotate 1 steps to the right: [7,1,2,3,4,5,6]
rotate 2 steps to the right: [6,7,1,2,3,4,5]
rotate 3 steps to the right: [5,6,7,1,2,3,4]

*/