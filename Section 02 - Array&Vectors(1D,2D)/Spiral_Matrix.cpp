#include <bits/stdc++.h>
using namespace std;

vector<int> spiralOrder(vector<vector<int>>& matrix) {
   
   vector<int> v;

   int m = matrix.size();
   int n = matrix[0].size();

   int start_row = 0;
   int end_row = m-1;

   int start_col = 0;
   int end_col = n-1;

   while(start_row<= end_row && start_col <= end_col){
        
      //startRow
      for(int col = start_col; col<=end_col; col++){
      	 v.push_back(matrix[start_row][col]);
      }

   	  //endCol
        for(int row = start_row+1; row<=end_row; row++){
        	v.push_back(matrix[row][end_col]);
        }

   	  //endRow
       for(int col=end_col-1; col>=start_col; col--){
          
          if(start_row == end_row){
          	break;
          }
          v.push_back(matrix[end_row][col]);
       }

   	  //startCol
       for(int row=end_row-1; row>start_row; row--){
       	   if(start_col == end_col){
              break; 
       	   }
       	   v.push_back(matrix[row][start_col]);
       }
    
    start_row++;
    end_row--;
    start_col++;
    end_col--;

   }   
   return v;
}

int main(){
    
    freopen("input.txt","r", stdin);
    freopen("output.txt", "w", stdout);
    
   vector<vector<int>> matrix{
   	{1,2,3,4},
   	{5,6,7,8},
   	{9,10,11,12}
   };

    vector<int> output = spiralOrder(matrix);

    for(auto x : output){
      cout<<x<<" ";
    }
       
	return 0;
}

/*

Input: matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
Output: [1,2,3,4,8,12,11,10,9,5,6,7]

*/