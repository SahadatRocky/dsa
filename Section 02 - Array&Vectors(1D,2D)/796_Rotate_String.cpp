#include<bits/stdc++.h>
using namespace std;

 bool rotateString(string s, string goal) {
    
    queue<char> q1,q2;

    for(auto x: s){
    	q1.push(x);
    }

    for(auto x: goal){
    	q2.push(x);
    }

    int k = q2.size()-1;

    while(k--){
    	char front = q1.front();
    	q1.pop();

    	q1.push(front);

    	if(q1 == q2){
    		return true;
    	}
    }

    return false;
 }

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
   string s = "abcde", goal = "cdeab";
   
    cout<<rotateString(s, goal)<<endl;
   
    return 0;
}


/*

Input: s = "abcde", goal = "cdeab"
Output: true
Example 2:

Input: s = "abcde", goal = "abced"
Output: false

*/