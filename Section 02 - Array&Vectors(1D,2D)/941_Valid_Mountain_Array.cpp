
#include<bits/stdc++.h>
using namespace std;

bool validMountainArray(vector<int>& arr) {
        
    int n = arr.size();
    if(n<3){
        return false;
    }
        
    int left = 0;
    int right = n-1;
        
    while(left + 1 < n-1 && arr[left] < arr[left+1]){
        left++;
    }
        
    while(right - 1 > 0 && arr[right] < arr[right-1]){
        right--;
    }
    
    return left == right;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  
    vector<int> arr{0,3,2,1};
	cout<< validMountainArray(arr)<<endl;

    return 0;
}

/*

Input: arr = [2,1]
Output: false
Example 2:

Input: arr = [3,5,5]
Output: false
Example 3:

Input: arr = [0,3,2,1]
Output: true

*/