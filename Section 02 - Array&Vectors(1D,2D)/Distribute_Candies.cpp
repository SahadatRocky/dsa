#include<bits/stdc++.h>
using namespace std;

int distributeCandies(vector<int>& candies){
    int n = candies.size();
    set<int> st;

    for(auto x : candies){
    	st.insert(x);
    }

    int type = st.size();
    int candy = n/2;

    return min(candy,type);

}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> candies{1,1,2,2,3,3};
    cout<<distributeCandies(candies)<<endl;

    return 0;   
}  

/*

Input: candyType = [1,1,2,2,3,3]
 
Output: 3
 
Explanation: Alice can only eat 6 / 2 = 3 candies. Since there are only 3 types, she can eat one of each type.


*/