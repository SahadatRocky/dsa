
#include<bits/stdc++.h>
#include<string>
using namespace std;

int highest_mountain(vector<int>& arr){
    
    int n = arr.size();
    
    int highest = 0;
    for(int i=1; i<n-1;i++){
       
       if(arr[i] > arr[i-1] && arr[i] > arr[i+1]){
           int cnt = 1;
           int j = i;

           //backword
           while(j>=1 && arr[j] > arr[j-1]){ 
           	   j--;
               cnt++;
           }

           //forward
           while(i<n-1 && arr[i] > arr[i+1]){
			   i++;
           	   cnt++;
           }

          highest = max(highest, cnt); 
       }
    }

   return highest;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

   vector<int> arr{5,6,1,2,3,4,5,4,3,2,0,1,2,3,-2,4};

	cout<< highest_mountain(arr)<<endl;

    return 0;
}

//output 9