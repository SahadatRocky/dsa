#include<bits/stdc++.h>
using namespace std;

vector<int> findClosestElements(vector<int> arr, int k, int x) {
         priority_queue< pair<int,int>, vector< pair<int,int>>, greater< pair<int,int>> > pq;
        
        for(auto el: arr){
            pq.push(make_pair(abs(x-el), el));
        }
        
        vector<int> v;
        
        while(k--){
            v.push_back(pq.top().second);
            pq.pop();
        }
        sort(v.begin(), v.end());
        return v;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<int> arr{1,2,3,4,5};
    int k = 4;
    int x = 3;

    vector<int> output = findClosestElements(arr, k, x);

    for(auto x : output){
        cout<<x<<" ";
    }
    cout<<endl;

    return 0;
}

/*

Input: arr = [1,2,3,4,5], k = 4, x = 3
Output: [1,2,3,4]
Example 2:

Input: arr = [1,2,3,4,5], k = 4, x = -1
Output: [1,2,3,4]

*/