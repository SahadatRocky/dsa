#include<bits/stdc++.h>
using namespace std;

int subarraySum(vector<int>& nums, int k) {
        
    int ans = 0;
    int sum = 0;
        
    map<int ,int > mp;
    mp[0] = 1;
    for(auto x : nums){
            
        sum += x;
        int f = sum - k;
        if(mp.find(f) != mp.end()){
            ans+= mp[f];
        }
            
        mp[sum]++;
    }
        
    return ans;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<int> nums{1,1,1};
    int k = 2;
    cout<<subarraySum(nums, k)<<endl;
    return 0;
}

/*

Input: nums = [1,1,1], k = 2
Output: 2
Example 2:

Input: nums = [1,2,3], k = 3
Output: 2

*/