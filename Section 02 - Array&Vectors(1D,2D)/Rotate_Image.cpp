
#include<bits/stdc++.h>
using namespace std;

vector<vector<int>> rotateImage(vector<vector<int>>& matrix) {
        
        int m = matrix.size();
        int n = matrix[0].size();
        
        for(int i=0; i<m; i++){
            for(int j=i; j<n; j++){
               int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
        
        for(int i=0; i<m; i++){
            reverse(matrix[i].begin(), matrix[i].end());
        }
        
        return matrix;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
   vector<vector<int>> arr{ {1,2,3}, 
    						{4,5,6}, 
    						{7,8,9}};


    vector<vector<int>> output = rotateImage(arr); 

    	 for(auto x: output){
        	for(auto d : x){
        		cout<<d<<" ";
        	}
        	cout<<endl;
        }

    return 0;
}


/*


Sample Input

matrix = [[1,2,3],[4,5,6],[7,8,9]]


Sample Output

[[7,4,1],[8,5,2],[9,6,3]]



*/
