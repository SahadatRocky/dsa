#include<bits/stdc++.h>
using namespace std;


vector<int> find_missing_repeating(vector<int> array)
{
    int n = array.size() + 1;

    vector<int> v(n, 0);

    vector<int> ans;

    for (int i = 0; i < array.size(); i++)
    {
        v[array[i]]++;
    }

    for (int i = 1; i <= array.size(); i++)
    {
        if (v[i] == 0 || v[i] > 1)
        {
            ans.push_back(i);
        }
    }

    return ans;
}




int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<int> arr{3,1,2,5,3};    

    vector<int> output = find_missing_repeating(arr);

    for(auto x : output){
    	cout<<x<<" ";
    }
    cout<<endl;
   
    return 0;
}