
#include<bits/stdc++.h>
using namespace std;

int findKthLargest(vector<int>& nums, int k) {
  sort(nums.begin(), nums.end(), greater<int>()); //Large to Small 

  return nums[k-1];
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums{3,2,3,1,2,4,5,5,6};
    int k = 4;

    cout<<findKthLargest(nums,k)<<endl;

    return 0;   
}    
/*

Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
 
Output: 4

*/