
#include<bits/stdc++.h>
#include<string>
using namespace std;

int trappedWater(vector<int>& height){
     
    int n = height.size();
    
    vector<int> left(n,0),right(n,0);
    left[0] = height[0];
    right[n-1] = height[n-1];
    
    //left
    for(int i = 1; i<n; i++){
        left[i] = max(left[i-1],height[i]);
    }

    //right
    for(int j=n-2; j>=0; j--){
      right[j] = max(right[j+1],height[j]);
    }

    int water  = 0;

    for(int i=0; i<n; i++){
        water += min(left[i], right[i]) - height[i];
    }
   
   return water;

}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

   vector<int> water = {0,1,0,2,1,0,1,3,2,1,2,1};
    cout<<trappedWater(water)<<endl;

    return 0;
}

//output 6