#include<bits/stdc++.h>
#include<string>
using namespace std;

string to_string(int a) {
	string ans;
	stringstream ss;
	ss << a;
	ss >> ans;
	return ans;
}

vector<string> fizzBuzz(int n){
    
    vector<string> v;

    for(int i=1; i<=n; i++){
    	if(i%15 == 0){
    		v.push_back("FizzBuzz");
    	}
    	else if(i%5 == 0){
            v.push_back("Buzz");
    	}
    	else if(i%3 == 0){
    		v.push_back("Fizz");
    	}
    	else{
    		v.push_back(to_string(i));
    	}
    }   

    return v;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

    vector<string> output = fizzBuzz(15);
    
    for(auto x: output){
    	cout<<x<<" ";
    }
    cout<<endl;

    return 0;
}

/*
   fizzbuzz(15) == {
		"1", "2", "Fizz", "4", "Buzz",
		"Fizz", "7", "8", "Fizz", "Buzz",
		"11", "Fizz", "13", "14", "FizzBuzz"
    }  
*/


