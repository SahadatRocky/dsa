
#include<bits/stdc++.h>
#include<string>
using namespace std;


int largestBand(vector<int>& arr){
     
     unordered_set<int> st;

     for(auto x : arr){
        st.insert(x);
     }

     int longestLen = 1;
     
     for(auto element : st){
         
         int parent = element - 1;

         if(st.find(parent) == st.end()){
             int next_no = element + 1;
             int cnt = 1;

             while(st.find(next_no) != st.end()){
                next_no++;
                cnt++;
             }

             longestLen = max(longestLen,cnt);
         }
     }
    return longestLen;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

    vector<int> arr{1, 9, 3, 0, 18, 5, 2, 4, 10, 7, 12, 6};
    cout << largestBand(arr)<<endl;

    return 0;
}


/*  
    9-10   = 2
    0-1-2-3-4-5-6-7 = 8
    12 = 1
    18 = 1
*/