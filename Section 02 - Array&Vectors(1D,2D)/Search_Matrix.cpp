#include<bits/stdc++.h>
using namespace std;

bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int m = matrix.size();
        int n = matrix[0].size();
        
        int i = 0;
        int j = n - 1;
        
        while(( i>=0 && i<m ) && (j>=0 && j<n) ){
            if(matrix[i][j] == target){
                return true;
            }
            else if(matrix[i][j] > target){
                j--;
            }else{
                i++;
            }
        }
        return false;
    }
    
int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    int k = 4;
    vector<vector<int>> v{
    	{1, 4, 9},
    	{2, 5, 10},
    	{6, 7, 11}
    };
    
    cout<<searchMatrix(v,k)<<endl;
    return 0;
}