#include<bits/stdc++.h>
using namespace std;

string to_string(int a){
   
   string ans;
   stringstream ss;
   ss<<a;
   ss>>ans;

   return ans;
}

string getHint(string secret, string guess){

    int bull = 0;
    int cow = 0;
    unordered_map<char,int>m;
    for(int i=0; i<secret.size(); i++){
    	if(secret[i] == guess[i]){
    		bull++;
    	}else{
    		if( (m[secret[i]]++ < 0) || (m[guess[i]]-- > 0) ){
    			cow++;
    		}
    	}
    }

    return to_string(bull)+"A"+to_string(cow)+"B";
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    string secret = "1123", guess = "0111";

    cout<<getHint(secret, guess)<<endl;

    return 0;   
}  

/*

Input: secret = "1123", guess = "0111"
 
Output: "1A1B"
 
Explanation: Bulls are connected with a '|' and cows are underlined:
"1123"        "1123"
  |      or     |
"0111"        "0111"
Note that only one of the two unmatched 1s is counted as a cow since the non-bull digits can only be rearranged to allow one 1 to be a bull.

*/