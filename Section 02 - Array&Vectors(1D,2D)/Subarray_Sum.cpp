
#include<bits/stdc++.h>
using namespace std;

int largestSubarraySum(int arr[],int n){
    
    int largest_sum = 0;
    
    for(int i= 0; i<n; i++){
          for(int j= i; j<n; j++){
           int subArray_sum = 0;
               for(int k = i; k<j; k++){
                   subArray_sum += arr[k];
               }
               largest_sum = max(largest_sum,subArray_sum);
        }
    }
    return largest_sum;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

    int arr[] = {-2,3,4,-1,5,-12,6,1,3};
    int n = sizeof(arr)/sizeof(int);

    cout<< largestSubarraySum(arr,n) <<endl;

    return 0;
}
