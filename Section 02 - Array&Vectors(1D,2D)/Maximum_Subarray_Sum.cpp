
#include<bits/stdc++.h>
#include<string>
using namespace std;

int maximumSubArraySum(vector<int>& arr){
     
     int current_sum = arr[0];
     int maximum_sum = arr[0];

     for(int i=0; i<arr.size(); i++){

         current_sum = max(arr[i], current_sum + arr[i]);
         maximum_sum = max(maximum_sum, current_sum);
     }

     return maximum_sum;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  
    vector<int> arr{-1,2,3,4,-2,6,-8,3};
    cout<<maximumSubArraySum(arr)<<endl;
   

    return 0;
}


/// output 13