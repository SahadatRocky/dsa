
#include<bits/stdc++.h>
using namespace std;

pair<int, int> closestSum(vector<int> arr, int x){
      pair<int,int> p;
      int res_l = 0;
      int res_r = 0;
      int l =0;
      int r =arr.size() - 1;
      int diff = INT_MAX;

      while(l<r){
         
         if(abs(arr[l] + arr[r] - x) < diff){
            res_l = l;
            res_r = r;
            diff =  abs(arr[l] + arr[r] - x);
         }

         if(arr[l] + arr[r] > x){
            r--;
         }else{
             l++;
         }
      }

      p.first = arr[res_l];
      p.second = arr[res_r];

      return p;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<int> arr{10, 22, 28, 29, 30, 40};
    int x = 54;
    pair<int,int> output = closestSum(arr,x);
    
    cout<<output.first<<" and "<<output.second<<endl;

    return 0;
}

/*

Sample Input

{10, 22, 28, 29, 30, 40}, x = 54


Sample Output

22 and 30


*/