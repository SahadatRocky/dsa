
#include<bits/stdc++.h>
#include<string>
using namespace std;

//Expected Complexity O(N)
vector<int> productArray(vector<int> arr){
   int n = nums.size();
   if(n == 1){
      return {0};
   }

   int maxProduct = 1;
   vector<int> res(n, 0); 
   for(int i=0; i<nums.size(); i++){
      res[i] = maxProduct;
      maxProduct = maxProduct * nums[i];
      
   }

   maxProduct = 1;
   for(int i= nums.size()-1; i>=0; i--){
       res[i] = res[i] * maxProduct;
       maxProduct = maxProduct * nums[i];
   }
   return res;
}
int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  
    vector<int> arr{1,2,3,4,5};

    auto output = productArray(arr);
    for(auto x : output){
    	cout<<x<<" ";
    }
    cout<<endl;
    return 0;
}


/*
Sample Input

Both inputs and outputs are vectors.

{1,2,3,4,5}
Sample Output

{120, 60, 40, 30, 24}


*/