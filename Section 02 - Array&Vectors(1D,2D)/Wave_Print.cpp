
#include<bits/stdc++.h>
using namespace std;

 vector<int> WavePrint(int m, int n, vector<vector<int>> arr)
{
     vector<int> ans;
    for(int row = 0; row<n; row++){
        ans.push_back(arr[row][m-1]);
    }
    for(int row = n-1; row>=0; row--){
        ans.push_back(arr[row][m-2]);
    }
    
    for(int row = 0; row<n; row++){
        ans.push_back(arr[row][m-3]);
    }

    for(int row = n-1; row>=0; row--){
        ans.push_back(arr[row][m-4]);
    }


    return ans;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<vector<int>> arr { 
                      {1, 2, 3, 4},
                      {5, 6, 7, 8},
                      {9, 10, 11,12},
                      {13, 14, 15, 16}
                    };

    int n = 4,m = 4; 

    vector<int> output = WavePrint(m,n,arr);
    
    for(auto x: output){
        cout<<x<<" ";
    }

    return 0;
}


/*
 
 Examples :

Input :
1 2 3 4

5 6 7 8

9 10 11 12

13 14 15 16

Output :
4 8 12 16 15 11 7 3 2 6 10 14 13 9 5 1



Input :
1 9 4 10

3 6 90 11

2 30 85 72

6 31 99 15

Output :
10 11 72 15 99 85 90 4 9 6 30 31 6 2 3 1


*/