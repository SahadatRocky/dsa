
#include<bits/stdc++.h>
using namespace std;

bool nameCompare(pair<string,int>& s1, pair<string,int>& s2){
    return s1.first < s2.first;
}

bool priceCompare(pair<string,int>& s1, pair<string,int>& s2){
    
    return s1.second < s2.second;
}

vector<pair<string,int>> sortFruits(vector<pair<string,int>> v, string S){
   
   if(S == "name"){
       sort(v.begin(), v.end(), nameCompare);
   }else{
      sort(v.begin(), v.end(), priceCompare);
   }
   
   return v;
    
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<pair<string,int> > fruits = {
        {"apple",10},
        {"mango",100},
        {"guava",20},
        {"papaya",40},
        {"orange",60},
        {"banana",120},
    };

    string S = "price"; 

    vector<pair<string,int>> output = sortFruits(fruits , S);
    
    for(auto x: output){
        cout<<x.first<<" "<<x.second<<endl;
    }

    return 0;
}


/*

Sample Input

{ (Mango,100), (Guava,70), (Grapes,40), (Apple,60), (Banana,30) }, S = "price"


Sample Output

{ ("Banana", 30), ("Grapes", 40), ("Apple", 60), ("Guava", 70), ("Mango", 100) }


*/