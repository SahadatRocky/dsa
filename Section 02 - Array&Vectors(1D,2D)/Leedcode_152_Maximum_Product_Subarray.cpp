#include<bits/stdc++.h>
using namespace std;

int maxProduct(vector<int>& nums) {
   
   int maxProduct = 1;
   int maxSum = INT_MIN;

   for(int i=0; i<nums.size(); i++){
   	  maxProduct = maxProduct * nums[i];
   	  maxSum = max(maxSum, maxProduct);

   	  if(maxProduct == 0){
   	  	maxProduct = 1;
   	  }
   }

   maxProduct = 1;

   for(int i=nums.size()-1; i>=0; i--){
   	 maxProduct = maxProduct * nums[i];
   	 maxSum = max(maxSum, maxProduct);

       if(maxProduct == 0){
         maxProduct = 1;
        }
   } 

   return maxSum;

}

int main(){
    
    freopen("input.txt","r", stdin);
    freopen("output.txt", "w", stdout);

    vector<int>nums{2,3,-2,4};

    cout<<maxProduct(nums)<<endl;

	return 0;
}

/*

Input: nums = [2,3,-2,4]
Output: 6
Explanation: [2,3] has the largest product 6.
Example 2:

Input: nums = [-2,0,-1]
Output: 0
Explanation: The result cannot be 2, because [-2,-1] is not a subarray.

*/