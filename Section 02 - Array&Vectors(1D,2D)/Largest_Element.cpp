
#include<bits/stdc++.h>
using namespace std;

int largestElement(vector<int>& arr){
    
   int largestElement = 0;

    for(auto x : arr){
       largestElement = max(largestElement, x);
    }

   return largestElement;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<int> arr{-3, 4, 1, 2, 3};
    cout<<largestElement(arr)<<endl;

    return 0;
}

/*

Sample Input

-3 4 1 2 3

Sample Output

4

*/