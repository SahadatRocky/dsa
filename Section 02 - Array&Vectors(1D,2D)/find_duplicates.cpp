#include<bits/stdc++.h>
using namespace std;


int find_dupicate(vector<int>& arr){
    
    sort(arr.begin(), arr.end());
   
    for(int i=0; i< arr.size() - 1 ; i++){
        if(arr[i] == arr[i+1]){
            return arr[i];
        } 
    }

    return -1;
}


//linklist cycle method
int find_dupicate1(vector<int>& arr){
    
    int slow = arr[0];
    int fast = arr[0];
   
    do{
        slow = arr[slow];
        fast = arr[arr[fast]];
        
    }while(slow != fast);

    fast = arr[0];

    while(slow != fast){
        slow = arr[slow];
        fast = arr[fast];
    }

    return slow;
}



int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<int> arr{1,3,4,2,2};

    cout<<find_dupicate(arr)<<endl;

    cout<<find_dupicate1(arr)<<endl;
   
    return 0;
}