

#include<bits/stdc++.h>
#include<string>
using namespace std;

pair<int,int> minimumDifference(vector<int> &a1,vector<int> &a2){

	sort(a2.begin(),a2.end());
	int p1,p2;
	
	int diff = INT_MAX;
	
	for(int x : a1){
			auto lb = lower_bound(a2.begin(),a2.end(),x) - a2.begin();

			if(lb>=1 and x - a2[lb-1] < diff){
				  diff = x - a2[lb-1];
				  p1 = x;
				  p2 = a2[lb-1];
				  
			}

		 if(lb!=a2.size() and a2[lb]-x < diff ){
			 		diff = a2[lb] - x;
			 		p1 = x;
			 		p2 = a2[lb];
		 }
	}
	
  return {p1,p2};
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  
    vector<int> a1{23, 5, 10, 17, 30};
    vector<int> a2{26, 134, 135, 14, 19};

    pair<int,int> output = minimumDifference(a1,a2);
    cout<<output.first<<" "<<output.second<<endl;
   

    return 0;
}


/*
Input

Array1 = [23, 5, 10, 17, 30]
Array2 = [26, 134, 135, 14, 19]
Output

17,19

*/