
#include<bits/stdc++.h>
using namespace std;

int linear_search(int arr[],int n,int key){
    
    for(int i=0; i<n; i++){
        if(arr[i] == key){
            return i;
        }
    }

    return -1;

}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

    int arr[] = {10,15,12,9,6,4,3,10,8};
    int n = sizeof(arr)/sizeof(int);

    int key = 6;

    int index = linear_search(arr,n,key);

    index != -1 ? cout<<key <<" is present at index "<< index <<endl :
           cout<<key <<" is NOT Found!" <<endl;

    return 0;
}
