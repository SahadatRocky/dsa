#include<bits/stdc++.h>
using namespace std;

 vector<vector<int>> trippletSum(vector<int> &arr,int S){
     
     
     sort(arr.begin(), arr.end());
     vector<vector<int>> ans;
     int n = arr.size();

     for(int i=0; i<n; i++){
         
         int j = i + 1;
         int k = n - 1;

         while(j<k){
            
            int sum = arr[i] + arr[j] + arr[k];

            if(sum == S){
               ans.push_back({arr[i] , arr[j] , arr[k]});
               j++;
               k--;
            }
            else if(sum > S){
                k--;
            }else{
                j++;
            }
         }  
     }
     
    return ans;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

   vector<int> arr{1, 2, 3, 4, 5, 6, 7, 8, 9, 15};
    int S = 18;

   auto result = trippletSum(arr,S);

   for(auto v : result){
        for(auto no : v){
            cout<<no<<",";
        }
        cout<<endl;
    }

    return 0;
}
