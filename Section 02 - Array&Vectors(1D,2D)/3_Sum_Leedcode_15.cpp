#include <bits/stdc++.h>
using namespace std;

 vector<vector<int>> threeSum(vector<int>& nums) {
        sort(nums.begin(), nums.end());

        set<vector<int>> st; 
        vector<vector<int>> ans;
        int n = nums.size();
        
        for(int i=0; i<n; i++){
            int j = i+1;
            int k = n-1;
            
            while(j<k){
                int sum = nums[i] + nums[j] + nums[k];
                
                if(sum == 0){
                    st.insert( {nums[i], nums[j], nums[k]});
                    j++;
                    k--;
                }
                else if(sum > 0){
                    k--;
                }else{
                    j++;
                }
            }
        }
        
        for(auto x : st){
            ans.push_back(x);
        }
        return ans;
    }


int main(){
   
   freopen("input.txt","r", stdin);
   freopen("output.txt", "w", stdout);

   vector<int> arr{-1,0,1,2,-1,-4};
   vector<vector<int>> output = threeSum(arr);
   
   for(auto v1: output){
   	  for(auto x : v1){
         cout<<x<<" ";
   	  }
   	  cout<<endl;
   }
   cout<<endl;

   return 0;
}

/*

Input: nums = [-1,0,1,2,-1,-4]
Output: [[-1,-1,2],[-1,0,1]]
Explanation: 
nums[0] + nums[1] + nums[2] = (-1) + 0 + 1 = 0.
nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0.
nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0.
The distinct triplets are [-1,0,1] and [-1,-1,2].
Notice that the order of the output and the order of the triplets does not matte

*/