#include<bits/stdc++.h>
#include<string>
using namespace std;

vector<int> pairSum(vector<int>& arr,int S){
    
    unordered_set<int> st;
    vector<int> ans;
    for(int i=0; i<arr.size(); i++){
       
       int x = S - arr[i];

       if(st.find(x) != st.end()){
            ans.push_back(x);
            ans.push_back(arr[i]);
            return ans;

       }

        st.insert(arr[i]);
    }

    return {};

}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
  

   vector<int> arr{10, 5, 2 , 3, -6, 9 , 11};
   int S = 4;

   vector<int> output = pairSum(arr,S);
    
    for(auto x: output){
    	cout<<x<<" ";
    }
    cout<<endl;

    return 0;
}

/*

10, -6

*/