#include<bits/stdc++.h>
using namespace std;

int minCostclimbingStair(vector<int>& cost,int i ,vector<int>& dp){
    
    //base case
    if(i >= cost.size()){
    	return 0;
    }

    //memoization
    if(dp[i] != -1){
    	return dp[i];
    }

    int indexOne = minCostclimbingStair(cost,i+1,dp);
    int indexTwo = minCostclimbingStair(cost,i+2,dp);
    
    int ans = min(indexOne, indexTwo) + cost[i];

    return dp[i] = ans;

}


int main(){
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);

   vector<int> cost{10,15,20};
   vector<int>dp(cost.size() + 1, -1);
   cout<< min(minCostclimbingStair(cost,0,dp), minCostclimbingStair(cost,1,dp))<<endl; 
   
   return 0;
}


/*

Input: cost = [10,15,20]
Output: 15
Explanation: You will start at index 1.
- Pay 15 and climb two steps to reach the top.
The total cost is 15.


*/