#include<bits/stdc++.h>
using namespace std;

// O(n^2) is better

int countBST(int n, vector<int>& dp){
   
   if(n == 0 || n == 1){
   	return 1;
   }

   if(dp[n] != -1){
   	return dp[n];
   }
   
   int ans = 0;
   for(int i=1; i<=n; i++){
       
       int x = countBST(i-1, dp);
       int y = countBST(n-i, dp);
       ans += x*y;
   }
   return dp[n] = ans;
}


int countBSTBottomU(int N){

	vector<int> dp(N+1, 0);
	dp[0] = dp[1] = 1;

	for(int n=2; n<=N; n++){
        for(int i=1; i<=n; i++){
           dp[n] += dp[i-1] * dp[n-i]; 
        }
	}

	return dp[N];
}

int main(){
   
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);
   
   int n;
   cin>>n;
   vector<int> dp(n+1, -1); 
   cout<<countBST(n, dp)<<endl;

   cout<<countBSTBottomU(n)<<endl;

   return 0;
}

/*
input:
n = 3

output:
5

*/