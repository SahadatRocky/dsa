#include<bits/stdc++.h>
using namespace std;

//Top Down O(n^k)
int countWays(int n,int k,vector<int> &dp){
    
   //base case
	if(n== 0){
	   return 1;
	}

	if(n<0){
		return 0;
	}
    
    if(dp[n] != -1){
    	return dp[n];
    }
    
    int ans = 0;
    for(int jump = 1; jump<=k; jump++){
    	ans += countWays(n - jump,k,dp);
    }

   return dp[n] = ans;
}


///Buttom up O(n*k)
int countWaysBU(int n,int k,vector<int>& dp){
    
    dp[0] = 1;
    for(int i = 1; i<=n; i++){
    	for(int jump = 1 ; jump<=k; jump++){
    		
    		if(i - jump >= 0){
                dp[i] = dp[i] + dp[i - jump];
    		}
    	}
    }
    
    return dp[n];
}



int main(){
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);

   int n,k;
   cin>>n>>k;
   vector<int> dp(n+1, -1); 
   cout<<countWays(n,k,dp)<<endl;
   
   vector<int> dp1(n+1, 0); 
   cout<<countWaysBU(n,k,dp1)<<endl;

   return 0;
}

/*
 
input  :
  n = 4
  k = 3  

output :
  7 

*/