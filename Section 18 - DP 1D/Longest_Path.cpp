#include<bits/stdc++.h>
#define int long long int
using namespace std;

const int N = 1e5 + 1;
vector<int> gr[N];
vector<int> dp(N,-1);

int dfs(int cur){
    
    if(dp[cur] != -1){
    	return dp[cur];
    }

    int ans = 0;
    for(auto nbr : gr[cur]){
        ans = max(ans, dfs(nbr) + 1);
    }

    return ans;
}

int32_t main() {

	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int n, m;
	cin >> n >> m;
	for (int i = 0; i < m; i++) {
		int x, y;
		cin >> x >> y;
		gr[x].push_back(y);
	}
    
	int ans = 0;
	for (int i = 1; i <= n; i++) {
		ans = max(ans, dfs(i));
	}

	cout << ans;
	return 0;
}

/*
input:
4 5
1 2
1 3
3 2
2 4
3 4

output:
3
*/