#include<bits/stdc++.h>
using namespace std;

int maxProfit(vector<int>& prices) {
   int currentMin = INT_MAX;
   int maxProfit = 0;

   for(int i=0; i<prices.size(); i++){

   	  currentMin = min(currentMin, prices[i]);
   	  maxProfit = max(maxProfit, prices[i] - currentMin);
   } 
   
   return maxProfit;
}


int main(){
    
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> prices{7,1,5,3,6,4};

    cout<<maxProfit(prices)<<endl;
	return 0;
}

/*

Input: prices = [7,1,5,3,6,4]
Output: 5
Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.

*/