#include<bits/stdc++.h>
using namespace std;

vector<int> memo;
    
int solve(int n){
        
        //base case
        if(n == 0 || n == 1){
            return n;
        }
        
        //memoization
        if(memo[n] != -1){
            return memo[n];
        }
        
        return memo[n] = solve(n-1) + solve(n-2);
}
    
int fib(int n) {
        memo.resize(n+1, -1);
        return solve(n);
}


int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int n;
    cin>>n;
    cout<<fib(n)<<endl;

	return 0;
}