#include<bits/stdc++.h>
using namespace std;

int solve(vector<int>& prices) {

   int maxProfit = 0;
   int n = prices.size();

   for(int i=1; i<n; i++){
      if(prices[i-1] < prices[i]){
         maxProfit += prices[i] - prices[i-1];
      }
   }

   return maxProfit;
}


int main(){
    
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> prices{7,1,5,3,6,4};

    cout<<solve(prices)<<endl;

	 return 0;
}

/*

Input: prices = [7,1,5,3,6,4]
Output: 7
Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
Total profit is 4 + 3 = 7.
*/