#include<bits/stdc++.h>
using namespace std;

int solve(int l,int r,vector<int>& v){
    
    if(l == r){
    	return v[l];
    }

    if(l>r){
    	return 0;
    }

    int op1 = v[l] + min( solve(l+2,r,v), solve(l+1,r-1,v) );
    int op2 = v[r] + min( solve(l,r-2,v), solve(l+1,r-1,v) );

    int ans = max(op1, op2);

    return ans;
}

int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 4;
    vector<int> v{1,2,3,4};

    cout<<solve(0,n-1,v)<<endl;
    
	return 0;
}

/*


Sample Testcase:

Input: 4 1 2 3 4
Output: 6

Explanation: Oswald will pick up coin with value 4, Henry will pick coin with value 3, Oswald will pick 2 and Henry will pick 1. Hence 4+2=6.

*/