#include<bits/stdc++.h>
using namespace std;

long long frogJump2(vector<int> &stones,int k){
     
     int n = stones.size();
     vector<long long> dp(n+1, 0);
     dp[1] = abs(stones[1] - stones[0]) + dp[0];
     for(int i = 2; i<n; i++){
     	dp[i] = INT_MAX;
     	for(int kk=1 ; kk<=k && i - kk >=0; kk++){
             dp[i] = min(dp[i], abs(stones[i] - stones[i-kk]) + dp[i-kk]);
     	}
     }

     return dp[n-1];
} 

int main(){
   
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);
   
   int k = 3;
   // cin>>k;
   vector<int> stones{10, 30, 40, 50, 20};
   cout<<frogJump2(stones,k)<<endl;
   return 0;
}

/*

Input:

k= 3

10 30 40 50 20

Output:

30

*/