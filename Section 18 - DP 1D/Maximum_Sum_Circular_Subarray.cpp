#include<bits/stdc++.h>
using namespace std;

int kedans(vector<int>& nums){
    
    int currentSum = nums[0];
    int maxSum = nums[0];

    for(int i=1; i<nums.size(); i++){
    	currentSum = max(nums[i], currentSum + nums[i]);
    	maxSum = max(maxSum, currentSum);
    }

    return maxSum;
}

int solve(vector<int>& nums){
   
   int x = kedans(nums); 

   int y = 0;

   for(int i=0;i<nums.size(); i++){
   	 y += nums[i];
   	 nums[i] *= -1;
   }

   int z = kedans(nums);

   return (y+z == 0) ? x : max(x,y+z); 
}

int main(){
    
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums{1,-2,3,-2};

    cout<<solve(nums)<<endl;
	return 0;
}

/*

Input: nums = [1,-2,3,-2]
Output: 3
Explanation: Subarray [3] has maximum sum 3.


*/