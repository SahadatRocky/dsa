#include<bits/stdc++.h>
using namespace std;
    
// Bottom-UP    
int fib(int n) {
   int dp[n+1];
   
   memset(dp, 0, sizeof(dp));
    
   dp[0] = 0;
   dp[1] = 1;

   for(int i=2; i<=n; i++){
        dp[i] = dp[i-2] + dp[i-1];
   }    

   return dp[n];
}


int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int n;
    cin>>n;
    cout<<fib(n)<<endl;

	return 0;
}