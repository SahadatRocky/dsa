#include<bits/stdc++.h>
using namespace std;

int n;

int solve(vector<int> &a,vector<int>& b,vector<int>& c,int i,int prev){
    
    //base case
    if(i>=n){
    	return 0;
    }
    
    int ans = INT_MIN;
    if(prev == 0){
       ans = max(ans, b[i] + solve(a,b,c,i+1,1) );
       ans = max(ans, c[i] + solve(a,b,c,i+1,2) );
    }
    else if(prev == 1){
    	ans = max(ans, a[i] + solve(a,b,c,i+1,0) );
       ans = max(ans, c[i] + solve(a,b,c,i+1,2) );
    }else if(prev == 2){
    	ans = max(ans, a[i] + solve(a,b,c,i+1,0) );
    	ans = max(ans, b[i] + solve(a,b,c,i+1,1) );
    }else{
    	ans = max(ans, a[i] + solve(a,b,c,i+1,0) );
    	ans = max(ans, b[i] + solve(a,b,c,i+1,1) );
    	ans = max(ans, c[i] + solve(a,b,c,i+1,2) );
    }

    return ans;
}


int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
   
    vector<int> a{10, 20, 30};
    vector<int> b{40, 50, 60};
    vector<int> c{70, 80, 90};

    n = a.size();
    cout<<solve(a,b,c,0,0)<<endl;
    

	return 0;
}