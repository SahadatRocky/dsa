#include<bits/stdc++.h>
using namespace std;

int rodCutting(int prices[],int n, vector<int>& dp){
 
    if( n < 0 ){
    	return 0;
    }

    if(dp[n] != -1){
    	return dp[n];
    }
    
    int ans = 0;
    for(int i=0; i<n; i++){
    	int cut = i + 1;
        
       int currentAns = prices[i] + rodCutting(prices, n - cut,dp);
       ans = max(ans, currentAns);
    }
    return dp[n] = ans;
}


int rodCuttingBU(int prices[],int n){
   
   int dp[n+1];

   dp[0] = 0;
   
   int ans = 0;
   for(int len = 1; len<=n; len++){
      
   	  for(int i=0; i<len; i++){

   	  	int cut = i + 1;
        int currentAns = prices[i] + dp[len - cut];
        ans = max(ans, currentAns);
        
   	  }

   	  dp[len] = ans;
   }

   return dp[n];
}


int main(){
   
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);

   int prices[] = {1,5,8,9,10,17,17,20};
   int n = sizeof(prices)/sizeof(int);
   vector<int> dp(n+1,-1);

   cout<<rodCutting(prices,n, dp)<<endl;

   cout<<rodCuttingBU(prices,n)<<endl;


   return 0;
}

/*

input: [1,5,8,9,10,17,17,20]

output: 22

input: [3,5,8,9,10,17,17,20]

output: 24

*/