#include<bits/stdc++.h>
using namespace std;


int solve(vector<int>& nums,int n,int i,vector<int>& dp){
        
    //base case
    if(i >= n){
        return 0;
    }
        
    //memoization
    if(dp[i] != -1){
        return dp[i];
    }
    
    //rec case
        
    int include = solve(nums,n,i+2, dp) + nums[i];
    int exclude = solve(nums,n,i+1, dp);
        
    int ans  = max(include,exclude);
        
    return dp[i] = ans;
}
    
int main(){
   
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);
   
   vector<int> nums{1,2,3,1};
   int n = nums.size();
   vector<int> dp(n+1, -1);
        
   cout<<solve(nums,n,0, dp)<<endl;

   return 0;
}


/*
Input: nums = [1,2,3,1]
Output: 4
Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
Total amount you can rob = 1 + 3 = 4.

*/
