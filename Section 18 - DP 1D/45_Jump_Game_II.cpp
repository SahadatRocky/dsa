#include<bits/stdc++.h>
using namespace std;

int min_jump(vector<int>& arr,int n, int i,vector<int>& dp){
     
     //base case
     if(i>=n-1){
       return 0;
     }
        
     if(dp[i] != -1){
        return dp[i]; 
     }
        
     int ans = 10000;

     for(int j=arr[i]; j>0; j--){
            
        ans = min(ans , 1 + min_jump(arr,n,i+j,dp));   
      }
      
      return dp[i] = ans;
}

int main(){
   
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);

   vector<int> arr{3, 4, 2, 1, 2, 3, 7, 1, 1, 3};
   int n = arr.size();
   vector<int> dp(n+1,-1);
   cout<<min_jump(arr,n, 0, dp)<<endl;

   return 0;
}

/*

input:
[3, 4, 2, 1, 2, 3, 7, 1, 1, 3]
output:
4

input:
[2,3,1,1,4]
output:
2

*/

