#include<bits/stdc++.h>
using namespace std;

 int solve(vector<int>& nums,vector<int>& dp,vector<int>& count){
        int maxLen = 0;
        int ans = 0;
        for(int i =1; i<nums.size(); i++){
           for(int j=0; j<i;j++){
               if(nums[j] < nums[i]){
                  if(dp[i] < dp[j]+1){
                      dp[i] = dp[j]+1;
                      count[i] = count[j];
                  }
                  else if(dp[i] == dp[j]+1){
                      count[i] +=count[j];
                  }
               }
           }
           maxLen = max(maxLen, dp[i]);
        }


        for(int i=0; i<nums.size(); i++){
            if(dp[i] == maxLen){
                ans += count[i];
            }
        }

        return ans;
    }

    int findNumberOfLIS(vector<int>& nums) {
        int n = nums.size();
        if(n == 1){
            return 1;
        }
        vector<int> dp(n,1);
        vector<int> count(n,1);

        return solve(nums, dp,count);
    }

int main(){
   
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);
   
   vector<int> nums{1,3,5,4,7};

   cout<<findNumberOfLIS(nums)<<endl;
   

   return 0;
}

/*

Input: nums = [1,3,5,4,7]
Output: 2
Explanation: The two longest increasing subsequences are [1, 3, 4, 7] and [1, 3, 5, 7].


Input: nums = [2,2,2,2,2]
Output: 5
Explanation: The length of the longest increasing subsequence is 1, and there are 5 increasing subsequences of length 1, so output 5.



*/