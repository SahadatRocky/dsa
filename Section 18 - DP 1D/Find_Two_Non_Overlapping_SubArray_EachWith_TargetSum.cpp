#include<bits/stdc++.h>
using namespace std;
#define inf 1e9

int solve(vector<int>& arr,int target){
    int n = arr.size();
    vector<int> dp(n+1,inf); 
    int l = 0, r = 0, sum = 0;
    int ans = inf, len = inf;
    
    while(r < n){
      sum += arr[r];

      while(sum > target){
      	  sum -= arr[l];
      	  l++;
      }

      if(sum == target){

      	if(l !=0 && dp[l-1]!= inf){
      		ans = min(ans, dp[l-1]+r-l+1);
      	}
      	len = min(len,r-l+1);
      }
      dp[r] = len;
      r++; 
      
    } 
    return ans == inf ? -1 : ans;
}


int main(){
    
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    
    vector<int> arr{3,1,1,1,5,1,2,1};
    int target = 3;

    cout<<solve(arr,target)<<endl;


	return 0;
}


/*

Input: arr = [3,1,1,1,5,1,2,1], target = 3
 
Output: 3
 
Explanation: Note that sub-arrays [1,2] and [2,1] cannot be an answer because they overlap.


*/

