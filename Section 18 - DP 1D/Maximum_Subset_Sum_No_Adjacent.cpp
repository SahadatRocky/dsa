#include<bits/stdc++.h>
using namespace std;

int maxSubsetSumNoAdjacent(vector<int>& arr){
    
    int n = arr.size();
    vector<int> dp(n+1, 0);

    dp[0] = max(0, arr[0]);
    dp[1] = max(0, max(arr[0], arr[1]));

    for(int i=2; i<n; i++){
        int include = arr[i] + dp[i-2];
        int exclude = dp[i-1];

    	dp[i] = max(include, exclude);
    }
    return dp[n-1];
}

int main(){
   
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);
   
   vector<int> arr{6, 10, 12, 7, 9, 14};

   cout<<maxSubsetSumNoAdjacent(arr);

   return 0;
}

/*

input :
[6, 10, 12, 7, 9, 14]

output:
32

*/

