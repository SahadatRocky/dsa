#include<bits/stdc++.h>
using namespace std;

int LIS(vector<int>& nums,vector<int>& dp){
   
    for(int i=0; i<nums.size(); i++){
       for(int j=0; j<i; j++){

       	  if(nums[j] < nums[i]){
             dp[i] = max(dp[i], dp[j] + 1);
       	  }
       } 
    }
    
    int lis = 0;
    for(int i=0; i<nums.size(); i++){
        if(lis < dp[i]){
        	lis = dp[i];
        } 
    }

    return lis;
}

int main(){
    
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);
   
   vector<int> nums{10,9,2,5,3,7,101,18};
   int n = nums.size();

   vector<int> dp(n+1,1);

   cout<<LIS(nums,dp);

   return 0;
}

/*

Input: nums = [10,9,2,5,3,7,101,18]
Output: 4
Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4.


*/