#include<bits/stdc++.h>
using namespace std;

int climbingStair(int n,vector<int>& dp){
        
    //base case
    if(n == 0 || n == 1){
        return 1;
    }

    //memoization
    if(dp[n] != -1){
        return dp[n];
    }
        
    //rec case
    int ans = climbingStair(n-1,dp) + climbingStair(n-2,dp);    
    return dp[n] = ans;
}


int main(){
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);

   int n;
   cin>>n;
   vector<int>dp(n+1, -1);
   cout<<climbingStair(n,dp)<<endl; 
   
   return 0;
}

/*

Input: n = 2
Output: 2
Explanation: There are two ways to climb to the top.
1. 1 step + 1 step
2. 2 steps


Input: n = 3
Output: 3
Explanation: There are three ways to climb to the top.
1. 1 step + 1 step + 1 step
2. 1 step + 2 steps
3. 2 steps + 1 step



*/