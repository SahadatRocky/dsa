#include<bits/stdc++.h>
using namespace std;

//button up
#define maxSize 1e9+7
int numSquares(int n) {
    vector<int> dp(n+1,maxSize);
    
    dp[0] = 0;
    dp[1] = 1;

    for(int i=2; i<=n; i++){
    	int x = sqrt(i);
    	for(int j=1; j<=x; j++){
            
            int y = j*j;
            if(i-y >=0){
            	dp[i] = min(dp[i], dp[i-y] + 1);
            }
    	}
    }
    return dp[n];
}

  int  solve(int n,vector<int>& dp){
        
        if(n == 0){
            return 0;
        }
        
        if(dp[n] != -1){
            return dp[n];
        }
        
        int minimum = n; 
        for(int i = 1; i*i<=n; i++){
            minimum = min(minimum, 1 + solve(n-(i*i), dp));
            
        }
        
        return dp[n] = minimum;
        
}

int main(){
	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 12;
    cout<<numSquares(n);

    vector<int> dp(n+1, -1);
        
    cout<<solve(n,dp)<<endl;

    return 0;   
}

/*
Input: n = 12
 
Output: 3
 
Explanation: 12 = 4 + 4 + 4.

*/