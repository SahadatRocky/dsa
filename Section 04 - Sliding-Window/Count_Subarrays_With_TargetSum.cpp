#include<bits/stdc++.h>
using namespace std;

int cntSubarrays(vector<int>arr,int k){
    unordered_map<int, int> m;
    int n = arr.size();
    int result = 0;
    int sum = 0;
 
    for (int i = 0; i < n; i++) {
        sum += arr[i];
        if (sum == k)
            result++;
 
        if (m.find(sum - k) != m.end())
            result += m[sum - k];

        m[sum]++;
    }
 
    return result;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    
  
	return 0;
}

/*
Sample Input

arr = {10, 2, -2, -20, 10}
K = -10


Sample Output

3



Explanation

10 + 2 - 2 + -20 = 10

-20 + 10 = -10

2 + -2 + -20 + 10 = -10
*/
