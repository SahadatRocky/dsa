#include<bits/stdc++.h>
using namespace std;

int maxSum(int arr[],int n,int k){
    
    if(n<k){
    	return -1;
    }
    
    int mxsum=0;
    int j=0;
    while(j<k){
    	mxsum +=arr[j];
    	j++;
    }

    int windowSum = mxsum;
    for(int i=k; i<n; i++){
    	windowSum+=arr[i] - arr[i-k];
    	mxsum = max(mxsum, windowSum);
    }
    
    return mxsum; 
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int arr[]={100, 200, 300, 400};
    int n = sizeof(arr)/sizeof(int);
    int k;

    cout<<maxSum(arr,n,k)<<endl;
  
	return 0;
}

/*

Input  : arr[] = {100, 200, 300, 400}, k = 2
Output : 700

Input  : arr[] = {1, 4, 2, 10, 23, 3, 1, 0, 20}, k = 4 
Output : 39
We get maximum sum by adding subarray {4, 2, 10, 23} of size 4.

Input  : arr[] = {2, 3}, k = 3
Output : Invalid
There is no subarray of size 3 as size of whole array is 2.

*/