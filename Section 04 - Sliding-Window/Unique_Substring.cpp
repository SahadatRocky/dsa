#include<bits/stdc++.h>
using namespace std;

string uniqueSubString(string s){
     
     int i =0;
     int j =0;
     int window_len = 0;
     int max_window_len = 0;
     int start_window = -1;
     
     unordered_map<char, int> mp;

     while(j<s.size()){
         
        char ch  =  s[j];

        if(mp.count(ch) && mp[ch]>=i){
             
             i = mp[ch] + 1;
             window_len = j - i;
        }

        //update the last occurance
        mp[ch] = j;
        window_len++;
        j++;

        if(window_len > max_window_len){
             
             max_window_len = window_len;
             start_window = i;
           }
        }

    return s.substr(start_window, max_window_len);    
}



int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    string s;
    cin>>s;

    cout<<uniqueSubString(s)<<endl;
    
	return 0;
}


/*
  abcabeb

  cabe

*/