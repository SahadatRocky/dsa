#include<bits/stdc++.h>
using namespace std;

void housing(int arr[],int n,int k){
    
    int i=0;
    int j=0;
    int sum=0;

    while(j<n){
        sum+=arr[j];
        while(sum > k && i < j){
           sum -= arr[i]; 
           i++; 
        }

        if(sum == k){
            cout<<i<<" "<<j<<endl;
        }
        j++;
    }
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int arr[] = {1,3,2,1,4,1,3,2,1,1};
    int n = sizeof(arr)/sizeof(int);
    int k = 4;

    housing(arr,n,k);
    
    // for(int i=0; i<n; i++){
    // 	cout<<arr[i]<<" ";
    // }
    // cout<<endl;


	return 0;
}
