#include<bits/stdc++.h>
using namespace std;

/*
//Brute Force approch
vector<int> solve(vector<int>& arr,int k){
   vector<int> ans; 
   for(int i=0;i<arr.size()-k+1; i++){
         int mx = arr[i];
         for(int j=i; j<=i+k-1; j++){
            mx = max(mx, arr[j]);
         }
        ans.push_back(mx);
   }
   return ans;
}
*/

//using Dequeue

vector<int> solve(vector<int>& arr,int k){

     vector<int> output;
     deque<int> q(k); 

     for(int i=0; i<arr.size(); i++){
        while(!q.empty() && q.front() == (i-k)){
            q.pop_front();
        }
        while(!q.empty() && arr[i] >= arr[q.back()]){
            q.pop_back();
        }
        
        q.push_back(i);
        if(i>=k-1){
           output.push_back(arr[q.front()]);        
        }
    }
    return output;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int k = 3;
    vector<int> arr{1, 2, 3, 1, 4, 5, 2, 3, 6};

    vector<int> output = solve(arr,k);

    for(auto x : output){
        cout<<x<<" ";
    }
    cout<<endl;
  
	return 0;
}

/*

Sample Input

K = 3
input = {1, 2, 3, 1, 4, 5, 2, 3, 6}
Sample Output

output = {3,3,4,5,5,5,6}
Explanation

Maximum of 1, 2, 3 is 3

Maximum of 2, 3, 1 is 3

Maximum of 3, 1, 4 is 4

Maximum of 1, 4, 5 is 5

Maximum of 4, 5, 2 is 5

Maximum of 5, 2, 3 is 5

Maximum of 2, 3, 6 is 6

*/
