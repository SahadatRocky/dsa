#include<bits/stdc++.h>
using namespace std;

vector<int> solve(string s,string p){
      
     if(s.size() < p.size()){
     	return {};
     } 

     vector<int> ans;
     vector<int> ss(26,0);
     vector<int> pp(26,0);

     for(int i=0; i<p.size(); i++){
     	pp[p[i]-'a']++;
     	ss[s[i]-'a']++;
     }

     if(ss==pp){
     	ans.push_back(0);
     }

     for(int i=p.size(); i<s.size(); i++){
     	 ss[s[i]-'a']++;
     	 ss[s[i-p.size()]-'a']--;

     	 if(ss==pp){
     	 	ans.push_back(i+1-p.size());
     	 }
     }

     return ans;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string s = "cbaebabacd";
    string p = "abc";

    vector<int> output = solve(s,p);

    for(auto x : output){
    	cout<<x<<" ";
    }
    cout<<endl;
  
	return 0;
}


/*

Input: s = "cbaebabacd", p = "abc"
Output: [0,6]
Explanation:
The substring with start index = 0 is "cba", which is an anagram of "abc".
The substring with start index = 6 is "bac", which is an anagram of "abc".

Input: s = "abab", p = "ab"
Output: [0,1,2]
Explanation:
The substring with start index = 0 is "ab", which is an anagram of "ab".
The substring with start index = 1 is "ba", which is an anagram of "ab".
The substring with start index = 2 is "ab", which is an anagram of "ab".

*/