#include<bits/stdc++.h>
using namespace std;

string find_window(string s,string p){
   
   //array as a freq map or hashmap
   int FP[256] = {0};
   int FS[256] = {0};

   for(int i=0; i<p.size(); i++){
     FP[p[i]]++;
   } 

   int cnt =0;
   int start = 0; // left contraction
   int start_idx = -1; // start_idx for best window 
   int min_so_far = INT_MAX;
   int window_size = 0;
   //sliding window algorithm
   
   //expand the window by including the current character

   for(int i=0; i<s.size(); i++){

     char ch = s[i];

     FS[ch]++;

     //count how many character have been match till now (string and pattern)
     if(FP[ch] != 0 && FS[ch] <= FP[ch]){
          cnt += 1;
     }
     
     if(cnt == p.size()){

          //start contracting from the left to remove unwanted character

          while(FP[s[start]] == 0 || FS[s[start]] > FP[s[start]]){
               FS[s[start]]--;
               start++;
          }

          //note the window size
          window_size =i - start + 1;
          if(window_size < min_so_far){
             min_so_far = window_size;
             start_idx = start;
          }

     }
   } 
   
   if(start_idx == -1){
     return "No Window found";
   }

   return s.substr(start_idx,min_so_far);
}



int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    string s,p;
    cin>>s>>p;

    cout<<find_window(s,p)<<endl;
    
	return 0;
}


/*
hello
elo

ello

*/