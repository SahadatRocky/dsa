#include<bits/stdc++.h>
using namespace std;


int numSubarrayProductLessThanK(vector<int>& nums, int k) {
        
        int n = nums.size();
        int i=0;
        int j=0;
        int count=0;
        int product = 1;

        while(j<n){
            product *=nums[j];
            while(i<=j && product>=k){
                product/=nums[i];
                i++;
            }
            count += j-i+1;
            j++;
        }
        return count;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);

   return 0;
}    


/*

Input: nums = [10,5,2,6], k = 100
Output: 8
Explanation: The 8 subarrays that have product less than 100 are:
[10], [5], [2], [6], [10, 5], [5, 2], [2, 6], [5, 2, 6]
Note that [10, 5, 2] is not included as the product of 100 is not strictly less than k.


Input: nums = [1,2,3], k = 0
Output: 0

*/
