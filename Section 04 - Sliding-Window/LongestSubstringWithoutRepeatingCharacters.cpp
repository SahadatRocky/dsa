#include<bits/stdc++.h>
using namespace std;
int lengthOfLongestSubstring(string s) {
    int l = 0;
    int r = 0;
    int len = 0;
    set<int> st;
    int n = s.size();
    while(l<n && r<n){

        if(st.find(s[r]) == st.end()){

            len = max(len, r-l+1);
            st.insert(s[r]);
            r++;
        } 
        else{
            st.erase(s[l]);
            l++;
        }
    }
    return len;
}



int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    string s;
    cin>>s;

    cout<<lengthOfLongestSubstring(s)<<endl;
    
    return 0;
}


/*
Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.

Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.

*/