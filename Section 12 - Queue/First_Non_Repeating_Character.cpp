#include<bits/stdc++.h>
using namespace std;

vector<char> FindFirstNonRepeatingCharacter(vector<char> str){
    vector<char> v;
    queue<char> q;
    map<char, int> mp;
    
    for(int i=0; i<str.size(); i++){
        q.push(str[i]);
        mp[str[i]]++;
        
        while(!q.empty()){
            
            if(mp[q.front()] > 1){
                q.pop();
            }else{
                v.push_back(q.front());
                break;
            }
        }
        
        if(q.empty()){
            v.push_back('0');
        }
    }
    
    return v;
}


vector<char> FindFirstNonRepeatingCharacter(string s){
    
    queue<char> q;
    int freq[26] = {0};
    vector<char> ans;

    for(int i=0; i<s.size(); i++){
        q.push(s[i]);
        freq[s[i]-'a']++;
        //cout<<"freq"<<freq[s[i]-'a']<<endl;
        while(!q.empty()){
            char ch = q.front();
            if( freq[ch - 'a'] > 1 ){
               q.pop(); 
            }else{
               ans.push_back(ch);
               break;
            }
        }

        if(q.empty()){
        	ans.push_back('0');
        }
    }
    return ans;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    string s = "aabcbcd";
    
	vector<char> output = FindFirstNonRepeatingCharacter(s);
    
    for(auto x : output){
    	cout<<x<<" ";
    }
    cout<<endl;

	return 0;
}

/*

Sample Testcase :

Input:

aabcbcd

Output:

a 0 b b c 0 d

*/

