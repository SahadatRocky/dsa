#include<bits/stdc++.h>
using namespace std;

queue<int> interLeave(queue<int> q){
    
    int n = q.size();
    queue<int> q1;
    queue<int> q2;

    for(int i=0; i<n/2; i++){
    	q1.push(q.front());
    	q.pop();
    }

    for(int i=0; i<n/2; i++){
    	q2.push(q.front());
    	q.pop();
    }

   
    for(int i=0; i<n/2; i++){
    	q.push(q1.front());
    	q1.pop();
    	q.push(q2.front());
    	q2.pop();
    }

    return q;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    queue<int> q;
    q.push(11);
    q.push(12);
    q.push(13);
    q.push(14);
    q.push(15);
    q.push(16);
    q.push(17);
    q.push(18);
    q.push(19);
    q.push(20);

    queue<int> output = interLeave(q);

    while(!output.empty()){
    	cout<<output.front()<<" ";
    	output.pop();
    }
    
    return 0;
}


/*

Sample Input

11 12 13 14 15 16 17 18 19 20

Sample Output

11 16 12 17 13 18 14 19 15 20

*/