#include<bits/stdc++.h>
using namespace std;

class Queue{
    
    int *arr;
    int countElement;
    int maxSize;
    int Front;
    int Rear;

public:
	Queue(int size = 5){
       maxSize = size; 
       arr = new int[maxSize];
       countElement = 0;
       Front = 0;
       Rear = maxSize - 1;
	}

	bool full(){
		return countElement == maxSize;
	}

	bool empty(){
		return countElement == 0;
	}

	void push(int data){
       if(!full()){
          //take the rear to the next index
       	  Rear = (Rear + 1) % maxSize;
       	  arr[Rear] = data;
       	  countElement++;
       } 
	}

	void pop(){
		if(!empty()){
			Front = (Front + 1) % maxSize; 
			countElement--;
		}
	}

    int front(){
    	return arr[Front];
    }
};


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    Queue myqueue(7);
    myqueue.push(1);
    myqueue.push(2);
    myqueue.push(3);
    myqueue.push(4);
    myqueue.push(5);
    myqueue.push(6);
    myqueue.push(7);
    myqueue.push(8);
    myqueue.pop();
    myqueue.pop();
    myqueue.push(9);
    
    while(!myqueue.empty()){
       cout<< myqueue.front() <<" ";
       myqueue.pop();  
    }
    cout<<endl;

	return 0;
}
