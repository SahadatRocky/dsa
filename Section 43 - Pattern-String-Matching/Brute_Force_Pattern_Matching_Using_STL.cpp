#include<bits/stdc++.h>
using namespace std;

int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    string text = "ababaababbbbabaaa";
    string pattern = "aba";

    int n = text.size();
    int m = pattern.size();
   
    for(int i = 0 ; i+m <= n; i++){
        if(text.substr(i,m) == pattern){
           cout<<i<<" ";
        }
    }

    return 0; 
}