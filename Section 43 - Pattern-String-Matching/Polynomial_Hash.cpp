#include<bits/stdc++.h>
using namespace std;
#define ll long long int

ll poly_hash(string s){
   
   ll hash = 0, p =31, mod = 1e9+7;
   ll p_power = 1;
   for(ll i=0; i<s.size(); i++){
       hash += (s[i] - 'a' + 1) * p_power;
       p_power *= p;

       hash %= mod;
       p_power %= mod;
   }
   return hash;
}

int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    string s = "abababa";

    cout<<poly_hash(s)<<endl;

    return 0; 
}