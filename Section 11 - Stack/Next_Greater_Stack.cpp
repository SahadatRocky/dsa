#include<bits/stdc++.h>
using namespace std;

vector<int> next_greater(vector<int>& arr){
    
    int n = arr.size();
    vector<int> arr1(n,0);
    stack<int> s;

    for(int i=n-1; i>=0; i--){

       while(!s.empty() && s.top() <= arr[i]){
       	 s.pop();
       }

       if(s.empty()){
       	  arr1[i] = -1;
       }else{
       	  arr1[i] = s.top();
       }
       s.push(arr[i]);
    }
    
    return arr1;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> v{ 4, 5, 2, 25 };

    vector<int> output = next_greater(v);

    for(auto x: output){
    	cout<<x<<" ";
    }
    cout<<endl;
    
	return 0;
}


/*

Sample Input

v = { 4, 5, 2, 25 }
Sample Output

{ 5, 25, 25, -1 }


*/