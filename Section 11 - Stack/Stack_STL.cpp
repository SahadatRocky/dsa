#include<bits/stdc++.h>

using namespace std;

int main(){


    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    stack<string> st;

    st.push("c++");
    st.push("java");
    st.push("python");
    st.push("os");
    
    while(!st.empty()){
        cout<<st.top()<<endl;
        st.pop();
    }
    
	return 0;
}
