#include<bits/stdc++.h>
using namespace std;

int reverse(int n){
   
   stack<int> st;

   while(n != 0){
       int rem = n % 10;
       st.push(rem);
       n = n / 10;
   }

   int rev = 0;
   int p = 1;

   while(!st.empty()){

       rev = rev + st.top() * p;
       st.pop();
       p = p * 10;
   }
   
   return rev;
}

int main(){


    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int n;
    cin>>n;
    cout<<reverse(n)<<endl;
    
	return 0;
}

/*

Sample Input

456
Sample Output

654

*/