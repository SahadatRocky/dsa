#include<bits/stdc++.h>
using namespace std;

bool checkRedundant(string str){
    
    bool operator_found = false;
    stack<int> s;
    for(auto ch : str){
        if(ch != ')'){
            s.push(ch);
        }else{
           
           while(!s.empty() && s.top() == '('){
               char top = s.top();
               s.pop();
               if(top == '+' or top == '-' or top == '*' or top == '/'){
                operator_found = true;
               }
           }
           s.pop();
           if(operator_found == false){
            return true;
           }
        }
    }

    return false;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    string s = "((a+b)+c)";

    cout<<checkRedundant(s)<<endl;
    
	return 0;
}
