#include<bits/stdc++.h>
using namespace std;

int getMaxArea(vector<int> hist){
    int n = hist.size();
    stack<int> s;
 
    int max_area = 0; 
    int i = 0;
    int area_with_top;
    int top;
    while (i < n)
    {
        if (s.empty() || hist[s.top()] <= hist[i]){
            s.push(i++);
        }
        else
        {
            top = s.top();  
            s.pop();  
            area_with_top = hist[top] * (s.empty() ? i : 
                                            i - s.top() - 1);
            
            max_area = max(max_area, area_with_top);
        }
    } 

    while (!s.empty())
    {
        top = s.top();
        s.pop();
        area_with_top = hist[top] * (s.empty() ? i :
                                i - s.top() - 1);
 
         max_area = max(max_area, area_with_top);
    }
    return max_area;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> arr{6, 2, 5, 4, 5, 1, 6};
    cout<<getMaxArea(arr)<<endl;
}

/*

Sample Input

 {6, 2, 5, 4, 5, 1, 6}
Sample Output

12 


*/