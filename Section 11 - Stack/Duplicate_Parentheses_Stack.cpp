#include<bits/stdc++.h>
using namespace std;

bool duplicateParentheses(string str){
  
  stack<char> s;
  for(auto ch : str){
    
    if(ch == ')'){
       
       char top = s.top();
       s.pop();

       int elementInside = 0;

       while(top != '('){
           
           elementInside++;
           top = s.top();
           s.pop();  
       } 

       if(elementInside < 1){
          return true;
       }
    }else{
        s.push(ch);
    }
  }  

  return false;

}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string str = "((a+b)+((c+d)))";
    cout<<duplicateParentheses(str)<<endl;

}


/*

Sample Input 1

((a+b)+((c+d)))
Sample Output 1

true
Sample Input 2

(((a+(b)))+(c+d))
Sample Output 2

true

*/