#include<bits/stdc++.h>
using namespace std;

bool isBalance(string str){
    
    stack<char> s;

    for(auto ch : str){

        if(ch == '(' || ch == '{' || ch == '['){
            s.push(ch);
        }

        else if(ch == ')'){
            if(!s.empty() && s.top() == '('){
                s.pop();
            }else{
                return false;
            }
        }

        else if(ch == '}'){
            if(!s.empty() && s.top() == '{'){
                s.pop();
            }else{
                return false;
            }
        }
        
        else if(ch == ']'){
            if(!s.empty() && s.top() == '['){
                s.pop();
            }else{
                return false;
            }
        }
    }

    return s.empty() ? true : false;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    string s = "((a+b+c)+[d])";

    cout<<isBalance(s)<<endl;
    
	return 0;
}
