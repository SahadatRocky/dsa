#include<bits/stdc++.h>
using namespace std;

void insertAtBottom(stack<int> &st , int data){
    //base case 
    if(st.empty()){
        st.push(data);
        return;
    }
    // rec case

    int t = st.top();
    st.pop();
    insertAtBottom(st,data);
    st.push(t);
}

void reverse(stack<int> &st){
   
   if(st.empty()){
    return;
   }

   int t = st.top();
   st.pop();
   reverse(st);
   insertAtBottom(st,t);
}


int main(){


    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    stack<int> st;

    st.push(1);
    st.push(2);
    st.push(3);
    st.push(4);

    // insertAtBottom(st,5);
    reverse(st);
    
    while(!st.empty()){
        cout<<st.top()<<endl;
        st.pop();
    }
    
	return 0;
}
