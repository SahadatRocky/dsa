#include<bits/stdc++.h>
using namespace std;

void stockSpan(int prices[], int n, int span[]){
    stack<int> s;
    s.push(0);

    span[0] = 1;

    //loop for the rest of the days
    for(int i=1; i<n; i++){
        int current_price = prices[i];

        while(!s.empty() && prices[s.top()] <= current_price){
            s.pop();
        }  

        if(!s.empty()){
             int prev_height = s.top();
             span[i] = i - prev_height;
        }else{
            span[i] = i + 1;  
        }
         
        s.push(i);
    }
}

int main(){


    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int prices[] = {100, 80, 60, 70, 60, 75, 85};
    int n = sizeof(prices)/ sizeof(int);
    int span[100000] = {0};

    stockSpan(prices,n,span);

    for(int i=0; i<n; i++){
    	cout<<span[i]<<" ";
    }
    cout<<endl;
    
	return 0;
}