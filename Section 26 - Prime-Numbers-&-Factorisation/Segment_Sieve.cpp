#include <bits/stdc++.h>
#define ll long long int

using namespace std;

const int N = 1000000;
vector<int> primes;

void primeSeive(vector<int>& sieve){
    sieve[0] = 0;
    sieve[1] = 0;

    for(ll i=2; i<=N; i++){
        if(sieve[i]){
            primes.push_back(i);
            //making all multiples of i as non prime
            for(ll j = i*i; j<=N; j = j+i){

                sieve[j] = 0; //non prime
            }
        }
    }
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> sieve(N+1,1);
    primeSeive(sieve);
    
    int t;
    cin>>t;
    while(t--){
       int m,n;
       cin>>m>>n;
       vector<int> segment(n-m+1, 0);
       
       //iterate over the primes,mark multiples of prime in segment array as non prime (1)
       for(auto p : primes){
           
           //stop when p^2 > n
           if(p*p > n){
              break;
           }

           //otherwise we need to find the nearest starting point
           int start = (m/p) * p;

           //don't start from 0 ,instead 2 * prime

           if(p>=m && p<=n){
              start = 2 * p;
           }
           
           //start marking the numbers as not prime from start
           for(int j = start; j<=n; j = j+p){
                
                if(j < m){
                    continue;
                }

                //non-prime
                segment[j-m] = 1;
           }
       }
       
       //prime stored as 0 in the segment
       for(int j=m; j<=n; j++){
        if(segment[j-m] == 0 && j !=1){
            cout<<j<<" ";
        }
      }
      cout<<endl;
    }

    return 0;
}