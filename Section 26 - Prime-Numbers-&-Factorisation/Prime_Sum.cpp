#include <bits/stdc++.h>
using namespace std;

int primeSum(int n){
    
   vector<int>prime(n+1,1);
    for(int i=2;i<=n;i++){
        if(prime[i]){
            for(int j=i*i;j<=n;j+=i){
                prime[j]=0;
            }
        }
    }
    int ans=0;
    for(int i=2;i<=n;i++){
        if(prime[i]==1 && prime[i+2]==1){
            ans++;
        }
    }
    return ans;
}


int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 5;
    cout<<primeSum(n)<<endl;

    return 0;
}

/*

Input: n= 5
 
Output: 1
 
Explanation:
In this case only pair (l, r) suits us - (2,3), because 2+3=5 , 5 is prime.

*/