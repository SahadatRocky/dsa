#include <bits/stdc++.h>
using namespace std;

#define ll long long int
const int N = 1000000;

void primeSeive(vector<int> &sieve){
    
    for(int i=1; i<=N; i++){
        sieve[i] = i;
    }

    for(ll i=2; i<=N; i++){
       if(sieve[i] == i){
       	  for(ll j = i*i; j<=N; j = j + i){
            if(sieve[j] == j){  
                sieve[j] = i;
            }
       	  }
       }
    }
}

vector<int> getFactorisation(int n,vector<int>& sieve){
    vector<int> factors;
    while(n!=1){
        if(sieve[n]){
            factors.push_back(sieve[n]);
            n = n/sieve[n];   
        }
    }

    return factors;
}


int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> sieve(N+1, 1);

    primeSeive(sieve);

    int n = 30;
    vector<int> output = getFactorisation(n,sieve);
    

    for(auto x : output){
       cout<<x<<",";
    }

    return 0;
}