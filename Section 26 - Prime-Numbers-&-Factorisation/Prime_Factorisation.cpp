#include <bits/stdc++.h>
using namespace std;

void prime_factorisation(int n){
   
   for(int i = 2; i<=n; i++){
   	  if(n%i == 0){
   	  	int cnt = 0;
   	  	while(n%i == 0){
            n = n/i;
            cnt++; 
   	  	}
   	  	cout<<i<<"^"<<cnt<<","<<endl;
   	  }
   }
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 30;

    prime_factorisation(n);

    return 0;
}