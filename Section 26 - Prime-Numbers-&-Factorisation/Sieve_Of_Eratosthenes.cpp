#include <bits/stdc++.h>
using namespace std;

#define ll long long int
const int N = 1000000;

void primeSeive(vector<int> &sieve){
    sieve[0] = 0;
    sieve[1] = 0;

    for(ll i=2; i<=N; i++){
       if(sieve[i]){
          //making all multiples of i as non prime
       	  for(ll j = i*i; j<=N; j = j + i){
              sieve[j] = 0; //non prime
       	  }
       }
    }
}


int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> sieve(N+1, 1);

    primeSeive(sieve);
    
    for(int i =0; i<=100; i++){
       
       if(sieve[i]){
       	cout<<i<<" ";
       }
    }

    return 0;
}