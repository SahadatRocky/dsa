#include <bits/stdc++.h>
using namespace std;

bool isPrime(int n){
    
    if(n<=1){
    	return false;
    }

    for(int i = 2; i<n; i++){
    	if(n%i == 0){
    		return false;
    	}
    }

    return true;
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 11;
    // isPrime(n) ? cout<<"True" : cout<<"False"<<endl;
    for (int i = 1; i <= n; i++) {
        // check if current number is prime
        if (isPrime(i))
            cout << i << " ";
    }
    return 0;
}