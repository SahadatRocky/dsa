#include <bits/stdc++.h>
using namespace std;


vector<int> closestDivisors(int num) {
      vector<int>ans;
       int a = num + 1;
       int b = num + 2;
        for(int i=sqrt(b);i>0;i--){
            if(a%i==0){
                ans.push_back(i);
                ans.push_back(a/i);   
                break;
            } 
            if(b%i==0){
                ans.push_back(i);
                ans.push_back(b/i);
                break;
            }
        }
    
      return ans;
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    int num = 8;
    vector<int> output = closestDivisors(num);

    for(auto x : output){
      cout<<x<<" ";
    }

    return 0;
}

/*

Input: num = 8
 
Output: [3,3]
 
Explanation: For num + 1 = 9, the closest divisors are 3 & 3, for num + 2 = 10, the closest divisors are 2 & 5, hence 3 & 3 is chosen.

*/