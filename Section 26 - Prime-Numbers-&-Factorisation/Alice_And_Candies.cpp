#include <bits/stdc++.h>
using namespace std;

int solve(int n){
    int cnt = 0;
    for(int i=1; i<=sqrt(n); i++){
        if(n%i == 0){
            int x = i;
            int y = n/i;
            if( (x+y)%2 ==0 ){
                cnt++;
            }
        }
    }
    
    return cnt;
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 45;
    cout<<solve(n)<<endl;

    return 0;
}

/*

Input: edges = 45
 
Output: 3
 
Explanation:  
Alice can buy 3 set of candy packets.
 
1. {5,7,9,11,13}
 
2. {13,15,17}
 
3. {45}

*/