#include<bits/stdc++.h>
using namespace std;

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    deque<int> dq(10);

    for(int i=0; i<10; i++){
        dq[i] = i * i;
    }

    dq.pop_back();
    dq.pop_front();

    dq.push_back(123);
    dq.push_front(50);
    
    cout<<dq.size()<<endl;



    for(auto x : dq){
        cout<<x<<" ";
    }
    cout<<endl;
  
	return 0;
}