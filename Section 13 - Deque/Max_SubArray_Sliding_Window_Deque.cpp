#include<bits/stdc++.h>
using namespace std;

void maxSubArrayK(vector<int> &arr,int n,int K){
    
    deque<int> Qi(K);
    int i;
    for (i = 0; i < K; ++i) {
        while ((!Qi.empty()) && arr[i] >= arr[Qi.back()]){
            Qi.pop_back();
        }    
        Qi.push_back(i);
    }
 
    for (; i < n; ++i) {

        cout << arr[Qi.front()] << " ";
        while ((!Qi.empty()) && Qi.front() <= i - K){
            Qi.pop_front();
        }    
        while ((!Qi.empty()) && arr[i] >= arr[Qi.back()]){
            Qi.pop_back();
        }
        Qi.push_back(i);
        
    }
    cout << arr[Qi.front()];
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<int> arr{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int n = arr.size();
    int k = 3;
    maxSubArrayK(arr,n,k);

	return 0;
}

/*
input:
1 2 3 4 5 6 7 8 9 10

output:
3 4 5 6 7 8 9 10

*/