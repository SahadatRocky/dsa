#include<bits/stdc++.h>
#include "trie.h"
using namespace std;


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    string words[] = {"hello","he","apple","aple","news"};
	Trie t;

	for(auto word:words){
		t.insert(word);
	}
    
    string key;
	cin>>key;
	cout<< t.search(key) <<endl;
	return 0;
}
/*

key = "he";
output : 1;

*/

