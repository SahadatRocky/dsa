
#include<bits/stdc++.h>
using namespace std;

struct node {
	node *next[26];
	node() {
		for (int i = 0; i < 26; i++) next[i] = NULL;
	}
};

node *root;

void insert_trie(string s) {
	node *temp = root;
	for (int i = 0; i < s.size(); i++) {
		int d = s[i] - 'a';

		if (temp->next[d] == NULL) {
			temp->next[d] = new node();
		}

		temp = temp->next[d];
	}
}


bool search_trie(string s) {
	node *temp = root;
	for (int i = 0; i < s.size(); i++) {
		int d = s[i] - 'a';
		if (temp->next[d] == NULL) return false;
		temp = temp->next[d];
	}
	return true;
}


int main(){

    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
   
    root = new node();

	string text = "ababba";
	string pat = "aba";

	int n = text.size();
	for (int i = 0; i < n; i++) {
		insert_trie(text.substr(i));
	}

	if (search_trie(pat)) {
		cout << "Found";
	}
	else {
		cout << "Not found";
	}

 
   return 0;
}