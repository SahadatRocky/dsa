#include<bits/stdc++.h>
using namespace std;

struct node{
    
    node *next[26];
    bool is_end;

    node(){
      for(int i=0; i<26;i++){
         next[i] = NULL;
      }
      is_end = false;
    }

};

node *root;

void insert_trie(string s){
   node *temp = root;
   for(int i=0; i<s.size(); i++){
      int d = s[i] - 'a';
      if(temp->next[d] == NULL){
         temp->next[d] = new node();
      }
      temp = temp ->next[d]; 
   }
   temp->is_end = true;
}

bool search_trie(string s){

   node *temp = root;
   for(int i=0; i<s.size(); i++){
      int d = s[i] - 'a';
      if(temp->next[d] == NULL){
         return false;
      }
      temp = temp ->next[d]; 
   }
   return temp->is_end;
}


int main(){

   freopen("input.txt", "r", stdin);
   freopen("output.txt", "w", stdout);

   root = new node();

   int n;
   cin >> n;
   for (int i = 0; i < n; i++) {
      string s;
      cin >> s;
      insert_trie(s);
   }

   if (search_trie("apple")) {
      cout << "Found";
   }
   else {
      cout << "Not Found";
   }
   return 0;
}