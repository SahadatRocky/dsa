#include<bits/stdc++.h>
using namespace std;

class node{

public:
    char data;
    unordered_map<char,node*> m;
    bool isTerminal;

    node(char data){
        data=data;
        bool isTerminal=false;
    }
};
class Trie{
   
public:
    node*root= new node('\0');
    
    void insert(string str){

        node*temp=root;
        for(char ch : str){
           if(temp->m.count(ch)==0){
               node* n = new node(ch);
               temp->m[ch]=n;
           }
           temp=temp->m[ch];
        }
        temp->isTerminal=true;
        return;
    }

    void dfs(node*temp, vector<string> &v, string word ){
        if(temp->isTerminal){
            v.push_back(word);
        }
        if(temp->m.empty()){
            return;
        }
        for(auto p:temp->m){
          word.push_back(p.first);
          dfs(temp->m[p.first],v,word);
          word.pop_back();
        }
        return;
    }

    vector<string> find(string str){
        vector<string> v;
        node* temp=root;
        string word="";
        for(int i=0; i<str.length(); i++){
           if(temp->m.count(str[i])==0){
               return v;
           }
           word.push_back(str[i]);
           temp=temp->m[str[i]];
        }
        if(temp->isTerminal){
            v.push_back(word);
        }
        dfs(temp,v,word);
        sort(v.begin(),v.end());
        return v;
    }
};
vector<string> findPrefixStrings(vector<string> words, string prefix){
    Trie t;
    for(auto s : words){
        t.insert(s);
    }
    vector<string> res = t.find(prefix);
    return res;
    
}



int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<string> words{"abc","abd","acde","abe","aeb","abba"};
    string small="ab";
    vector<string> output = findPrefixStrings(words, small);
    
    for(auto x: output){
    	cout<<x<<" ";
    } 
    cout<<endl;

	return 0;
}
/*

vector<string> words={"abc","abd","acde","abe","aeb","abba"};

string small="ab";

vector<string> output={"abba","abc","abd","abe"};

*/