#include<bits/stdc++.h>
using namespace std;
#define int long long int

const int p = 31, mod = 1e9+7;

int poly_hash(string s){
    
    int hash = 0;
    int p_power = 1;

    for(int i=0;i<s.size(); i++){
    	hash += (s[i] - 'a' + 1) * p_power;
    	p_power *= p;
    	hash %= mod;
    	p_power %= mod;
    }

    return hash; 
}


int powr(int a, int b){
    
    int res = 1;

    while(b){
       if(b&1){
       	 res = res * a;
       }
       b = a / 2;
       a = a * a;

       a %= mod;
       res %= mod;

    }

    return res;

}

int inv(int x){
	return powr(x, mod - 2);
}

int32_t main(){

    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    
    string text = "ababbabbaba";
    string pat = "aba";
    int m = pat.size();
    int n = text.size();
    int hash_pat = poly_hash(pat);
    int hash_text = poly_hash(text.substr(0,m));
    
    if(hash_text == hash_pat){
    	cout<<0<<endl;
    }

    for(int i=1; i+m<=n; i++){
         
        hash_text = (hash_text - (text[i] - 'a' + 1)+mod)%mod;
        hash_text = (hash_text * inv(p)) % mod;
        hash_text = (hash_text + (text[i+m-1] - 'a' + 1) * powr(p, m-1)) % mod;

        if(hash_text == hash_pat){
    	cout<<i<<endl;
      } 
    }

    return 0;
 }   