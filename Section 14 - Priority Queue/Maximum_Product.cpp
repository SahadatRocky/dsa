#include<bits/stdc++.h>
using namespace std;

int maxProduct(vector<int>& nums){
   priority_queue<int> pq;

   for(int i=0; i<nums.size(); i++){
   	   pq.push(nums[i]-1);
   }

   int p = pq.top();
   pq.pop();

   return p * pq.top();
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<int> nums{3,4,5,2};
    cout<<maxProduct(nums)<<endl;
	return 0;
}

/*

Sample Input

nums = [3,4,5,2]


Sample Output

12


Explanation

(nums[1]-1) * (nums[2]-1) = (4-1) * (5-1) = 12


*/