#include<bits/stdc++.h>
using namespace std;

int solve(vector<int>& nums){
    
    priority_queue<int, vector<int>, greater<int> > pq;
    for(int i=0; i<nums.size(); i++){
    	pq.push(nums[i]);
    }

    int sum = 0;
    while(pq.size() > 1){

    	int n1 = pq.top();
    	pq.pop();
    	int n2 = pq.top();
    	pq.pop();
        
        sum += n1 + n2;
        pq.push(n1 + n2);  

    }
    
    return sum;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums{1, 2, 3, 4, 5};
    cout<<solve(nums)<<endl;
    return 0;   
}  
  
/*

Input

nums = [1, 2, 3, 4, 5]
Output

33
Explanation

We take 1 and 2 out to get [3, 4, 5, 3]

We take 3 and 3 out to get [4, 5, 6]

We take 4 and 5 out to get [6, 9]

We take 6 and 9 out to get [15]

The sum is 33 = 1 + 2 + 3 + 3 + 4 + 5 + 6 + 9

*/