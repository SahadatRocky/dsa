#include<bits/stdc++.h>
using namespace std;

int maximumScore(int a,int b,int c){
   
   priority_queue<int> pq;

   pq.push(a);
   pq.push(b);
   pq.push(c);
   
   int count = 0;
   while(pq.size() >=2){

   	   int x = pq.top();
   	   pq.pop();
   	   int y = pq.top();
   	   pq.pop();

   	   x--;
   	   y--;

   	   if(x!=0){
   	   	 pq.push(x);
   	   }

   	   if(y!=0){
         pq.push(y); 
   	   }

   	   count++;
   }

   return count;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    int a = 2, b = 4, c = 6;

    cout<<maximumScore(a,b,c)<<endl;
    return 0;   
}

/*

Input: a = 2, b = 4, c = 6
 
Output: 6
 
Explanation: The starting state is (2, 4, 6). One optimal set of moves is:
- Take from 1st and 3rd piles, state is now (1, 4, 5)
- Take from 1st and 3rd piles, state is now (0, 4, 4)
- Take from 2nd and 3rd piles, state is now (0, 3, 3)
- Take from 2nd and 3rd piles, state is now (0, 2, 2)
- Take from 2nd and 3rd piles, state is now (0, 1, 1)
- Take from 2nd and 3rd piles, state is now (0, 0, 0)
There are fewer than two non-empty piles, so the game ends. Total: 6 points.

*/