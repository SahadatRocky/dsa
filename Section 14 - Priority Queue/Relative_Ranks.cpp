#include<bits/stdc++.h>
using namespace std;

string to_string(int a){
  
  string ans;
  stringstream ss;
  ss<<a;
  ss>>ans;

  return ans;
}

vector<string> findRelativeRanks(vector<int>& score) {
    // your code goes here
    priority_queue<pair<int,int>> pq;
        for(int i=0; i<score.size(); i++){
            pq.push({score[i],i});
        }
       
    int n= score.size();
    
    vector<string> vec(n);
   
    
    int cnt=0;
    
    
    while(!pq.empty()){
        cnt++;
        
        if(cnt==1){
            //cout<<"hey"<<endl;
            vec[pq.top().second].append("Gold Medal");
        }
        else if(cnt==2){
            vec[pq.top().second].append("Silver Medal");
        }
        else if(cnt==3){
            vec[pq.top().second].append("Bronze Medal");
        }
        else {
            vec[pq.top().second].append(to_string(cnt));
        }
        pq.pop();
    }
    return vec;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<int> score{5,4,3,2,1};

    vector<string> output = findRelativeRanks(score);

    for(auto x: output){
    	cout<<x<<" ";
    }
    cout<<endl;
    
	return 0;
}


/*

Input 1:

score = [5,4,3,2,1]

Output 1:

["Gold Medal","Silver Medal","Bronze Medal","4","5"]

*/