#include<bits/stdc++.h>
using namespace std;

class Compare{
   
public:
   bool operator()(int a, int b){
   		return a > b;
   }
};

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    // priority_queue<int, vector<int>, greater<int>> pq; // min-heap
    // priority_queue<int, vector<int>, Compare > pq;   //custom
    priority_queue<int> pq; // max-heap
    int arr[] = {10,15,20,13,6,90};
    int n = sizeof(arr)/sizeof(int);

    for(int x : arr){
    	pq.push(x);
    }

    while(!pq.empty()){
        
        cout<<pq.top()<<endl;
        pq.pop();
    }
    
	return 0;
}
