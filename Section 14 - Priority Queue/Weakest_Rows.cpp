#include<bits/stdc++.h>
using namespace std;


vector<int> kWeakestRows(vector<vector<int>>& mat, int k) {
  priority_queue <pair<int,int>, vector<pair<int,int>>, greater<pair<int,int>> > pq; //Min-heap
        for(int i=0;i<mat.size();i++)
        {
            int count=0;
            for(int j=0;j<mat[i].size();j++)
            {
                if(mat[i][j]==1)
                {
                    count++; //Counting the number of soldiers in each case
                }
            }
            pq.push(make_pair(count,i));
        }
        vector<int> x;
        while(k>0)
        {
            pair<int,int> temp=pq.top();
            x.push_back(temp.second);
            pq.pop();
            k--;
        }
        return x;
    
}



int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int k = 2;
    vector<vector<int>> mat{
    	{1,0,0,0},
    	{1,1,1,1},
    	{1,0,0,0},
    	{1,0,0,0},
    };

    vector<int> output = kWeakestRows(mat,k);

    for(auto x: output){
    	cout<<x<<" ";
    }
    cout<<endl;
    
	return 0;
}

/*

Sample Input

m = 4
 
n = 4
 
[[1,0,0,0],
 
[1,1,1,1],
 
[1,0,0,0],
 
[1,0,0,0]],
 
k = 2
Sample Output

[0,2]


Explanation

weakness of the rows is in the order- 0, 2, 3, 1



*/