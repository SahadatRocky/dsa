#include<bits/stdc++.h>
using namespace std;


int joining_ropes(vector<int>& ropes,int n){

   
   priority_queue<int , vector<int>, greater<int>> pq(ropes.begin(), ropes.begin() + n);

   int cost = 0;

   while(pq.size() > 1){
      int a = pq.top();
      pq.pop();

      int b = pq.top();
      pq.pop();

      int new_rope = a+b;
      cost = cost + new_rope;
      pq.push(new_rope);
   }

   return cost;
}



int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<int> ropes{4,3,2,6};
    int n = 4;
    cout<<joining_ropes(ropes,n)<<endl;


   
	return 0;
}

/*
input :
n = 4;
rope = [4,3,2,6]

output:
29

*/