#include<bits/stdc++.h>
using namespace std;

class MedianHandler{
public:
    float median;
    
    priority_queue<int,vector<int>, greater<int> > right; 
    priority_queue<int> left; //max heap
    
    void push(int number){
        //Complete this method to update median after every insertion 
        if(left.empty() and right.empty()){
            median = number;
            left.push(number);
        }
        
        else if(left.size()==right.size()){
            if(number <= median){
                left.push(number);
                median = left.top();
            }
            else{
                right.push(number);
                median = right.top();
            }
        }
        else if(left.size()>right.size()){
            
            if(number <= median){
                right.push(left.top());
                left.pop();
                left.push(number);
            }
            else{
                right.push(number);
            }
            median = (left.top() + right.top())/2.0;
        }
        else{
            if(number > median){
                left.push(right.top());
                right.pop();
                right.push(number);
            }
            else{
                left.push(number);
            }
            median = (left.top() + right.top())/2.0;
        }
        
    }
    float getMedian(){
        //Should return the median in O(1) time
        return median;
    }
};




// ============================

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    priority_queue<int> leftheap; //maxheap
    priority_queue<int,vector<int>,greater<int> > rightheap; //min heap

    int d;
    cin>>d;
    leftheap.push(d);

    float med = d;
    cout<< med <<" ";

    cin>>d;
    while(d!=-1){
        //left or right or equal
        if(leftheap.size() > rightheap.size()){
            if(d < med){
                rightheap.push(leftheap.top());
                leftheap.pop();
                leftheap.push(d);
            }
            else{
                rightheap.push(d);
            }
            med = (leftheap.top() + rightheap.top())/2.0;
        }
        else if(leftheap.size()==rightheap.size()){
            if(d < med){
                leftheap.push(d);
                med = leftheap.top();
            }
            else{
                rightheap.push(d);
                med = rightheap.top();
            }

        }
        else{
            if(d < med){
                leftheap.push(d);
            }
            else{
                leftheap.push(rightheap.top());
                rightheap.pop();
                rightheap.push(d);
            }
            med = (leftheap.top() + rightheap.top())/2;

        }

        cout<< med<<" ";
        cin>>d;
    }

	return 0;
}


/*

Input 1:

score = [10,5,2,3,0,12,18,20,22,-1]

Output 1:

[10,7.5,5,4,3,4,5,7.5,10]

*/