#include<bits/stdc++.h>
using namespace std;

int minSetSize(vector<int>& arr) {
       
       int n=arr.size();
        priority_queue<int> q;
        map<int,int> mp;
        for(int i=0; i<n; i++){
            mp[arr[i]]++;
        }
        for(auto pair:mp){
            q.push(pair.second);
        }
        int sum=0;
        int cnt=0;
        while(n-sum>n/2){
            sum+=q.top();
            q.pop();
            cnt++;
            
        }
        return cnt; 
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    vector<int> arr ={3,3,3,3,5,5,5,2,2,7};
    cout<<minSetSize(arr)<<endl;
    
	return 0;
}

/*

Input: arr = [3,3,3,3,5,5,5,2,2,7]

Output: 2

Explanation:

Choosing {3,7} will make the new array [5,5,5,2,2] which has size 5 (i.e equal to half of the size of the old array).

Possible sets of size 2 are {3,5},{3,2},{5,2}.

Choosing set {2,7} is not possible as it will make the new array [3,3,3,3,5,5,5] which has size greater than half of the size of the old array

*/