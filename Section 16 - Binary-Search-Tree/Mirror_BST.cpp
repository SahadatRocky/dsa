#include<bits/stdc++.h>
using namespace std;

class Node
{
  public:
   int data;
   Node *left;
   Node *right;

   Node(int data){
       this->data = data;
       left = right  = NULL;
   }
};

node *mirror(node* root){
    
    if(root == NULL){
        return NULL;
    }
    else{
        
        node* temp;
        temp = root->left;
        root->left = root->right;
        root->right = temp;
        
        mirror(root->left);
        mirror(root->right);
    }
}

node* mirrorBST(node* root){
    //complete this method
    
    mirror(root);
    return root;
}