#include<bits/stdc++.h>
using namespace std;

class Node
{
  public:
   int data;
   Node *left;
   Node *right;

   Node(int data){
       this->data = data;
       left = right  = NULL;
   }
};

bool isBSTUtil(Node* root, int min, int max)
{

    if (root==NULL)
        return true;
             
    if (root->data < min || root->data > max)
        return false;
     
    return
        isBSTUtil(root->left, min, root->data) &&
        isBSTUtil(root->right, root->data, max); 
}

bool isBST(Node * root){
    
    return isBSTUtil(root, INT_MIN,INT_MAX);
}
