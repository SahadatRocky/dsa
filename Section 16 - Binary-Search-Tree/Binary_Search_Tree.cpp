#include<bits/stdc++.h>
using namespace std;

class node{
   
public:
	int data;
	node*left;
	node*right;

	node(int data){
		this->data = data;
		this->left =  NULL;
		this->right = NULL;
	}
};

node* insert(node* root,int key){
    
    //base case
    if(root == NULL){
       return new node(key);
    }
    //rec case

    if(key < root->data){
    	root->left = insert(root->left, key);
    }
    else{
    	root->right = insert(root->right, key);
    }

    return root;
}

void printInorder(node* root){
    if(root == NULL){
    	return;
    }

    printInorder(root->left);
    cout<<root->data<<" ";
    printInorder(root->right);
}

void levelOrderPrint(node* root){
	queue<node*> q;
    q.push(root);
    q.push(NULL);

    while(!q.empty()){

    	node* temp = q.front();
    	q.pop();
    	if(temp == NULL){
             cout<<endl;
          
             if(!q.empty()){
             	q.push(NULL);
             }

    	}else{
            cout<<temp->data<<" ";
            if(temp->left){
            	q.push(temp->left);
            }
            if(temp->right){
            	q.push(temp->right);
            }

    	}
    }
   return;
}

bool search(node* root,int key){
   if(root == NULL){
   	return false;
   } 

   if(root->data == key){
   	  return true;
   }

   if(key < root->data){
      return search(root->left,key);
   }

   return search(root->right,key);
}

node* findMin(node* root){
    while(root->left != NULL){
       root = root->left;  
    }
    return root;
}


node* remove(node* root,int key){
		if(root==NULL){
			return NULL;
		}
		else if(key < root->data){
			root->left = remove(root->left,key);
		}
		else if(key > root->data){
			root->right = remove(root->right,key);
		}
		else{
			//when the current node matches with the key
			// No children 
			if(root->left==NULL and root->right==NULL){
				delete root;
				root = NULL;
			}
			// Single Child
			else if(root->left==NULL){
				node* temp = root;
				root = root->right;
				delete temp;
			}
			else if(root->right==NULL){
				node*temp = root;
				root = root->left;
				delete temp;
			}
			//2 Children
			else{
				node* temp = findMin(root->right);
				root->data = temp->data;
				root->right = remove(root->right,temp->data);
			}
		}
	return root;
}

//Challenge : Print all elements of BST which lie in the range k1 and k2
void printRange(node* root, int k1,int k2){
	if(root==NULL){
		return;
	}

	if(root->data >= k1 and root->data <= k2){
		//call on both sides
		printRange(root->left,k1,k2);
		cout<<root->data<<" ";
		printRange(root->right,k1,k2);
	}
	else if(root->data > k2){
		printRange(root->left,k1,k2);
	}
	else{
		// root->key < k1
		printRange(root->right,k1,k2);
	}
}

void printRoot2LeafPath(node* root, vector<int> &path){
     
     if(root == NULL){
     	return;
     }   
     if(root->left == NULL && root->right == NULL){
     	for(auto n : path){
     		cout<<n<<"->";
     	}
     	cout<<endl;
     	cout<<root->data<<" ";
     	return;
     }
     
     //rec case
     path.push_back(root->data);
     printRoot2LeafPath(root->left,  path);
     printRoot2LeafPath(root->right, path);
     //backtracking
     path.pop_back();
     return;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    node *root = NULL;
    int arr[] = {8,3,10,1,6,14,4,7,13};

    for(auto x: arr){
    	root = insert(root,x);
    }
    cout<<"inOrder"<<endl;
    printInorder(root);
	
	cout<<"levelOrder : "<<endl;
    levelOrderPrint(root);
	cout<<"search : "<<endl;
    cout<<search(root,6) <<endl;
    root = remove(root,6);
    
    cout<<"inOrder"<<endl;
    printInorder(root);
    cout<<endl;
    cout<<"Range is "<<endl;
	printRange(root,5,12);
    cout<<endl;
    vector<int> path;
	printRoot2LeafPath(root, path);
    
    return 0;
}