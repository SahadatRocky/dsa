#include<bits/stdc++.h>
using namespace std;

class node{

public:
	int data;
	node *next;

	node(int data){
		this->data = data;
		this->next = NULL;
	}
};

void push_front(node *&head, int data){
   
   if(head == NULL){
   	node *newNode = new node(data);
   	head = newNode;
   	return;
   } 

   //otherwise
   node *newNode = new node(data);
   newNode->next = head;
   head = newNode; 
}


void printAll(node* head){
    
    node* temp = head;
    cout<<"start->";
    while(temp!=NULL){
        cout<<temp->data<<"->";
        temp = temp->next;
    }
    cout<<"NULL"<<endl;
}

node* midElement(node *head){
    node *slow = head;
    node *fast = head->next;

    while(fast != NULL && fast->next != NULL){
        slow = slow->next;
        fast = fast->next->next;
    }

    return slow;
}

node* merge(node *a, node* b){
    //Complete this method
    //base case
    if(a==NULL){
        return b;
    }
    if(b==NULL){
        return a;
    }

    //rec case
    node * c;

    if(a->data < b->data){
        c = a;
        c->next = merge(a->next,b);
    }
    else{
        c = b;
        c->next = merge(a,b->next);
    }
    return c;
}

node* mergeSort(node* head){
   
   if(head == NULL || head->next == NULL){
      return head;
   }   

   node* mid = midElement(head);

   node* a = head;
   node* b = mid->next;
   mid->next = NULL;

   a = mergeSort(a);
   b = mergeSort(b);

   return merge(a,b);
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    node* a = NULL;
    push_front(a,10);
    push_front(a,7);
    push_front(a,5);
    push_front(a,1);
    push_front(a,17);
	push_front(a,2);
	push_front(a,14);

    printAll(a);
    a = mergeSort(a);
    printAll(a);

    return 0;
}