
#include<bits/stdc++.h>
using namespace std;

class node{

public:
	int data;
	node *next;

	node(int data){
		this->data = data;
		this->next = NULL;
	}
};

void push_front(node *&head, int data){
   
   if(head == NULL){
   	node *newNode = new node(data);
   	head = newNode;
   	return;
   } 

   //otherwise
   node *newNode = new node(data);
   newNode->next = head;
   head = newNode; 
}


void printAll(node* head){
    
    node* temp = head;
    cout<<"start->";
    while(temp!=NULL){
        cout<<temp->data<<"->";
        temp = temp->next;
    }
    cout<<"NULL"<<endl;
}

node* ditectCycle(node* head){
        node* slow = head;
        node* fast = head;
        
        while(fast != NULL && fast->next != NULL){
            
            slow = slow ->next;
            fast = fast ->next->next;
            
            if(slow == fast){
                return slow;
            }
        }
        
    return NULL;
}

bool hasCycle(node* head) {
        
        node* meet  = ditectCycle(head);
        node* start = head;
        
        while(start != meet){
            start = start->next;
            if(meet){
               meet = meet->next;    
            }
        } 

    return start;
}

bool containsCycle(node *head){
    //Complete this function 
    
    node*slow = head;
    node*fast = head;
    
    while(slow and fast and fast->next){
        slow = slow->next;
        fast = fast->next->next;
        if(slow==fast){
            return true;
        }
    }
    return false;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    node* a = NULL;
    push_front(a,3);
    push_front(a,8);
    push_front(a,7);
    push_front(a,6);
    push_front(a,5);
    push_front(a,4);
    push_front(a,3);
	push_front(a,2);
	push_front(a,1);

    printAll(a);

    cout<<hasCycle(a)<<endl;

    cout<<containsCycle(a)<<endl;
    
  
    return 0;
}