#include<bits/stdc++.h>
using namespace std;

class node{

public:
	int data;
	node *next;

	node(int data){
		this->data = data;
		this->next = NULL;
	}
};


void push_front(node *&head, int data){
   
   if(head == NULL){
   	node *newNode = new node(data);
   	head = newNode;
   	return;
   } 

   //otherwise
   node *newNode = new node(data);
   newNode->next = head;
   head = newNode; 
}

int kthLastElement(node *head, int k){
    //Complete this function to return kth last element
    
    node *slow = head;
    node *fast = head; 
    
    int cnt = 0;
    while(cnt < k){
        fast = fast->next;
        cnt++;
    }
    
    while(fast!=NULL){
      slow = slow->next;
      fast = fast->next;
    }
   return slow->data;
}

void printAll(node* head){
    
    node* temp = head;
    cout<<"start->";
    while(temp!=NULL){
        cout<<temp->data<<"->";
        temp = temp->next;
    }
    cout<<"NULL"<<endl;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    node* a = NULL;
    push_front(a,7);
    push_front(a,6);
    push_front(a,5);
    push_front(a,4);
    push_front(a,3);
	push_front(a,2);
	push_front(a,1);

    printAll(a);
    
    cout<<kthLastElement(a,3)<<endl;


    return 0;
}