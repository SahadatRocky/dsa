#include<bits/stdc++.h>
using namespace std;

class node{

public:
	int data;
	node *next;
	node(int data){
		this->data = data;
		this->next = NULL;
	}
};

node *head,*tail,*newNode;

void push_front(int data){
   
   if(head == NULL){
   	 newNode = new node(data);
   	 head = tail = newNode;
   	 return;
   } 

   //otherwise
   newNode = new node(data);
   newNode->next = head;
   head = newNode; 
}

void push_back(int data){
   
   if(head == NULL){
   	 newNode = new node(data);
   	 head = tail = newNode;
   	 return;
   } 

   //otherwise

   newNode = new node(data);
   tail->next = newNode;
   tail = newNode;
}

void insertDataInPosition(int data,int pos){
    
    if(pos == 0){
    	push_front(data);
    	return;
    } 

    //otherwise
    node *temp = head;
    for(int i=1; i<pos;i++){
    	temp = temp ->next;
    }

    newNode = new node(data);
    newNode->next = temp->next;
    temp->next = newNode;
}

int search(int key){
   
   node* temp = head;
   int index = 0;
   while(temp!=NULL){
       
       if(temp->data == key){
            return index;
       }

       index++;
       temp = temp->next; 
   }

   return -1;
}

int searchRecursive(node *temp,int key){
     
    //base case
    if(temp == NULL){
    	return -1;
    }

    //rec case

    if(temp->data == key){
    	return 0;
    }

    int index = searchRecursive(temp->next,key);

    if(index != -1){
    	return index + 1;
    }

    return -1;
}

int count(){
	int count = 0;
	node * temp = head;
	while(temp!=NULL){
		count++;
		temp = temp->next;
	}

	return count;
}

void Delete(int key){
   
   node *temp = head;

   while(temp->data != key){
   	   newNode = temp;
   	   temp = temp->next;
   }

   if(temp == head){
   	 head = temp->next;
   }
   else{
   	  newNode->next = temp->next;
   }
}


node* reverse(){
    node *prev = NULL;
    node *current = head;

    while(current!=NULL){
        node *nextNode = current->next;
        current->next = prev;

        prev = current;
        current = nextNode;
    }
    return prev;
}


node* recReverse(node* head){
   
   if(head == NULL || head->next == NULL){
      return head;
   }

   //otherwise

   node *sHead = recReverse(head->next);
   head->next->next = head;
   head->next = NULL;

   return sHead;
}

node* kReverse(node *head,int k){
     
     if(head == NULL){
     	return NULL;
     }

     node *prev = NULL;
     node *current = head;
     node *nextNode;
     int cnt =1;
     while(current!=NULL && cnt <=k){

     	 nextNode = current->next;
     	 current->next = prev;

     	 prev = current;
     	 current = nextNode;
     	 cnt++;
     }
     
     if(nextNode != NULL){
       head->next  = kReverse(nextNode,k);
     }

     return prev;
}


void printAll(node* head){
    
    node* temp = head;
    cout<<"start->";
    while(temp!=NULL){
    	cout<<temp->data<<"->";
    	temp = temp->next;
    }
    cout<<"NULL"<<endl;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    push_front(5);
    push_front(10);
    push_front(15);

    push_back(20);
    push_back(30);

    insertDataInPosition(2,0);
    insertDataInPosition(12,2);
    printAll(head);
    int key = 10;
    cout<< key <<" is present at index "<< search(key) <<endl;
    cout<< key <<" is present at index "<< searchRecursive(head, key) <<endl;
    cout<<"total Count is : "<<count()<<endl; 
    Delete(5);
    printAll(head);
    //head = reverse();
    // head = kReverse(head,2);
    //printAll(head);
    head = recReverse(head);
    printAll(head);

    return 0;
}