#include<bits/stdc++.h>
using namespace std;

class node{

public:
	int data;
	node *next;

	node(int data){
		this->data = data;
		this->next = NULL;
	}
};

void push_front(node *&head, int data){
   
   if(head == NULL){
   	node *newNode = new node(data);
   	head = newNode;
   	return;
   } 

   //otherwise
   node *newNode = new node(data);
   newNode->next = head;
   head = newNode; 
}

node* merge(node *a, node* b){
	//Complete this method
	//base case
	if(a==NULL){
		return b;
	}
	if(b==NULL){
		return a;
	}

	//rec case
	node * c;

	if(a->data < b->data){
		c = a;
		c->next = merge(a->next,b);
	}
	else{
		c = b;
		c->next = merge(a,b->next);
	}
	return c;
}

void printAll(node* head){
    
    node* temp = head;
    cout<<"start->";
    while(temp!=NULL){
    	cout<<temp->data<<"->";
    	temp = temp->next;
    }
    cout<<"NULL"<<endl;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    node* a = NULL;
    push_front(a,10);
    push_front(a,7);
    push_front(a,5);
    push_front(a,1);

    node* b = NULL;
    push_front(b,6);
    push_front(b,3);
    push_front(b,2);

    node *head = merge(a,b);

    printAll(head);


    return 0;
}