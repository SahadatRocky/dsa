#include<bits/stdc++.h>
using namespace std;

int solve(int i, int j,int m,int n){
    //base case
    if(i == m-1 && j == n-1){
        return 1;
    }
    
    if(i>=m || j>=n){
        return 0;
    }
    //rec case
    
    int ans = solve(i+1,j,m,n) + solve(i,j+1,m,n);     
    return ans;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    int n,m;

    cin>>m>>n;

    cout<<solve(0, 0, m, n)<<endl;

    return 0;
}