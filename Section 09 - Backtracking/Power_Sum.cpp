#include<bits/stdc++.h>
using namespace std;

int power(int a,int n){
    if(n==0){
        return 1;
    }
    return a*power(a,n-1);
}

int cal(int x,int n, vector<int>&nums){
	int sum=0;
    for(auto itr : nums){
        sum+=power(itr,n);
    }
    if(sum==x){
        return 1;
    }else{
        int answer=0;
        int v = nums.empty() ? 1: nums.back()+1;
        while(sum+ power(v,n)<=x){
            nums.push_back(v);
            answer+=cal(x,n,nums);
            nums.pop_back();
            v++;
        }
        return answer;
    }
}

int solve(int x,int n){
    vector<int> nums;
    return cal(x,n,nums);
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    int x= 100, n= 2;

    cout<<solve(x,n)<<endl;
}

/*

Input: x= 100, n= 2 
      
Output: 3
 
Explanation:
 
100= 10^2  = (6^2 + 8^2) = (1^2 + 3^2 + 4^2 + 5^2 + 7^2)

*/

