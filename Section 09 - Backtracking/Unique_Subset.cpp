#include <bits/stdc++.h>
using namespace std;

set<vector<int>> s;

void solve(vector<int>& nums,vector<int>& ans, int i){
    
    if(i == nums.size()){
        sort(ans.begin(), ans.end());
        s.insert(ans);
        return;
    }
    
    ans.push_back(nums[i]);
    solve(nums,ans, i+1);
    ans.pop_back();
    solve(nums,ans, i+1);
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<int> nums{1,2,2};
    s.clear();
    vector<int> ans;
    solve(nums,ans, 0);
    vector<vector<int>> v;

    for(auto x: s){
        v.push_back(x);
    }

    
    for(auto x: v){
        for(auto d: x){
            cout<<d<<" ";
        }
        cout<<endl;
    }    



    return 0;
}


/*

Sample Input

[1,2,2]
Sample Output

[[],[1],[1,2],[1,2,2],[2],[2,2]]


*/