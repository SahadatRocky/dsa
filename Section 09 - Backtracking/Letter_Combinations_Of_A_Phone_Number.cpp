#include <bits/stdc++.h>
using namespace std;

 vector<string> solve(string& digits, int i, map<int,string>& mp){
        
        if(i == digits.size()){
            return {""};
        }
        
        vector<string> temp;
        vector<string> ans;
        
        temp = solve(digits,i+1,mp);
        
        for(auto a: mp[digits[i] - '0']){
            for(auto x: temp){
                ans.push_back(a+x);
            }
        }
        
        return ans;
        
    }


vector<string> letterCombinations(string digits) {
    if(digits.size() == 0){
            return {};
        }
        map<int,string> mp;
        mp[2] = "abc";
        mp[3] = "def";
        mp[4] = "ghi";
        mp[5] = "jkl";
        mp[6] = "mno";
        mp[7] = "pqrs";
        mp[8] = "tuv";
        mp[9] = "wxyz";
        
        return solve(digits,0,mp);
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    string digits = "23";
    vector<string> output = letterCombinations(digits);

    for(auto x : output ){
    	cout<<x<<" ";
    }

    return 0;
}


/*

Input: digits = "23"
 
Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]

*/