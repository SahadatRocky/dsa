#include<bits/stdc++.h>
using namespace std;

int cnt = 0;

void solve(string str,int n,string result ,set<string>& st){
    
    for(int i=1; i<=n; i++){
        
        string pre = str.substr(0,i);

        if(st.find(pre) != st.end()){
            if(i == n){
                result += pre;
                cout<<result<<endl;
                cnt++;
                return;
            }
            solve(str.substr(i,n-i),n-i,result + pre + " ",st);  
        } 
    }
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    string str = "ilikesamsungmobile";
    vector<string> dictionary{"i", "like", "sam", "sung", "samsung", "mobile"};
    
    set<string> st;
    for(auto x: dictionary){
        st.insert(x);
    }
    
    solve(str,str.size(),"",st);
    cout<<cnt<<endl;

    return 0;
}


/*

Sample Input

dictionary = { i, like, sam, sung, samsung, mobile}
str = "ilikesamsungmobile"
Sample Output

2
Explanation

Following are the 2 possible ways to break the string

 i like sam sung mobile
 i like samsung mobile



*/

/*---using DP

 bool solve(int i,string s,set<string>& st,vector<int>& dp){
         
         if(i == s.size()){
            return 1; 
         }

         if(dp[i] != -1){
             return dp[i];
         }
         
         string temp;
         for(int j=i; j<s.size(); j++){
             temp += s[j];
             if(st.find(temp) != st.end()){
                 if(solve(j+1, s, st,dp)){
                      return dp[i] = 1;
                 }
             }
         }

         return dp[i] = 0;
    }

    bool wordBreak(string s, vector<string>& wordDict) {
        set<string> st;

        for(auto x : wordDict){
            st.insert(x);
        }

        vector<int> dp(s.size()+1, -1);
        return solve(0, s, st,dp);
    }


*/