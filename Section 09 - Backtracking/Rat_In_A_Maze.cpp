#include<bits/stdc++.h>
using namespace std;

bool isSafe(int arr[][20], int i, int j, int n){
    
    if(i<n && j<n && arr[i][j] == 1){
        return true;
    } 
    return false;
}

bool rateInMaze(int arr[][20], int i, int j, int n, int solArr[][20]){
    
    //base case

    if(i == n-1 && j == n-1){
        solArr[i][j] = 1;
        return true;
    }


    if(isSafe(arr,i,j,n)){
        solArr[i][j] = 1;

        if(rateInMaze(arr, i+1, j,n, solArr)){
            return true;
        }

        if(rateInMaze(arr, i, j+1,n, solArr)){
            return true;
        }
        
        //backtracking
        solArr[i][j] = 0;

    }

   return false;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);

   int n = 5;  
   int solArr[20][20] = {0};
   int arr[20][20] = {
                   {1,0,1,0,1},
                   {1,1,1,1,1},
                   {0,1,0,1,0},
                   {1,0,0,1,1},
                   {1,1,1,0,1},
                };

        rateInMaze(arr,0,0,n,solArr);

        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                cout<<solArr[i][j]<<" ";
            }
            cout<<endl;
        }        
    
    return 0;
}


/*

Sample Input

5 
1,0,1,0,1
1,1,1,1,1
0,1,0,1,0
1,0,0,1,1
1,1,1,0,1
Sample Output

1 0 0 0 0 
1 1 1 1 0 
0 0 0 1 0 
0 0 0 1 1 
0 0 0 0 1 



*/