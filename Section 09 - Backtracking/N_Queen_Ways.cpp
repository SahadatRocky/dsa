#include<bits/stdc++.h>
using namespace std;

bool canPlaceQueen(int board[][20],int n, int x,int y){
    //column
    for(int k=0; k<x; k++){
        if(board[k][y] == 1){
        	return false;
        }
    }

    // left diagonal
    int i = x;
    int j = y;

    while(i >= 0 && j >= 0){
    	if(board[i][j] == 1){
    		return false;
    	}

    	i--;
    	j--;

    } 

    //right diagonal
    i = x;
    j= y;

    while(i>=0 && j<n){
    	if(board[i][j] == 1){
    		return false;
    	}

    	i--;
    	j++;
    }
    
    return true;
}

void printBoard(int arr[][20],int n){
   
   for(int i=0; i<n; i++){
	   	for(int j=0; j<n; j++){
	   	    cout<<arr[i][j]<<" ";	
	   	}
	   	cout<<endl;
   }
   cout<<endl;
}

int solveNQueen(int n, int board[][20],int i){
    
    //base case
    if(i == n){
    	printBoard(board,n);
    	return 1;
    }

    //rec case

    //try to place a queen in every row
    int ways = 0;
    for(int j=0; j<n; j++){
         
         if(canPlaceQueen(board,n, i,j)){
         	board[i][j] = 1;

         	ways += solveNQueen(n,board,i+1);
         	
            //backtrack
         	board[i][j] = 0;
         }
    } 

    return ways;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n;
    cin>>n;

    int board[20][20] = {0};
    
    cout<<solveNQueen(n,board,0)<<endl;
    return 0;
}