#include<bits/stdc++.h>
using namespace std;

bool solve(vector<vector<char>>& board,string word, int i,int j, int k){
         
        if(k == word.size()){
            return true;
        }
        
        if( i<0 || j<0 || i >= board.size() || j >= board[0].size() || board[i][j] != word[k] ){
            return false;
        }
        
        bool ans;
        if(board[i][j] == word[k]){
            char ch = board[i][j];
            board[i][j] = '#';
            ans =  solve(board,word, i+1,j, k+1) ||
                        solve(board,word, i,j+1, k+1) ||
                        solve(board,word, i-1,j, k+1) ||
                        solve(board,word, i,j-1, k+1);
            
            board[i][j] = ch;
        }
        return ans;
}
    
bool exist(vector<vector<char>>& board, string word) {
        int n = board.size();
        int m = board[0].size();
        for(int i=0; i<n; i++){
            for(int j=0; j<m; j++){
                if( board[i][j] == word[0] && solve(board,word, i,j, 0) ){
                    return true;
                }
            }
        }
        
        return false;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<vector<char>> board{
        {'A','B','C','E'},
        {'S','F','C','S'},
        {'A','D','E','E'}
    };
    string word = "ABCCED";

    cout<<exist(board, word)<<endl;

    return 0;
}


/*

Sample Input

board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]] 
word = "ABCCED"


Sample Output

true

*/