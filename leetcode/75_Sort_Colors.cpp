#include <bits/stdc++.h>
using namespace std;

void swap(vector<int>& nums,int p1, int p2){
    
    int temp = nums[p1];
    nums[p1] = nums[p2];
    nums[p2] = temp;
}

vector<int> sortColors(vector<int>& nums){
    
    int zero = 0;
    int one = 0;
    int two = nums.size() - 1;

    while(zero <= two){

    	if(nums[zero] == 0){
    		swap(nums, zero, one);
            zero++;
            one++;  
    	}
    	else if(nums[zero] == 2){
    		swap(nums,zero,two);
    		two--;
    	}
    	else{
    		zero++;
    	}
    }
  
   return nums;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums={2,0,2,1,1,0};
    
    vector<int> output = sortColors(nums);
    for(auto x : output){
    	cout <<x<<" ";
    }
    cout<<endl;

	return 0;
}


/*

Input: nums = [2,0,2,1,1,0]
Output: [0,0,1,1,2,2]

Input: nums = [2,0,1]
Output: [0,1,2]

*/