#include <bits/stdc++.h>
using namespace std;

int solve(vector<int>& nums){
   
   map<int, int> mp;
   
   for(auto x : nums){
   	 mp[x]++;
   } 
   
   int mx = 0;
   int ans = 0; 
   for(auto x : mp){
   	  if(mx < x.second){
   	  	mx = x.second;
   	  	ans = x.first;
   	  }
   }

   return ans;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums={2,2,1,1,1,2,2};
    cout<<solve(nums)<<endl; 

	return 0;
}


/*
Input: nums = [3,2,3]
Output: 3

Input: nums = [2,2,1,1,1,2,2]
Output: 2

*/