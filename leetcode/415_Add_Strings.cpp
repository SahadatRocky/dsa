#include <bits/stdc++.h>
using namespace std;

string solve(string num1,string num2){
     
     int i = num1.size() - 1;
     int j = num2.size() - 1;

     string ans;
     int sum = 0;
     int carry = 0;

     while(i>=0 || j >=0){
      
      sum = carry;
     	if(i>=0){
     		sum = sum + num1[i--] - '0';
     	}

     	if(j>=0){
     		sum = sum + num2[j--] - '0';
     	}

     	carry = sum > 9 ? 1 : 0;
     	ans += (sum%10) + '0';
     }

     if(carry){
        ans += carry + '0';
     }
     reverse(ans.begin(), ans.end());

     return ans;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string num1 = "11";
    string num2 = "123";
    cout<<solve(num1, num2)<<endl;

	return 0;
}

/*

Input: num1 = "11", num2 = "123"
Output: "134"

Input: num1 = "456", num2 = "77"
Output: "533"

Input: num1 = "0", num2 = "0"
Output: "0"

*/