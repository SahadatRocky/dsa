#include <bits/stdc++.h>
using namespace std;

string solve(string num1, string num2){

	int i = num1.size() - 1;
	int j = num2.size() - 1;

    string ans;
    int sum =0;
    int carry = 0;  
    while(i>=0 || j>=0 || carry !=0){
    	sum = carry;
    	if(i >= 0){
           sum +=num1[i] - '0';
           i--;  
    	}

    	if(j>=0){
    		sum += num2[j] - '0';
    		j--;
    	}

    	carry = sum/2;
    	ans += (sum%2) + '0';
    }

    if(carry){
    	ans += carry + '0';
    }
    
    reverse(ans.begin(), ans.end()); 
    return ans;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string num1 = "11";
    string num2 = "1";

    cout<<solve(num1, num2)<<endl;

	return 0;
}

/*

Input: a = "11", b = "1"
Output: "100"

Input: a = "1010", b = "1011"
Output: "10101"

*/