#include <bits/stdc++.h>
using namespace std;

bool solve(vector<int>& nums){
   
   if(nums.size() < 3){
   	return false;
   }

   int l = INT_MAX;
   int m = INT_MAX;

   for(int i=0; i<nums.size() ; i++){
   	  if(nums[i] > m){
   	  	  return true;
   	  }
   	  else if(nums[i] < l){
   	  	l = nums[i];
   	  }  
   	  else if(nums[i] > l && nums[i] < m){
          m = nums[i]; 
   	  }
   }

   return false;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums={1,2,3,4,5};

    cout<<solve(nums)<<endl;

	return 0;
}

/*

Input: nums = [1,2,3,4,5]
Output: true
Explanation: Any triplet where i < j < k is valid.

Input: nums = [5,4,3,2,1]
Output: false
Explanation: No triplet exists.

Input: nums = [2,1,5,0,4,6]
Output: true
Explanation: The triplet (3, 4, 5) is valid because nums[3] == 0 < nums[4] == 4 < nums[5] == 6.
*/

