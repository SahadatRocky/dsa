#include <bits/stdc++.h>
using namespace std;

bool solve(string pattern,string s){
    
    string word;
    stringstream ss(s);
    vector<string> v;
    while(ss>>word){
       v.push_back(word); 
    }
    
    if(pattern.size() != v.size()){
           return false;
    }
    unordered_map<char, string> mp;
    unordered_map<string, char> m;

    for(int i=0; i<v.size(); i++){
    	if(mp.find(pattern[i]) != mp.end() && mp[pattern[i]] != v[i]){
            return false;
    	}
    	else if(m.find(v[i]) != m.end() && m[v[i]] != pattern[i]){
            return false;
    	}

    	mp[pattern[i]] = v[i];
    	m[v[i]] = pattern[i];
    }
    return true;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string pattern = "abba";
    string s = "dog cat cat fish";
    cout<<solve(pattern,s)<<endl;
	return 0;
}

/*

Input: pattern = "abba", s = "dog cat cat dog"
Output: true

Input: pattern = "abba", s = "dog cat cat fish"
Output: false

*/