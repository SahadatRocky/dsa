#include <bits/stdc++.h>
using namespace std;

vector<string> solve(string s){
     
     int n = s.size();

     vector<string> ans;
     unordered_map<string, int> m;

     for(int i=0; i<=n-10; i++){
         string temp = s.substr(i,10);
         if(m[temp] == 1){
            ans.push_back(temp);
         }   
         m[temp]++;
     }

     return ans;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT";

    vector<string> output = solve(s);

    for(auto x : output){
    	cout<<x<<endl;
    }    

	return 0;
}

/*

Input: s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"
Output: ["AAAAACCCCC","CCCCCAAAAA"]

Input: s = "AAAAAAAAAAAAA"
Output: ["AAAAAAAAAA"]

*/