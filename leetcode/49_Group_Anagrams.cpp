#include <bits/stdc++.h>
using namespace std;

vector<vector<string>> solve(vector<string>& strs){
     
     vector<vector<string>> ans;
     map<string, vector<string> > m;

     for(int i=0; i<strs.size(); i++){
     	string s = strs[i];
     	sort(s.begin(), s.end());
     	m[s].push_back(strs[i]);
     }

     for(auto x : m){
     	ans.push_back(x.second);
     }
    return ans;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<string> strs{"eat","tea","tan","ate","nat","bat"};

    vector<vector<string> > output = solve(strs);

    for(auto x : output){
    	for(auto c : x){
    		cout<<c<<" ";
    	}
    	cout<<endl;
    }
    cout<<endl;

    return 0;
}

/*
 
Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]


*/