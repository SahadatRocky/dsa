#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> solve(vector<vector<int>>& matrix){
   
   int m = matrix.size();
   int n = matrix[0].size();

   for(int row=0; row<m; row++){
      for(int col = row; col<n; col++){
        int temp = matrix[row][col];
        matrix[row][col] = matrix[col][row];
        matrix[col][row] = temp;

      }
   }
   
   for(int i=0; i<m; i++){
    reverse(matrix[i].begin(),matrix[i].end());
   } 
   return matrix;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<vector<int>> matrix{
        {1,2,3},
        {4,5,6},
        {7,8,9}
    };

    vector<vector<int>> output = solve(matrix);
    for(auto x : output){
        for(auto c : x){
            cout<<c<<" ";
        }
        cout<<endl;
    }
    cout<<endl;

    return 0;
}    

/*

Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
Output: [[7,4,1],[8,5,2],[9,6,3]]

Input: matrix = [[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]
Output: [[15,13,2,5],[14,3,4,1],[12,6,8,9],[16,7,10,11]]

*/