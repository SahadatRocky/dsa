#include <bits/stdc++.h>
using namespace std;

string solve(string s){
    
    int n = s.size();
    vector<vector<int>> dp(n, vector<int>(n,0));
    string ans;
    int maxLen = 0;
    for(int diff=0; diff<n; diff++){
    	for(int i=0,j= i+diff; j<n; i++,j++){
            
            if(i==j){
            	dp[i][j] = 1;
            }
            else if(diff == 1){
               dp[i][j] = (s[i]==s[j]) ? 2 : 0; 
            }
            else{
            	if((s[i]==s[j]) && dp[i+1][j-1]){
            		dp[i][j] = dp[i+1][j-1] + 2;
            	}
            }


            if(dp[i][j]){
               maxLen = j-i+1;
               ans = s.substr(i,maxLen);
            } 
    	}
    }

    return ans;

}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string s = "babad";
    cout<<solve(s)<<endl;

	return 0;
}

/*
Input: s = "babad"
Output: "bab"
Explanation: "aba" is also a valid answer.

Input: s = "cbbd"
Output: "bb"


*/