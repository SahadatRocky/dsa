#include <bits/stdc++.h>
using namespace std;

string solve(string num1,string num2){
    
    int m = num1.size();
    int n = num2.size();

    vector<int> result(m+n, 0);
    string ans;

    for(int i=m-1; i>=0; i--){
    	for(int j=n-1; j>=0; j--){
    		int mul = (num1[i] - '0') * (num2[j] - '0');
    		int p1 = i+j;
    		int p2 = i+j+1;
    		int sum = mul + result[p2];
    		result[p1] += sum/10;
    		result[p2] = sum%10; 
    	}
    }


    int i = 0;
    while(i < m+n && result[i] == 0){
    	i++;
    }

    while(i < m+n){
    	ans += result[i] + '0';
    	i++;
    }

    return (ans.size() == 0) ? "0" : ans;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string num1 = "123";
    string num2 = "456";

    cout<<solve(num1, num2)<<endl;

	return 0;
}

/*

Input: num1 = "2", num2 = "3"
Output: "6"

Input: num1 = "123", num2 = "456"
Output: "56088"

*/