#include <bits/stdc++.h>
using namespace std;

int solve(string s){
     
     map<char, int> m;

     for(auto x : s){
     	m[x]++;
     } 

     int len = 0;
     int carry = 0;

     for(auto x : m){
     	if(x.second%2 == 1){
             carry = 1;
             len += x.second - 1; 
     	}
     	else{
     		len += x.second;
     	}
     }

     len += carry;

     return len;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string s = "abccccdd";
    cout<<solve(s)<<endl;
    

	return 0;
}

/*

Input: s = "abccccdd"
Output: 7
Explanation: One longest palindrome that can be built is "dccaccd", whose length is 7.

Input: s = "a"
Output: 1
Explanation: The longest palindrome that can be built is "a", whose length is 1.

*/