#include <bits/stdc++.h>
using namespace std;


vector<int> solve(string s){
    
    vector<int> ans;
    unordered_map<char, int>m;

    for(int i=0; i<s.size(); i++){
    	m[s[i]] = i;
    }

    int start = 0;
    int end = 0;
    for(int i=0; i<s.size(); i++){
    	end = max(end , m[s[i]]);
    	if(i == end){
           ans.push_back(end-start+1);
           start = i+1; 
    	}
    }
    return ans;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string s = "ababcbacadefegdehijhklij";
    vector<int> output = solve(s);
    
    for(auto x : output){
    	cout<<x<<" ";
    }

	return 0;
}


/*

Input: s = "ababcbacadefegdehijhklij"
Output: [9,7,8]
Explanation:
The partition is "ababcbaca", "defegde", "hijhklij".
This is a partition so that each letter appears in at most one part.
A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it splits s into less parts.

*/