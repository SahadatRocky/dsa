#include <bits/stdc++.h>
using namespace std;

vector<int> solve(int n){
    
    vector<vector<int>> ans;
    for(int row=0; row<n+1;row++){
    	vector<int> v(row+1, 1);
    	for(int col=1; col<row; col++){
           v[col] = ans[row-1][col] + ans[row-1][col-1]; 
    	}
    	ans.push_back(v);
    }

   return ans[n];
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n;
    cin>>n;

    vector<int> output = solve(n);
	for(auto x : output){
    		cout<<x<<" ";
    	}
    	cout<<endl;
    return 0;
}    

/*

Input: rowIndex = 3
Output: [1,3,3,1]

Input: rowIndex = 0
Output: [1]

Input: rowIndex = 1
Output: [1,1]
*/