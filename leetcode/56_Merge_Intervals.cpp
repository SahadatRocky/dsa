#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> solve(vector<vector<int>>& intervals){
    
    if(intervals.size() <= 1){
    	return intervals;
    }

    sort(intervals.begin(), intervals.end());

    vector<vector<int>> mergedIntervals;
    vector<int> tempInterval = intervals[0];

    for(auto x : intervals){
    	cout<<x[0]<<"--"<<tempInterval[1]<<endl;
        if(x[0] <= tempInterval[1]){
        	tempInterval[1] = max(x[1], tempInterval[1]);
        } 
        else{
        	mergedIntervals.push_back(tempInterval);
        	tempInterval = x;
        }
    }

   mergedIntervals.push_back(tempInterval);
   return mergedIntervals;
} 



int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    vector<vector<int>> intervals
    {
        {1,3},
    	{2,6},
    	{8,10},
    	{15,18}
    };
    
    vector<vector<int>> output = solve(intervals);
    
    for (int i = 0; i < output.size(); i++)
    {
        for (int j = 0; j < output[i].size(); j++)
        {
            cout << output[i][j] << " ";
        }    
        cout << endl;
    }

	return 0;
}

/*

Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlap, merge them into [1,6].

Input: intervals = [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.
*/