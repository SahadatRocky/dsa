#include <bits/stdc++.h>
using namespace std;

vector<int> solve(vector<int>& nums) {
        int n = nums.size();
        vector<int> ans(n,1);

        int left = 1;
        for(int i=0; i<n; i++){
            ans[i] = ans[i]*left;
            left = left * nums[i];
        }

        int right = 1;
        for(int i= n-1 ; i>=0; i--){
            ans[i] = ans[i]*right;
            right= right * nums[i];
        }

        return ans;
    }

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums={1,2,3,4};

    vector<int> output = solve(nums);

    for(auto x : output){
        cout<<x<<" ";
    }
    cout<<endl;

	return 0;
}

/*
Input: nums = [1,2,3,4]
Output: [24,12,8,6]

Input: nums = [-1,1,0,-3,3]
Output: [0,0,9,0,0]


*/