#include <bits/stdc++.h>
using namespace std;

int solve(vector<int>& nums, int k) {
        int n = nums.size();
        map<int,int>m;
        m[0] = 1;
        int sum = 0;
        int ans = 0;

        for(auto x : nums){
            sum+=x;
            int f = sum - k;
            if(m.find(f) != m.end()){
                 ans += m[f]; 
            }
            m[sum]++;
        }

        return ans;
    }

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums={1,1,1};
    int k = 2;

    cout<<solve(nums,k)<<endl;
	return 0;
}

/*

Input: nums = [1,1,1], k = 2
Output: 2

Input: nums = [1,2,3], k = 3
Output: 2


*/