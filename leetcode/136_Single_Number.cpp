#include <bits/stdc++.h>
using namespace std;

int solve(vector<int>& nums){
    int xorr = 0;
    for(int i=0; i<nums.size(); i++){
       xorr ^= nums[i];
    }

    return xorr;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums={4,1,2,1,2}; 
    
    cout<<solve(nums)<<endl;


	return 0;
}


/*

Input: nums = [2,2,1]
Output: 1

Input: nums = [4,1,2,1,2]
Output: 4

Input: nums = [1]
Output: 1
*/