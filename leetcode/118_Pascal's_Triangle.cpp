#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> solve(int n){
    
    vector<vector<int>> ans;
    for(int row=0; row<n; row++){
    	vector<int> v(row+1, 1);
    	for(int col=1; col<row; col++){
           v[col] = ans[row-1][col] + ans[row-1][col-1]; 
    	}
    	ans.push_back(v);
    }

   return ans;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n;
    cin>>n;

    vector<vector<int>> output = solve(n);
	for(auto x : output){
    	for(auto c : x){
            cout<<c<<" ";
          } 
          cout<<endl;
    	}
    return 0;
}    

/*
Input: numRows = 5
Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]

Input: numRows = 1
Output: [[1]]

*/