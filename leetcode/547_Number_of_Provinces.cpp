#include<bits/stdc++.h>
using namespace std;

 void dfs(vector<int>gr[],vector<int>& visited,int src){
        
        visited[src] = 1;

        for(auto nbr : gr[src]){
            if(!visited[nbr]){
              dfs(gr, visited, nbr);
            }
            
        }
   }

    int findCircleNum(vector<vector<int>>& isConnected) {
        
        int m = isConnected.size();
        int n = isConnected[0].size();
        
        vector<int> gr[n];
        vector<int> visited(n,0);
        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                 if(isConnected[i][j] == 1 && i!=j){
                      gr[i].push_back(j);
                      gr[j].push_back(i);
                 } 
            }
        }

        int cnt = 0;
        for(int i=0; i<n; i++){
            if(!visited[i]){
                cnt++;
                dfs(gr,visited,i);
            }
        }
        return cnt;
    }


int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    

    return 0;
}    