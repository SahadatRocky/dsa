#include <bits/stdc++.h>
using namespace std;

int solve(vector<vector<int> >& intervals){
      
      if(intervals.size() == 0){
      	return 0;
      }

      sort(intervals.begin(), intervals.end());

      int end = intervals[0][1];
      int cnt = 0;
      for(int i=1; i<intervals.size(); i++){
      	  if(intervals[i][0] < end){
             cnt++;
             end = min(end, intervals[i][1]);
      	  } 
      	  else{
      	  	end = intervals[i][1];
      	  }
      }

     return cnt; 
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<vector<int> > intervals={
    	{1,2},
    	{1,2},
    	{1,2}
    };
    
    cout<<solve(intervals)<<endl; 

	return 0;
}

/*

Input: intervals = [[1,2],[2,3],[3,4],[1,3]]
Output: 1
Explanation: [1,3] can be removed and the rest of the intervals are non-overlapping.

Input: intervals = [[1,2],[1,2],[1,2]]
Output: 2
Explanation: You need to remove two [1,2] to make the rest of the intervals non-overlapping.

Input: intervals = [[1,2],[2,3]]
Output: 0
Explanation: You don't need to remove any of the intervals since they're already non-overlapping.

*/