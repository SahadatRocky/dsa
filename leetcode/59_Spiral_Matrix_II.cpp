#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> solve(int n){
    vector<vector<int>> matrix(n,vector<int>(n));

    int cnt = 1;
    int start_row = 0;
    int end_row = n-1;
    int start_col = 0;
    int end_col = n-1;

    while(start_row <= end_row && start_col <= end_col){
        
       //start row
       for(int col = start_col; col <=end_col; col++){
           matrix[start_row][col] = cnt;
           cnt++;
       }

       //end col
       for(int row = start_row+1; row <= end_row; row++){
           matrix[row][end_col] = cnt;
           cnt++;
       }        

       // //end row
       for(int col = end_col-1; col>=start_col; col--){
          if(start_row == end_row){
              break;
          }
          matrix[end_row][col] = cnt;
          cnt++;
       }

       // //start col
       for(int row = end_row-1; row > start_row; row--){
          if(start_col == end_col){
              break;
          }
          matrix[row][start_col] = cnt;
          cnt++;
       }       

       start_row++;
       end_row--;
       start_col++;
       end_col--;
    }  

    return matrix;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n;
    cin>>n;

    vector<vector<int>> output = solve(n);
    
    for(auto x : output){
        for(auto c : x){
            cout<<c<<" ";
        }
        cout<<endl;
    }
    cout<<endl;

    return 0;
} 

/*

Input: n = 3
Output: [[1,2,3],[8,9,4],[7,6,5]]

Input: n = 1
Output: [[1]]

*/