#include <bits/stdc++.h>
using namespace std;

bool solve(vector<vector<int> >& matrix,int target){
     
     int m = matrix.size();
     int n = matrix[0].size();

     int i=0;
     int j=n-1;

     while( (i>=0 && i<m) && (j>=0 && j<n)){
     	if(matrix[i][j] == target){
     		return true;
     	}

     	if(matrix[i][j] > target){
     	   j--;
     	}

     	else{
     	   i++;
     	}
    }
    return false;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<vector<int> > matrix={
    	{1,4,7,11,15},
    	{2,5,8,12,19},
    	{3,6,9,16,22},
    	{10,13,14,17,24},
    	{18,21,23,26,30}
    };

    int target = 5;

    cout<<solve(matrix,target)<<endl;

    return 0;
}

/*

Input: matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], target = 5
Output: true

Input: matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], target = 20
Output: false


*/    