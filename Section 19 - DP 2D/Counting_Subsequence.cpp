#include<bits/stdc++.h>
using namespace std;

int countSub(string s1,string s2,int m,int n,vector<vector<int>>& dp){
    
    //base case
    if( (m == -1 && n == -1) || n == -1 ){
    	return 1;
    }
    
    if(m == -1){
    	return 0;
    }
    //memoization
    // if(dp[m][n] != -1){
    //   return dp[m][n];
    // } 
    
    //rec case
    if(s1[m] == s2[n]){
    	return countSub(s1, s2, m-1, n-1, dp) + countSub(s1, s2, m-1, n, dp);
    }

    return countSub(s1, s2, m-1, n, dp);
}

//Button UP
int countSubBU(string s1,string s2){
    
    int m = s1.size();
    int n = s2.size();

    vector<vector<int>> dp(m+1, vector<int>(n+1, 0));

    //fill the table in buttom up manner
    //first col as 1
    
    for(int i=0; i<=m; i++){
    	dp[i][0] = 1;
    }
    
    //1.1.......m.n
    for(int i=1; i<=m; i++){
    	for(int j=1; j<=n; j++){
    	   if(s1[i-1] == s2[j-1]){
    	   	   dp[i][j] = dp[i-1][j-1] + dp[i-1][j];
    	   }else{
    	   	dp[i][j] = dp[i-1][j];
    	   }
       }
    }

    return dp[m][n];
}

int main(){
   
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);
   
   string s1 = "ABCECDGCC";
   string s2 = "ABC";
   int m = s1.size();
   int n = s2.size();
   vector<vector<int>> dp(n+1, vector<int>(m+1, -1));

   cout<<countSub(s1, s2, m-1, n-1, dp)<<endl;

   cout<<countSubBU(s1, s2)<<endl;

   return 0;
}