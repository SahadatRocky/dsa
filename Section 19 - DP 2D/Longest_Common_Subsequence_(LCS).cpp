#include<bits/stdc++.h>
using namespace std;
// Top Down Approch
int LCS(string s1,string s2,int i, int j,vector<vector<int>>& dp){
     
    //base case
    if(i == s1.size() || j == s2.size()){
      return 0;
    }

    if(dp[i][j] != -1){
      return dp[i][j];
    }
     
    int ans = 0;
    if(s1[i] == s2[j]){
      ans = 1 + LCS(s1, s2, i+1, j+1, dp);
    }else{
      int op1 = LCS(s1, s2, i+1, j, dp);
      int op2 = LCS(s1, s2, i, j+1, dp);
      ans = max(op1, op2);
    }

    return dp[i][j] = ans;
}

//// Bottom UP Approch

int LCSBU(string s1,string s2){
    
    
    int n = s1.size();
    int m = s2.size();
    vector<vector<int>> dp(n+1, vector<int>(m+1, 0));
    for(int i=1; i<=n; i++){
      for(int j = 1; j<=m; j++){
         
         if(s1[i-1] == s2[j-1]){
            dp[i][j] = 1 + dp[i-1][j-1];
         }else{
            int op1 = dp[i-1][j];
            int op2 = dp[i][j-1];
            dp[i][j] = max(op1, op2);
         }
      }
    }

 
    for(int i=0; i<=n ;i++){
      for(int j=0; j<=m ;j++){
         
         cout<<dp[i][j]<<" "; 
      } 
      cout<<endl;
    }
    cout<<endl;
  return dp[n][m]; 
}



int main(){
   
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout);
   
   string s1 = "ABCD";
   string s2 = "ABEDG";
   int n = s1.size();
   int m = s2.size();
   vector<vector<int>> dp(n+1, vector<int>(m+1, -1));

   cout<<LCS(s1,s2,0,0,dp)<<endl;

   cout<<LCSBU(s1,s2)<<endl;

   return 0;
}