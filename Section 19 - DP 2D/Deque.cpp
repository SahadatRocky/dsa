#include<bits/stdc++.h>

#define int long long int
#define ld long double

using namespace std;

const int N = 3001;
int n, a[N], memo[N][N];

int dp(int i, int j) {
    if (i == j) {
        return a[i];
    }
    if (memo[i][j] != -1e18) {
        return memo[i][j];
    }
    int ans = max(a[i] - dp(i + 1, j), a[j] - dp(i, j - 1));
    return ans;
}


int32_t main() {

	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            memo[i][j] = -1e18;
        }
    }

    cout << dp(0, n - 1);

	return 0;
}


/*
input:
4
10 80 90 30

output:
10

*/