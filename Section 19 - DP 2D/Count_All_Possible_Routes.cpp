#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007

int dp[101][201],n;

int countRoutes(vector<int>& a, int start, int finish, int fuel) {
     
     //base case
     if(fuel < 0){
     	return 0;
     }

	 //memoization
     if(dp[start][fuel] != -1){
     	return dp[start][fuel];
     }

     long long  ans = (start == finish);

     for(int i=0; i<n; i++){
     	if(i != start){
     		ans = (ans + countRoutes(a, i, finish, fuel - abs(a[i] - a[start]) ))%mod;
     	}
     }
    
    return dp[start][fuel] = ans; 
}


int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> locations{2,3,6,8,4};
    int start = 1;
    int finish = 3;
    int fuel = 5; 
    n = locations.size();
    memset(dp, -1, sizeof(dp));
    cout<<countRoutes(locations, start, finish, fuel)<<endl;

	return 0;
}


/*

Input: locations = [2,3,6,8,4], start = 1, finish = 3, fuel = 5
 
Output: 4
 
Explanation: The following are all possible routes, each uses 5 units of fuel:
1 -> 3
1 -> 2 -> 3
1 -> 4 -> 3
1 -> 4 -> 2 -> 3

*/