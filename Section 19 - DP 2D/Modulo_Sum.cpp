#include<bits/stdc++.h>
using namespace std;

string solve(int n,int m,vector<int> a){
    if(n>m)
    {
        return "Yes";
    }
    vector<vector<bool>>dp(n,vector<bool>(m+1,false));
    dp[0][a[0]%m]=true;
    for(int i=1;i<n;i++)
    {
        int x=a[i]%m;
        dp[i][x]=true;
        for(int j=0;j<=m;j++)
        {
            int y=(x+j)%m;
            dp[i][j]=(dp[i-1][j] | dp[i][j]);
            dp[i][y]=(dp[i][y]  | dp[i-1][j]);
        }
    }
    
    return dp[n-1][0] ? "Yes" : "No";
}

int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n=3, m = 5;
    vector<int> a{1, 2, 3};
    
    cout<<solve(int n,int m,vector<int> a)<<endl;

	return 0;
}

/*

Input:  n = 3, m = 5, a = {1, 2, 3}
 
Output: Yes
 
Explanation:  You can choose numbers 2 and 3, the sum of which is divisible by 5.

*/