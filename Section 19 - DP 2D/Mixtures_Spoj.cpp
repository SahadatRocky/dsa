#include<bits/stdc++.h>
using namespace std;

long long mixtures(vector<int> v, int i, int j, vector<vector<int>>& dp)
{
	if (i == j) return 0;
	if (dp[i][j] != -1) return dp[i][j];

	long long res = INT_MAX;

	for (int k = i; k < j; k++)
	{
		long long ans = mixtures(v, i, k, dp) + mixtures(v, k + 1, j, dp);
		ans += ( ( (v[k] - (i > 0 ? v[i - 1] : 0) ) % 100 ) * ((v[j] - v[k]) % 100) );
		res = min(res, ans);
	}

	return dp[i][j] = res;
}

int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> v{40, 60, 20};
    
    int n = v.size();

    vector<vector<int>> dp(n+1, vector<int>(n+1, -1));

    for (int i = 1; i < n; i++)
	{
		v[i] += v[i - 1]; //cumulative sum
	}

	long long ans = mixtures(v, 0, n - 1, dp);
	cout<<ans<<endl;
	return 0;
}

/*

Input:

18 19

Output:

342

Sample Testcase 2:

Input:

40 60 20

Output:

2400


*/