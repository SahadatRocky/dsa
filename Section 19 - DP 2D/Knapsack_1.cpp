#include<bits/stdc++.h>
using namespace std;

//Top Down Approch

int knapsack( int wt[],int prices[],int N,int W ,vector<vector<int> >& dp, int index, int weight ){
     
    //base case 
	if(index >= N){
	   return 0;
	}

    //memoization
	if(dp[index][weight] != -1){
	   return dp[index][weight];
	}
     
    int include = 0;
    int exclude = 0;
	//take the item
	if( wt[index] + weight <= W ){
    include =  prices[index] + knapsack( wt, prices, N, W ,dp, index+1, wt[index] + weight);
	}
    
    //don't take the item
    exclude = knapsack( wt, prices, N, W ,dp, index+1, weight);
    return dp[index][weight] = max(include, exclude);
}


int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int wt[] = {3,4,5};
    int prices[] = {30,50,60};

    int N = 3;
    int W = 8;

    
    vector<vector<int> > dp(N+1, vector<int>(W+1, -1));
    cout<<knapsack( wt, prices, N, W ,dp, 0, 0)<<endl;

	return 0;
}

/*

input:

3 8
3 30
4 50
5 60

output:
90

*/
