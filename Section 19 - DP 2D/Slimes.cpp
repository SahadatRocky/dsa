#include<bits/stdc++.h>

#define int long long int
using namespace std;
const int N = 400;
int n, a[N], pref[N], memo[N][N];

int sum(int l, int r) {
	return pref[r] - (l == 0 ? 0 : pref[l - 1]);
}

int dp(int l, int r) {
	if (l == r) {
		return 0;
	}

	if (memo[l][r] != 0) {
		return memo[l][r];
	}	

	int ans = 1e18;
	for (int i = l; i < r; i++) {
		ans = min(ans, dp(l, i) + dp(i + 1, r) + sum(l, r));
	}
	return ans;
}

int32_t main() {

	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a[i];
		pref[i] = a[i];
		if (i) {
			pref[i] += pref[i - 1];
		}
	}

	cout << dp(0, n - 1);
	return 0;
}

/*
input:
4
10 20 30 40

output:
190


*/