#include<bits/stdc++.h>
using namespace std;

int solve(vector<int>& nums,vector<vector<int>>& memo,int i,int target,int total){
        
    if(i == nums.size()){
        return total==target ? 1 : 0;
    }
        
    if(memo[i][total + 1000] != -1){
        return memo[i][total + 1000];
    }
        
    return  memo[i][total + 1000] = solve(nums,memo,i+1,target,total + nums[i]) + 
           solve(nums,memo,i+1,target, total- nums[i]);   
}


int findTargetSumWays(vector<int> nums, int target) {
    int n = nums.size();
    vector<vector<int>> memo(n+1,vector<int>(2001, -1));
    return solve(nums,memo,0,target,0);
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    vector<int> nums{1,1,1,1,1}; 
    int target = 3;
    cout<<findTargetSumWays(nums,target)<<endl;
    
    return 0;
}

/*

Input: nums = [1,1,1,1,1], target = 3
 
Output: 5
 
Explanation: There are 5 ways to assign symbols to make the sum of nums be target 3.
-1 + 1 + 1 + 1 + 1 = 3
+1 - 1 + 1 + 1 + 1 = 3
+1 + 1 - 1 + 1 + 1 = 3
+1 + 1 + 1 - 1 + 1 = 3
+1 + 1 + 1 + 1 - 1 = 3

*/