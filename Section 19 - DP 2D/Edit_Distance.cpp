#include<bits/stdc++.h>
using namespace std;

/*
int minDistance(string word1, string word2) {
        int m = word1.size();
        int n = word2.size();
        vector<vector<int> > dp(m+1, vector<int>(n+1, 0));

        for(int row=0; row<=m; row++){
            dp[row][0] = row;
        }
        
        for(int col=0; col<=n; col++){
            dp[0][col] = col;
        }

        for(int i=1; i<=m; i++){
            for(int j=1; j<=n; j++){
                if(word1[i-1] == word2[j-1]){
                    dp[i][j] = dp[i-1][j-1];
                }
                else{
                    int insert = dp[i][j-1];
                    int del = dp[i-1][j];
                    int replace = dp[i-1][j-1];

                    dp[i][j] = 1 + min(insert, min(del, replace));
                }
            }
        }
       return dp[m][n];
}
*/

int minDistance(string str1,int n1,string str2,int n2,int dp[500][500]){
   
   if(n1 == 0){
   	return n2;
   }
   if(n2 == 0){
   	return n1;
   }

   if(dp[n1][n2] != -1){
   	  return dp[n1][n2];
   }
   
   int ans = 0;
   if(str1[n1-1] == str2[n2-1]){
       ans = minDistance(str1, n1-1, str2, n2-1, dp);
   }else{

   	   int op1,op2,op3;
       op1 = minDistance(str1, n1, str2, n2-1, dp); //insert
       op2 = minDistance(str1, n1-1, str2, n2, dp); //remove
       op3 = minDistance(str1, n1-1, str2, n2-1, dp); //replace

       ans = 1 + min(op1, min(op2,op3));

   } 

   return dp[n1][n2] = ans;

}


int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int dp[500][500];

    memset(dp, -1, sizeof(dp));
    
    string s1 = "geek";
    string s2 = "gesek";

    int n1 = s1.size();
    int n2 = s2.size();
    
     cout<<minDistance(s1, n1, s2, n2, dp)<<endl;
 
	return 0;
}

/*

Input:   str1 = "geek", str2 = "gesek"

Output:  1

We can convert str1 into str2 by inserting a 's'.



Input:   str1 = "cat", str2 = "cut"

Output:  1

We can convert str1 into str2 by replacing 'a' with 'u'.



Input:   str1 = "sunday", str2 = "saturday"

Output:  3

Last three and first characters are same.  We basically

need to convert "un" to "atur".  This can be done using

below three operations.

Replace 'n' with 'r', insert t, insert a

*/