#include<bits/stdc++.h>

#define int long long int
using namespace std;

const int N = 1e4;

string k;
int d, mod = 1e9 + 7;
int memo[N][2][100];

int dp(int i, bool last, int m) {
	if (i == k.size()) {
		return (m == 0);
	}

	if (memo[i][last][m] != -1) {
		return memo[i][last][m];
	}	

	int ans = 0;
	int till = last ? (k[i] - '0') : 9;

	for (int digits = 0; digits <= till; digits++) {
		ans += dp(i + 1, last && (digits == till), (m + digits) % d);
		ans %= mod;
	}

	return ans;
}

int32_t main() {

	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	cin >> k >> d;
	memset(memo, -1, sizeof(memo));
	cout << (dp(0, 1, 0) - 1 + mod) % mod;

	return 0;
}

/*
input:
30 4

output:
6

*/