#include<bits/stdc++.h>
#define int long long int

using namespace std;
int n, mod = 1e9 + 7;
string str;
int memo[3005][3005];

int dp(int i, int s, int g) {
	if (i == n) {
		return 1;
	}	
	
	if (memo[i][s] != -1) {
		return memo[i][s];
    }

	int ans = 0;
	if (str[i - 1] == '<') {
		for (int j = 1; j <= g; j++) {
			ans += dp(i + 1, s + j - 1, g - j);
		}
	}
	else {
		for (int j = 1; j <= s; j++) {
			ans += dp(i + 1, j - 1, g + s - j);
		}
	}

	return ans;
}

int32_t main() {

	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
    
    cin >> n >> str;

    int ans = 0;
	memset(memo, -1, sizeof(memo));

	for (int i = 1; i <= n; i++) {
		ans += dp(1, i - 1, n-i);
	}
	cout << ans % mod;

	return 0;
}

/*

input:
4
<><

output:
5

*/