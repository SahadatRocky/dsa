#include<bits/stdc++.h>
#define int long long int

using namespace std;

int32_t main() {

	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);


	int n, k, mod = 1e9 + 7;
	cin >> n >> k;

	int a[n + 1];

	for (int i = 1; i <= n; i++) {
		cin >> a[i];
	}

	int dp[n + 1][k + 1];
	memset(dp, 0, sizeof(dp));

	dp[0][0] = 1;

	for (int i = 1; i <= n; i++) {

		for (int j = 1; j <= k; j++) {
			dp[i - 1][j] += dp[i - 1][j - 1];
		}

		for (int j = 0; j <= k; j++) {
			dp[i][j] = dp[i - 1][j];
			if (j - a[i] > 0) {
				dp[i][j] -= dp[i - 1][j - a[i] - 1];
			}	
			dp[i][j] = ((dp[i][j] % mod) + mod) % mod;
		}
	}


	for (int i = 0; i <= n; i++) {
		for (int j = 0; j <= k; j++) {
			cout << dp[i][j] << " ";
		} cout << endl;
	}


	cout << dp[n][k];
	return 0;
}

/*

input:
3 4
1 2 3

output:
5

*/