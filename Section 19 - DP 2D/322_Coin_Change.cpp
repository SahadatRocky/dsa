#include<bits/stdc++.h>
using namespace std;

//Bottom Up Approch

int minimumNoOfCoinForChange(vector<int>& coins,int amount){
     
     vector<int> dp(amount+1, 0);
     dp[0] = 0;

     for(int i=1; i<=amount; i++){
     	dp[i] = INT_MAX;
     	for(auto c : coins){
     		if(i-c >= 0 && dp[i-c] != INT_MAX){
     		   dp[i] = min(dp[i], dp[i-c]+1);	
     		}
     	}
     }

     return dp[amount] == INT_MAX ? -1 : dp[amount];
}


///Top Down Approch 

int coinChange(vector<int>& coins,int i,vector<vector<int>>& dp,int amount){
    
    if(i>= coins.size()){
    	return 1e9;
    }

    //base case
    if(i < coins.size()){
        if(amount == 0){
           return 0;  
        };
    } 

    //memoization
    if(dp[i][amount] != -1){
    	return dp[i][amount];
    }

    //rec case
    
    int include = 1e9;
    int exclude;

    if(coins[i] <= amount){
         
    	include = 1 + coinChange(coins,i,dp,amount - coins[i]);
    }
    
    exclude = coinChange(coins,i+1,dp,amount);

    int ans = min(include, exclude);
    
    return dp[i][amount] = ans;

}




int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<int> coins{1,2,5};
    int amount = 11;

    int n = coins.size();
    vector<vector<int>> dp(n+1, vector<int>(amount+1, -1));

    int ans  = coinChange(coins,0,dp,amount); 

    ans == 1e9 ? cout<<"-1" : cout<<ans<<endl;

    cout<<minimumNoOfCoinForChange(coins, amount);

	return 0;
}

/*

Input: coins = [1,2,5], amount = 11
Output: 3
Explanation: 11 = 5 + 5 + 1

*/