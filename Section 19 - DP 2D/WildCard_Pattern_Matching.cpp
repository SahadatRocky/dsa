#include<bits/stdc++.h>
using namespace std;

int match(string str,int n1,string p,int n2 ,vector<vector<int>>& dp){
    
    if(n1 == 0 && n2 == 0){
    	return 1;
    }
    //pattern 0 
    if(n2 == 0){
       return 0;
    }
    
    //str 0
    if(n1 == 0){

    	while(n2 > 0){
            if(p[n2-1] != '*'){
                return 0;
            }
            n2--;
    	}
    	return 1;
    }

    //memoization
    if(dp[n1][n2] != -1){
    	return dp[n1][n2];
    }
    
    if(p[n2-1] == '*'){
       dp[n1][n2] = match(str, n1-1, p, n2 ,dp) || match(str, n1, p, n2-1 ,dp);
    }
    
    else if(p[n2-1] == '?'){
       dp[n1][n2] = match(str, n1-1, p, n2-1 ,dp);
    }
    
    else if( str[n1-1]==p[n2-1]){
       dp[n1][n2] = match(str, n1-1, p, n2-1 ,dp);
    }else{
    	dp[n1][n2] = 0;
    }
    
    return dp[n1][n2];
}


bool patternMatching(string str, string p){
     
     int n1 = str.size();
     int n2 = p.size();

     vector<vector<int>> dp(n1 + 1, vector<int>(n2 + 1, -1));

     int ans = match(str, n1, p, n2 ,dp);

     if(ans == 1){
     	return true;
     }else{
     	return false;
     }
}

int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string str = "baaabab";
    string p = "**ba**ab";
    cout<<patternMatching(str,p)<<endl;

	return 0;
}

/*

For example,

Text = "baaabab",

Pattern = “**ba**ab", output : true

Pattern = "baaa?ab", output : true

Pattern = "ba*a?", output : true

Pattern = "a*ab", output : false

*/