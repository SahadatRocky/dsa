#include<bits/stdc++.h>
using namespace std;

int solve(int n,int i,vector<vector<int>>& dp){

         if(i == 1){
             return 1;
         } 


         if(dp[i][n] != -1){
             return dp[i][n];
         }

         int include = 0;
         int exclude = 0;

         if(i<=n){
             include += i * solve(n-i, i, dp);  
         }
         exclude += solve(n, i-1, dp);
         return dp[i][n] = max(include,exclude); 
    }
 
    int integerBreak(int n) {
        
        vector<vector<int>> dp(n+1, vector<int>(n+1, -1));

        return solve(n, n-1, dp);

    }
int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    
 
	return 0;
}

/*

Input: n = 2
Output: 1
Explanation: 2 = 1 + 1, 1 × 1 = 1.

Input: n = 10
Output: 36
Explanation: 10 = 3 + 3 + 4, 3 × 3 × 4 = 36.

*/