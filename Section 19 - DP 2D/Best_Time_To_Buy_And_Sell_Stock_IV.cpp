#include<bits/stdc++.h>
using namespace std;

int solve(int k, vector<int>& prices) {
    int n = prices.size();
    if(n == 0){
    	return 0;
    }    

    int dp[k+1][n+1];
    memset(dp, 0, sizeof(dp));

    for(int i=1; i<=k; i++){
    	for(int j=1; j<n; j++){
    		dp[i][j] = dp[i][j-1];
    		for(int k=0; k<j; k++){
               dp[i][j] = max(dp[i][j], prices[j] - prices[k] + dp[i-1][k]);
    		}
    	}
    }

    return dp[k][n-1];
}

int main(){
    
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int k = 2;
    vector<int> prices{2,4,1};

    cout<<solve(k,prices)<<endl;

	 return 0;
}

/*

Input: k = 2, prices = [2,4,1]
 
Output: 2
 
Explanation: Buy on day 1 (price = 2) and sell on day 2 (price = 4), profit = 4-2 = 2.

*/