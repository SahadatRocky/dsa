#include<bits/stdc++.h>
#define int long long int
using namespace std;

const int N = 101;
int prices[N], wt[N], W, n, mW[N][100001], dp[N][100001];

//Buttom Up Approch

int32_t main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    cin >> n >> W;

    for (int i = 1; i <= n; i++) {
        cin >> wt[i] >> prices[i];
    }

    for (int i = 0; i <= n; i++) {
        for (int val = 0; val < 100001; val++) {
            mW[i][val] = 1e13;
        }
    }

    dp[0][0] = 1;
    mW[0][0] = 0;

    for (int i = 1; i <= n; i++) {
        for (int val = 0; val < 100001; val++) {
            // taking item
            if (val - prices[i] >= 0 && 
                dp[i - 1][val - prices[i]] && 
                mW[i - 1][val - prices[i]] + wt[i] <= W) {

                dp[i][val] = 1;
                mW[i][val] = min(mW[i][val], mW[i - 1][val - prices[i]] + wt[i]);
            }

            // not taking
            if (dp[i - 1][val]) {
                dp[i][val] = 1;
                mW[i][val] = min(mW[i][val], mW[i - 1][val]);
            }
            
        }
    }

    int ans = 0;
    for (int w = 0; w < 100001; w++) {
        if (dp[n][w]) ans = w;
    }

    cout << ans;
    
	return 0;
}

/*

input:

3 8
3 30
4 50
5 60

output:
90

input:

1 1000000000
1000000000 10

output:
10

*/
