#include<bits/stdc++.h>
using namespace std;

int wine(int arr[], int L, int R, int y,vector<vector<int> >& dp){
     //base case
    if(L>R){
        return 0;
    }
    
    if(dp[L][R] != 0){
        return dp[L][R];
    }
    
    int pick_left = y * arr[L] + wine(arr, L+1, R, y+1, dp);
    int pick_right =y * arr[R] + wine(arr, L, R-1, y+1, dp);

    return dp[L][R] = max(pick_left, pick_right);
}

int wineBottomUp(int arr[], int n,vector<vector<int> >& dp1){
    
    for(int i=n-1; i>=0; i--){
        for(int j = 0; j<n; j++){
            
            if(i == j){
                dp1[i][j] = n * arr[i];
            }

            if(i<j){

                int y = n - (j - i);
                int pick_left =  y * arr[i] + dp1[i+1][j];
                int pick_right = y * arr[j] + dp1[i][j-1];

                dp1[i][j] = max(pick_left, pick_right);
            }
        }
    }
   
   return dp1[0][n-1];
}


int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int arr[] = {2,3,5,1,4};
    int n = 5;
    vector<vector<int> > dp(n+1, vector<int>(n+1,0));
    
    cout<<wine(arr, 0, n-1, 1, dp)<<endl;

    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            cout<<dp[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<endl;

    vector<vector<int> > dp1(n+1, vector<int>(n+1,0));
    
    cout<<wineBottomUp(arr, n, dp1)<<endl;
    
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            cout<<dp1[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<endl;

	return 0;
}