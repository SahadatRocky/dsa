#include<bits/stdc++.h>
using namespace std;

//Approch 2
int palindromePartitioningMinCuts(string s){
   
   int n = s.length();
	vector<vector<bool> > isPalin(n+1,vector<bool>(n,false));
	
	for(int i=0;i<n;i++){
		isPalin[i][i] = true;
	}
	
	//2d dp palindromic grid for helper
	// tell whether a string i...j is a palindrome or not
	for(int len=2;len<=n;len++){
		for(int i=0;i<=n-len;i++){
			//substring i to j
			int j = i + len - 1;
			if(len==2){
				isPalin[i][j] = (s[i]==s[j]);
			}
			else{
				isPalin[i][j] = (s[i]==s[j] and isPalin[i+1][j-1]);
			}
		}
	}
	
	//min cut logic
	vector<int> cuts(n+1,INT_MAX);
	
	for(int i=0;i<n;i++){
		if(isPalin[0][i]){
			cuts[i] = 0;
		}
		else{
			cuts[i]  = cuts[i-1] + 1;
			for(int j=1;j<i;j++){
				if(isPalin[j][i] and cuts[j-1] + 1 < cuts[i]){
					cuts[i] = cuts[j-1] + 1;
				}
			}
		}
	}
  return cuts[n-1];
}



//Approch 1

bool isPalindrome(string s,int i,int j){
    while(i<j){
       if(s[i++] != s[j--]){
    	  return false;
       }
    }
    return true;
}

int palindromePartitioning(string s){
    
    int n = s.size();
    vector<vector<int> > dp(n+1, vector<int>(n+1, 0));

    for(int len = 1; len<n; len++){
    	for(int row = 0, col = len; row < n-len ; row ++, col++){
            if(isPalindrome(s,row,col)){
                dp[row][col] = 0;
            }else{
            	dp[row][col] = INT_MAX;
                for(int k = row; k < col; k++){
                	dp[row][col] = min(dp[row][col], 1 + dp[row][k] + dp[k+1][col] );
                }   
            }
    	}
    }

    return dp[0][n-1];
}

int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string str = "abcde";

    cout<<palindromePartitioningMinCuts(str)<<endl;

    cout<<palindromePartitioning(str)<<endl;

	return 0;
}

/*

Input : str = “geek”

Output : 2

We need to make minimum 2 cuts, i.e., “g | ee | k”



Input : str = “aaaa”

Output : 0

The string is already a palindrome.



Input : str = “abcde”

Output : We need to make 4 cuts, i.e., “a | b | c | d | e”



Input : str = “abbac”

Output : 1 i.e., “abba | c”


*/