#include<bits/stdc++.h>
using namespace std;

int MCM(int n,vector<int>& arr){
     
     vector<vector<int>> dp(n+1, vector<int>(n+1, 0));

     for(int len = 2; len<n; len++){
        for(int row = 0,col = len; row< n-len; row++,col++){
            dp[row][col] = INT_MAX;
            for(int k = row+1; k<col; k++){
                dp[row][col] = min(dp[row][col], 
                    dp[row][k] + dp[k][col] + 
                    arr[row]*arr[k]*arr[col]);
            }
        }
     }
     return dp[0][n-1];
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 5;
    vector<int> arr{2,3,4,1,3};
    
    cout<<MCM(n, arr)<<endl;

	return 0;
}

/*
input
int n = 5;
int arr[2,3,4,1,3];

output
24

*/