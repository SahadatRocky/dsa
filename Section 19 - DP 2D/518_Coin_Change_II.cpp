#include<bits/stdc++.h>
using namespace std;

int coinChange2(vector<int> &coins,int i,vector<vector<int> >&dp,int amount){
     
    //base case
    if(i >= coins.size()){
       return 0;
    }
    
    if(i < coins.size()){
       if(amount == 0){
       	return 1;
       }
    }
    //memoization

    if(dp[i][amount] != -1){
    	return dp[i][amount];
    } 

    //rec case 
    int ans = 0;
    if(coins[i] <= amount){
    	ans += coinChange2(coins,i,dp,amount - coins[i]);
    }
    ans+= coinChange2(coins,i+1,dp,amount);
    
    return dp[i][amount] = ans;
}


int main(){
   
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<int> coins{1,2,5};
    int amount = 5;
    int n = coins.size();
    vector<vector<int> > dp(n+1, vector<int>(amount+1, -1));
   
    cout<<coinChange2(coins,0,dp,amount)<<endl;

   
   return 0;
}

/*

Input: amount = 5, coins = [1,2,5]
Output: 4
Explanation: there are four ways to make up the amount:
5=5
5=2+2+1
5=2+1+1+1
5=1+1+1+1+1

*/