#include <bits/stdc++.h>

using namespace std;

int gcd(int a, int b){
    if(a<b){
        swap(a,b);
    } 
    
    if(b == 0){
        return a;
    }
    
    return gcd(b, a%b);
}

bool isGoodArray(vector<int>& nums) {
        int ans = 0;
        for(auto x: nums){
            ans = gcd(x, ans);
        }

   return ans == 1;     
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums{12,5,7,23};
    cout<<isGoodArray(nums)<<endl;

    return 0;
}

/*

Input: nums = [12,5,7,23]
 
Output: true
 
Explanation: Pick numbers 5 and 7.
5*3 + 7*(-2) = 1

*/