#include <bits/stdc++.h>
using namespace std;

#define int long long int

const int P = 7, N = 1e5;

int fact[N];

int addm(int a,int b){
   
    return (a+b) % P;
}

int subm(int a,int b){
   
   return ((a-b) % P + P) % P;
}

int mulm(int a, int b){
   return (a*b) % P;
}

int powrm(int a, int b){
   int res = 1;
   while(b){
   	if(b&1){
   		res = mulm(res,a);
   	}
   	a = mulm(a,a);
   	b = b/2;
   }

   return res;
}

int divm(int a, int b){
   return mulm(a, powrm(b, P-2)); 
}

int calculate_factorials(){

	fact[0]=1;
	for(int i=1; i<N; i++){
		fact[i] = mulm(fact[i-1], i);
	}
}

int inv(int x){
	return powrm(x, P-2);
}

int ncr(int n, int r){
    return mulm( mulm(fact[n], inv(fact[r])), inv(fact[n-r]));
    
}

int32_t main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    cout<<addm(3,5)<<endl;
    cout<<subm(3,5)<<endl;
    cout<<mulm(324,324)<<endl;
    cout<<divm(56,2)<<endl;

    calculate_factorials();
    cout<<fact[5]<<endl;
    cout<<ncr(5,2)<<endl;
    return 0;
}