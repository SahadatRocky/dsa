#include<bits/stdc++.h>
#define int long long int

using namespace std;

const int mod = 1e9 + 7;

int fastMultiplication(int a,int n){
     
     int res = 0;

     while(n>0){
     	int lastSetBit = n&1;
     	if(lastSetBit){
     		res = a + res;
     		res = res % mod;
     	}
     	a = a + a;
     	a = a % mod;
     	n = n>>1;
     }

     return res;
}


int32_t main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int a = 20,n = 20;
    cout<<"FastNyltiplication:"<<fastMultiplication(a,n)<<endl;
	return 0;
}
