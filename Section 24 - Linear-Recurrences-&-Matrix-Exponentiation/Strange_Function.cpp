#include <bits/stdc++.h>
using namespace std;
#define int long long int
const int mod = 9;

int pow(int a, int n)
{
    int res = 1;

     while(n>0){
        int lastSetBit = n&1;
        if(lastSetBit){
            res = a * res;
            res = res % mod;
        }
        a = a * a;
        a = a % mod;
        n = n>>1;
     }

    if (res == 0)
        return 9;

    return res;
}

int32_t main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    int t;
    cin >> t;
    while (t--)
    {
        int n, a;
        cin >> a >> n;
        cout << pow(a, n) << endl;
    }

    return 0;
}