#include<bits/stdc++.h>
#define int long long int

using namespace std;

const int mod = 1e9 + 7;

int BinaryExponentiation(int a,int n){
    
    int res = 1;
    
    while(n>0){
    	int lastSetBit = n&1;
    	if(lastSetBit){
    		res = a * res;
            res = res % mod;
    	}

    	a = a*a;
        a = a % mod;
    	n = n>>1; 
    }

    return res;
}

int32_t main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 4;
    if(n%3 == 0 ){
        cout<<BinaryExponentiation(3, n/3) % mod <<endl;
    }
    else if(n%3 == 1){
       if(n == 1){
        cout<<1<<endl;
       }else{
        cout<<(BinaryExponentiation(3, n/3 - 1 ) * 4 )% mod <<endl;
       }
    }else{
        if(n == 2){
        cout<<2<<endl;
       }else{
        cout<<(BinaryExponentiation(3, n/3 ) * 2 )% mod <<endl;
       }
    }
   
	return 0;
}


/*
Input

n = 4
Output

4

*/