#include<bits/stdc++.h>
using namespace std;

string solve(int A,int B, int C){
     
     if(C%2 == 0 || (A >= 0 && B >= 0)){
         A = abs(A);
         B = abs(B);
         
         if(A>B){
             return ">";
         }else if(A<B){
             return "<";
         }else{
             return "=";
         }
         
     }else{
         if(A<0 && B>=0){
             return "<";
         }else if(A>=0 && B<0){
             return ">";
         }else{
             if(abs(A) > abs(B)){
                 return ">";
             }else if(abs(A) < abs(B)){
                 return "<";
             }else{
                 return "=";
             }
         }
     }
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    int A = 3, B = 2, C = 4;
    cout<<solve(A, B, C)<<endl; 
    
	return 0;
}

/*

Example 1:

Input

A = 3,  B = 2 , C = 4
Output

">"
Explanation:

We have pow(3,4)=81 and pow(2,4)=16.

Example 2:

Input

A = -7,  B = 7 , C = 2
Output

"="
Explanation:

We have pow(−7,2)=49 and pow(7,2)=49.

*/