#include<bits/stdc++.h>
#define int long long int

using namespace std;

const int mod = 1e9 + 7;

int ModularBinaryExponentiation(int a,int n){
    
    int res = 1;
    
    while(n>0){
    	int lastSetBit = n&1;
    	if(lastSetBit){
    		res = a * res;
            res = res % mod;
    	}

    	a = a*a;
        a = a % mod;
    	n = n>>1; 
    }

    return res;
}

int32_t main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int a = 2,n = 200;
    cout<<"ModularBinaryExponentiation:"<<ModularBinaryExponentiation(a,n)<<endl;
	return 0;
}
