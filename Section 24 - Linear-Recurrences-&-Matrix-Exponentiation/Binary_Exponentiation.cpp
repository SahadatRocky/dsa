#include<bits/stdc++.h>
using namespace std;

int BinaryExponentiation(int a,int n){
    
    int res = 1;
    
    while(n>0){
    	int lastSetBit = n&1;
    	if(lastSetBit){
    		res = a * res;
    	}

    	a = a*a;
    	n = n>>1; 
    }

    return res;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int a = 2,n = 10;
    cout<<"BinaryExponentiation:"<<BinaryExponentiation(a,n)<<endl;
	return 0;
}
