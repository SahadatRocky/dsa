#include<bits/stdc++.h>
using namespace std;

const int N = 1e5, M = 20;
vector<int> gr[N];
int dep[N],parent[N][M];


// O(N*M)
void DFS(int cur, int par){
   dep[cur] = dep[par] + 1;
   parent[cur][0] = par;
   
   for(int j=1; j<M; j++){
    parent[cur][j] = parent[parent[cur][j-1]][j-1];
   } 

   for(auto x : gr[cur]){
      if(x != par){
        DFS(x,cur);
      }
   } 
}

// O(M) = logN
int LCA(int u, int v) {
    if (u == v) return u;
    if (dep[u] < dep[v]) swap(u, v);
    int diff = dep[u] - dep[v];
    for (int j = M - 1; j >= 0; j--) {
        if ((diff >> j) & 1) {
            // jth bit of diff is set
            u = parent[u][j];
        }
    }
    // u and v are on the same level
    for (int j = M - 1; j >= 0; j--) {
        if (parent[u][j] != parent[v][j]) {
            u = parent[u][j];
            v = parent[v][j];
        }
    }

    // Par[v][0]
    return parent[u][0];
}

// O(1)
int LengthFromUtoV(int u, int v) {
    int lca = LCA(u, v);
    return dep[u] + dep[v] - 2 * dep[lca];
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n;
    cin>>n;

    for(int i=1; i<n; i++){
       int x,y;
       cin>>x>>y;
       gr[x].push_back(y);
       gr[y].push_back(x);
    }   

    DFS(1,0);
    
    for(int i=1; i<=12; i++){
        cout<<i<<"->";
        for(int j=0; j<4; j++){
            cout<<parent[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<endl;

    cout<<"------------->>>"<<endl;
    cout << LengthFromUtoV(9, 12) << '\n';
    cout << LengthFromUtoV(10, 8) << '\n';

	return 0;
}

/*
input
12
1 2
1 3
2 4
2 5
2 6
4 10
5 8
8 9
8 11
3 7
7 12

output

1
2
8


*/