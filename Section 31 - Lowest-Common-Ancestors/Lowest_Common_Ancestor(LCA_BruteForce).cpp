#include<bits/stdc++.h>
using namespace std;

const int N = 1e5;
vector<int> gr[N];
int dep[N],parent[N];

void DFS(int cur, int par){
   
   parent[cur] = par;
   dep[cur] = dep[par] + 1;

   for(auto x : gr[cur]){
      if(x != par){
        DFS(x,cur);
      }
   } 
}

int LCA(int u,int v){
    
    if(u == v){
        return u;
    }

    //depth of u is more then depth of v

    if(dep[u] < dep[v]){
        swap(u,v);
    }

    int diff = dep[u] - dep[v];
    
    //dept of both nodes same
    while(diff--){
       
       u = parent[u]; 
    }

    //untill they are equal nodes keep clambing
    while(u != v){
       u = parent[u];
       v = parent[v];
    }
    
    return u;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n;
    cin>>n;

    for(int i=1; i<n; i++){
       int x,y;
       cin>>x>>y;
       gr[x].push_back(y);
       gr[y].push_back(x);
    }   

    DFS(1,0);

    // cout<<LCA(9,12)<<endl;
    cout<<LCA(10,8)<<endl;
    // cout<<LCA(9,11)<<endl;
	return 0;
}

/*
input
12
1 2
1 3
2 4
2 5
2 6
4 10
5 8
8 9
8 11
3 7
7 12

output

1
2
8


*/