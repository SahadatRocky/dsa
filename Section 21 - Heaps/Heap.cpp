#include<bits/stdc++.h>
using namespace std;

class Heap{

	vector<int> v;
	void heapify(int i){

		int left = 2*i;
		int right = 2*i + 1;

		int minIdx = i;
		if(left < v.size() and v[left] < v[i]){
			minIdx = left;
		}
		if(right < v.size() and v[right] < v[minIdx]){
			minIdx = right;
		}

		if(minIdx!=i){
			swap(v[i],v[minIdx]);
			heapify(minIdx);
		}
	}
public:
	Heap(int default_size=10){
		v.reserve(default_size+1);
		v.push_back(-1);
	};

	void push(int data){

		//ad data to end of the heap
		v.push_back(data);

		int idx = v.size() - 1;
		int parent = idx/2;

		while(idx>1 and v[idx] < v[parent]){
			swap(v[idx],v[parent]);
			idx = parent;
			parent = parent/2;
		}
	}

	int top(){
		return v[1];
	}

	void pop(){
		//1. Swap first and last element
		int idx = v.size() - 1;
		swap(v[1],v[idx]);
		v.pop_back();
		heapify(1);
	}	

	bool empty(){
		return v.size()==1;
	}
};

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int marks[] = {90,80,12,13,15,56,94};

	Heap h;

	for(int x:marks){
		h.push(x); //logn
	}

	while(!h.empty()){
		cout<< h.top() <<endl;
		h.pop(); //logn
	}
    
	return 0;
}
