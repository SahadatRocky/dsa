
#include<bits/stdc++.h>
using namespace std;

void min_pair(vector<int>& a1,vector<int>& a2){
    
    //sort the array
    sort(a2.begin(), a2.end());
    
    //iterator over 1 array and look for closest elements in the second array
    
    int diff = INT_MAX;
    int p1,p2;

    for(auto x : a1){
         auto lb = lower_bound(a2.begin(), a2.end(), x) - a2.begin();
         cout<<"lb:"<<lb<<endl; 
         //left

         if(lb>=1 && x-a2[lb-1] < diff ){
            diff = x-a2[lb-1];
            p2 = x;
            p1 = a2[lb-1]; 
         } 
         if(lb!=a2.size() && a2[lb] - x < diff){
             diff = a2[lb] - x;
             p1 = x;
             p2 = a2[lb];
         }
    } 

    cout<<"min_pair:"<<p1<<" "<<p2<<endl;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    vector<int> a1 = {-1, 5, 10, 20, 3};
    vector<int> a2 = {26, 134, 135, 15, 17}; 
    
    min_pair(a1,a2);

    return 0;
}