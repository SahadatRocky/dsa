#include<bits/stdc++.h>
using namespace std;

int minEatingSpeed(vector<int> &piles, int h) {
    int n = piles.size();
        int start = 1;
        int end = 0;
        for(auto x: piles){
            end = max(end, x);
        }
        int result = end; 
        while(start <= end) {
            int k = (start + end)/2;
            
            long int hours = 0;
            for(auto x : piles ){
                hours += ceil((double) x/k);
            }
            if(hours <= h) {
                result = min(result, k);
                end = k - 1;
            } else {
                start = k + 1;
            }
        }
        
        return result;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    vector<int> piles{30,11,23,4,20}; 
    int h = 5;
    cout<<minEatingSpeed(piles, h)<<endl;

    return 0;
}

/*

Input: piles = [30,11,23,4,20], h = 5
 
Output: 30

*/