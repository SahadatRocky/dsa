#include<bits/stdc++.h>
using namespace std;

int solve(int n,int k,vector<int>& a){
    int l=1,r=1e9,mid,ans;
    while(l<=r){
        mid=(l+r)/2;
        int sum=0;
        for(int i=0;i<n;i++){
            sum+=(a[i]-1)/mid;
        }
        if(sum<=k){
            ans=mid;
            r=mid-1;
        }
        else{
            l=mid+1;
        }
    }
    return ans;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n = 2;
    int k= 3;
    vector<int> a{7, 9};
    cout<<solve(n,k,a)<<endl;

    return 0;
}

/*

Input: n= 2, k =3, a= [7, 9]
 
Output: 4
Explanation: 
First, we will cut the log of length 7 at a point whose distance from an end of the log is 3.5, resulting in two logs of length 3.5 each.
Next, we will cut the log of length 9 at a point whose distance from an end of the log is 3, resulting in two logs of length 3 and 6.
Lastly, we will cut the log of length 6 at a point whose distance from an end of the log is 3.3, resulting in two logs of length 3.3 and 
2.7 .
In this case, the longest length of a log will be 3.5, which is the shortest possible result. After rounding up to an integer, the output should be 4.

*/