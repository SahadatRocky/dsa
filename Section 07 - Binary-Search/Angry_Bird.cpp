
#include<bits/stdc++.h>
using namespace std;

bool canplaceBird(int B,int n,vector<int>& nests,int sep){
      
      int birds = 1;
      int location = nests[0];
      
      for(int i = 1; i<n; i++){
         int currentLocation = nests[i];

         if(currentLocation - location >= sep){
            birds++;
            location = currentLocation;

            if(birds == B){
               return true;
            }
         }
      }
   
   return false;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int B = 3;
    vector<int> nests{1,2,4,8,9};
    
    //sorting
    sort(nests.begin(), nests.end());

    int n = nests.size();
    
    int s =0; 
    int e = nests[n - 1] - nests[0]; //maximum possible ans 
    int ans = -1;
    while(s<=e){
      
      int mid  = (s+e)/2;
      
      bool canplace = canplaceBird(B, n, nests, mid);

      if(canplace){
          ans = mid;
          s= mid+1;
      }else{
         e =mid - 1;
      }

    }

    cout<<ans<<endl;

    return 0;
}