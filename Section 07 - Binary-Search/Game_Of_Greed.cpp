
#include<bits/stdc++.h>
using namespace std;

bool divideAmongK(vector<int>& arr,int n,int k,int limit){
    
    //return true if every partition get atleast limit no of coins

    int cnt = 0;
    int current_sum = 0;

    for(int i=0; i<n; i++){
        if(current_sum + arr[i] >= limit){
            cnt += 1;
            current_sum = 0;
        }else{
            current_sum += arr[i];
        }
    } 
    return cnt>=k;
}


int k_partition(vector<int> &arr,int n,int k){
    
    int s = 0;
    int e = 0;
    int ans = 0;

    for(auto x: arr){
        e +=x;
    }    

    while(s<=e){
        int mid = (s+e)/2;
        
        bool isPossible = divideAmongK(arr,n,k,mid);
        if(isPossible){
           ans = mid;
           s= mid + 1; 
        }else{
            e = mid - 1;
        }
    }

    return ans;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    vector<int> arr{1,2,3,4};
    int n = arr.size();
    int k = 3;

    cout<<k_partition(arr,n,k)<<endl;


    return 0;
}