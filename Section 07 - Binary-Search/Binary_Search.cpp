
#include<bits/stdc++.h>
using namespace std;

 int binarySearch(int arr[],int n,int key){
      
      int s = 0;
      int e = n - 1;

      while(s<=e){
         
         int mid = (s+e)/2;

         if(arr[mid]  == key){
            return mid;
         }
         else if(arr[mid] > key){
            e = mid - 1;
         }
         else{
            s = mid + 1;
         }
      }
    return -1;
 }

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int arr[] = {-1,0,3,5,9,12};
    int n = sizeof(arr)/sizeof(int);
    int key = 9;
    
    int result = binarySearch(arr,n,key);
    
    result == -1 ? cout<<"Not Found" : cout<<result<<endl;

    return 0;
}


/*

Input: nums = [-1,0,3,5,9,12], target = 9
Output: 4
Explanation: 9 exists in nums and its index is 4
Example 2:

Input: nums = [-1,0,3,5,9,12], target = 2
Output: -1
Explanation: 2 does not exist in nums so return -1

*/