#include<bits/stdc++.h>
using namespace std;

int guessNumber(int n) {
        
        int s = 1;
        int e = n;
        
        while(s<=e){
            
            int mid = s + (e-s)/2;
            int res = guess(mid);
            
            if(res == 0){
                return mid;
            }
            
            else if(res == 1){
                s = mid + 1;
            }
            else{
                e = mid - 1;
            }
        }
        
        return -1;
    }

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
 
    int n = 10, pick = 6;
    cout<<guessNumber(n)<<endl;
    return 0;
    
}


/*

Input: n = 10, pick = 6
Output: 6
Example 2:

Input: n = 1, pick = 1
Output: 1
Example 3:

Input: n = 2, pick = 1
Output: 1

*/