#include<bits/stdc++.h>
using namespace std;

bool isPerfectSquare(int num) {
    long long int start = 1;
    long long int end = num;
        
    while(start <= end){
        long long int mid = (start + end)/2;
            
        if(mid*mid == num){
            return true;
        }
        if(mid*mid < num){
            start = mid + 1;
        }
        else{
           end = mid - 1;   
        }
    }
        
    return false;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int num = 16;
    isPerfectSquare(num) ? cout<<"True"<<endl : cout<<"False"<<endl;

    return 0;
}

/*

Example 1:

Input: num = 16
 
Output: true
Example 2:

Input: num = 8
 
Output: false

*/