#include <bits/stdc++.h>
using namespace std;

int firstOccrence(vector<int>& nums,int target){
        int ans = -1;
        
        int s = 0;
        int e = nums.size() - 1;
        
        while(s<=e){
            
            int mid = (s+e)/2;
            
            if(nums[mid] == target){
                ans = mid;
                e = mid - 1;
            }
            else if(nums[mid] < target){
                s = mid + 1;
            }else{
                e = mid - 1;
            }
        }
        return ans;
    }
    
    int lastOccrence(vector<int>& nums,int target){
        int ans = -1;
        int s = 0;
        int e = nums.size() - 1;
        
        while(s<=e){
            int mid = (s+e)/2;
            
            if(nums[mid] == target){
                ans = mid;
                s = mid + 1;
            }else if(nums[mid] < target){
                s = mid + 1;
            }else{
                e = mid - 1;
            }
        }
        return ans;
    }


  vector<int> searchRange(vector<int>& nums, int target) {
        vector<int> res;
        int f = firstOccrence(nums,target);
        int l = lastOccrence(nums,target);
        res.push_back(f);
        res.push_back(l);
        
        return res;
    }  


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    return 0;     
} 