
#include<bits/stdc++.h>
using namespace std;

int lowerBound(vector<int>& arr,int key){
   
   int ans = -1;

   int s = 0;
   int e = arr.size() - 1;

   while(s<=e){

      int mid = (s+e)/2;

      if(arr[mid] == key){
         return arr[mid];
      }
      else if(arr[mid] > key){
         e = mid - 1;
      }
      else{
         s = mid + 1;
         ans = arr[mid];
      }

   }
   return ans;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<int> arr{-1, -1, 2, 3, 5};
    int key = 4;
    cout<<lowerBound(arr,key)<<endl;

    return 0;
}


/*

Example 1  :

A = [-1, -1, 2, 3, 5]

Val = 4

Answer :  3

Since 3 is just smaller than 4 in the array.

Example 2  :

A = [1, 2, 3, 4, 6]

Val = 4

Answer :  4
Since 4 is equal to 4.  


*/