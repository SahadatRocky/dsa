
#include<bits/stdc++.h>
using namespace std;

 float squreRoot(int n,int p){
      
      int s = 0;
      int e = n;
      float ans = 0;
      while(s<=e){
         
         int mid = (s+e)/2;

         if(mid*mid  == n){
            return mid;
         }
         else if(mid*mid < n){
            ans = mid;
            s = mid + 1;
         }
         else{
      
            e = mid - 1;
         }
      }

      cout<<ans<<endl;

      //linear search for each place
      float inc = 0.1;
      for(int place = 1; place<=p; place++){
          
          while(ans*ans <= n){
              ans+=inc; 
          }
          
          //take one step back
          
          ans = ans - inc;

          inc = inc/10;
      }

    return ans;
 }

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n,p;
    cin>>n>>p;
    cout<<squreRoot(n,p)<<endl;
    return 0;
}

/*

10
4

3.1622

*/