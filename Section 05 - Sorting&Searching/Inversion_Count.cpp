
#include<bits/stdc++.h>
using namespace std;

int merge(int arr[],int s,int e){
      
      int i = s;
      int mid = (s+e)/2;
      int j = mid + 1;
      
      vector<int> v;
      int cnt = 0;
      while(i<=mid && j<=e){
          
          if(arr[i] < arr[j]){
            v.push_back(arr[i++]);
          }else{
            cnt += (mid - i + 1);
            v.push_back(arr[j++]);
          }
      }


      while(i<=mid){
        v.push_back(arr[i++]);
      }

      while(j<=e){
        v.push_back(arr[j++]);
      }

     int k =0;
     for(int i=s; i<=e; i++){
        arr[i] = v[k];
        k++;
     }

     return cnt;
}


int inversionCount(int arr[],int s,int e){
     
     if(s>=e){
        return 0;
     }

     int mid = (s+e)/2;
    int c1 =  inversionCount(arr,s,mid);
    int c2 = inversionCount(arr,mid+1,e);

    int ci = merge(arr,s,e);

    return c1 + c2 + ci;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int arr[] = {0,5,2,3,1};
    int n = sizeof(arr)/sizeof(int);
    
    int s = 0;
    int e = n - 1;

    cout<<inversionCount(arr,s,e)<<endl;


	return 0;
}
