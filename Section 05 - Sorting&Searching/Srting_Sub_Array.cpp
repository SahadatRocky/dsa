#include<bits/stdc++.h>
using namespace std;

pair<int,int> subarraySorting(vector<int> a){
    //Expected Optimal Complexity O(n) Time, O(1) Space
    //may be start with O(NLogN) and try to improve
    //complete this function

    vector<int> b(a);
    sort(a.begin(), a.end());
    
    int i = 0;
    int n = a.size();

    while(i<n && a[i] == b[i]){
      i++;
    }

    int j = a.size() - 1;
    while(j>=0 && a[j] == b[j]){
    	j--;
    }

    if(i == a.size()){
    	return {-1,-1};
    }
   
    return {i,j};
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<int> arr{0, 2, 4, 7, 10, 11, 7, 12, 13, 14, 16, 19, 29};
    
    pair<int,int> p = subarraySorting(arr);
    
    cout<<p.first<<" "<<p.second<<endl;

    return 0;
}

/*

Sample Input

[0, 2, 4, 7, 10, 11, 7, 12, 13, 14, 16, 19, 29]
Sample Output

[4,6]
Explanation

If we sort the subarray [10, 11, 7] then entire array becomes sorted.


*/