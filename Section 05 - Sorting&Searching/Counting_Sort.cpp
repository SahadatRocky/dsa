
#include<bits/stdc++.h>
using namespace std;

void countingSort(int arr[],int n){
     
     //find largest element
     int largest = -1;
     for(int i=0; i<n; i++){
        largest = max(largest, arr[i]);
     }

     //create a count array/vector
     vector<int> freq(largest+1,0);

     //update the freq array
     for(int i=0; i<n; i++){
        freq[arr[i]]++;
     }

     //put back the element from freq into original array 
     int j = 0;
     for(int i = 0; i<=largest; i++){
         
         while(freq[i] > 0){
            
            arr[j] = i;
            freq[i]--;
            j++; 
         }
     }
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int arr[] = {88,97,10,12,15,1,5,6,12,5,8};
    int n = sizeof(arr)/sizeof(int);
    
    countingSort(arr,n);
    
    for(int i=0; i<n; i++){
    	cout<<arr[i]<<" ";
    }
    cout<<endl;


	return 0;
}
