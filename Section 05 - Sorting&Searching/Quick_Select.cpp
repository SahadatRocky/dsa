
#include<bits/stdc++.h>
using namespace std;

int partition(vector<int> &arr,int s,int e){
     
     int pivot = arr[e];
     int i = s - 1;

     for(int j=s; j<e; j++){
         if(arr[j] < pivot){
          i++;
          swap(arr[i],arr[j]);
         }
     }
     
     swap(arr[i+1], arr[e]);

     return i+1;
     

}


int quickSelect(vector<int> &arr,int s,int e,int k){
     
     int p = partition(arr,s,e);
     
     if(p == k){
        return arr[p];
     }
     else if(k < p){
         quickSelect(arr,s,p-1,k);
     }else{
         quickSelect(arr,p+1,e,k);
     }

}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    vector<int> arr{10,5,2,0,7,6,4};
    int n = arr.size();
    
    int s = 0;
    int e = n - 1;
    
    int k;
    cin>>k;
    cout<<quickSelect(arr,s,e,k)<<endl;

    // for(int i=0; i<n; i++){
    // 	cout<<arr[i]<<" ";
    // }
    // cout<<endl;


	return 0;
}
