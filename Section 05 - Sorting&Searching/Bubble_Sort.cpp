
#include<bits/stdc++.h>
using namespace std;

void bubble_sort(int arr[],int n){
     
     for(int i=n-1; i>0; i--){
         for(int j=0; j<i; j++){
             
             if(arr[j] > arr[j+1]){
             	int  temp = arr[j];
             	arr[j] = arr[j+1];
             	arr[j+1] = temp;
             }
         } 
     }
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int arr[] = {-2,3,4,-1,5,12,6,1,3};
    int n = sizeof(arr)/sizeof(int);
    bubble_sort(arr,n);
    
    for(int i=0; i<n; i++){
    	cout<<arr[i]<<" ";
    }
    cout<<endl;


	return 0;
}
