
#include<bits/stdc++.h>
using namespace std;

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int arr[] = {-2,3,4,-1,5,12,6,1,3};
    int n = sizeof(arr)/sizeof(int);
    
    sort(arr, arr+n);
    reverse(arr,arr+n);
    
    for(int i=0; i<n; i++){
    	cout<<arr[i]<<" ";
    }
    cout<<endl;


	return 0;
}
