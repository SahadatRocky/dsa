
#include<bits/stdc++.h>
using namespace std;

void insertion_sort(int arr[],int n){
     
     for(int i=1; i<n; i++){
         
         int current = arr[i];
         int prev = i - 1;

         while(prev >=0 && arr[prev] > arr[current]){
                 arr[prev+1] = arr[prev];
                 prev = prev - 1;     
         }

         arr[prev+1] = current;
     }  
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int arr[] = {-2,3,4,-1,5,12,6,1,3};
    int n = sizeof(arr)/sizeof(int);
    insertion_sort(arr,n);
    
    for(int i=0; i<n; i++){
    	cout<<arr[i]<<" ";
    }
    cout<<endl;


	return 0;
}
