
#include<bits/stdc++.h>
using namespace std;

void selection_sort(int arr[],int n){
     
     for(int pos=0; pos<n-1; pos++){
           
           int pos_min = pos;

           for(int j = pos; j<n; j++){

                if(arr[j] < arr[pos_min]){
                    pos_min = j;
                }
           }

           swap(arr[pos_min], arr[pos]);
     }
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int arr[] = {5,12,6,1,3};
    int n = sizeof(arr)/sizeof(int);
    selection_sort(arr,n);
    
    for(int i=0; i<n; i++){
    	cout<<arr[i]<<" ";
    }
    cout<<endl;


	return 0;
}
