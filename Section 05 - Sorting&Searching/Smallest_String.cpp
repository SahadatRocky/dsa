
#include<bits/stdc++.h>
using namespace std;

bool compare(string x, string y){
    return x+y < y+x;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    string arr[] = {"a", "ab", "aba"};
    int n = sizeof(arr)/sizeof(int);

    sort(arr, arr+n, compare); 

    for(auto s: arr){
        cout<<s;
    }
    cout<<endl;
	return 0;
}
