#include<bits/stdc++.h>
using namespace std;

pair<int,int> search(int m, int n, vector<vector<int>> v, int k){
    //write your code here.
    int i = 0;
    int j = n - 1;
    
    while( (i>=0 && i<m) && (j>=0 && j<n) ){
    	if(v[i][j] == k){
    		break; 
    	}
    	else if(v[i][j] > k){
          j--;
    	}else{
    		i++;
    	}
    }

    pair<int,int> p = {i,j};
    return p;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);

    int n = 3,m = 3,k = 10;
    
    vector<vector<int>> v{
    	{1, 4, 9},
    	{2, 5, 10},
    	{6, 7, 11}
    };
    
    pair<int,int> p = search(m,n,v,k);
    
    cout<<p.first<<" "<<p.second<<endl;

    return 0;
}


/*

Sample Testcase:

Input:

3 3



1 4 9

2 5 10

6 7 11



10

Output:

{1,2}

Explanation:

Index of 10 in matrix is 1,2.

*/