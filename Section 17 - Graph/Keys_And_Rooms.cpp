#include<bits/stdc++.h>
using namespace std;


bool canVisitAllRooms(vector<vector<int>> rooms){
    
    int n = rooms.size();
    
    int visited[5] = {0};
    queue<int> q;
    // int key = rooms[0][0]; 
    q.push(0);
    visited[0] = 1;

    while(!q.empty()){
       int f = q.front();
       q.pop();

       for(auto nbr : rooms[f]){
       	if(!visited[nbr]){
       		q.push(nbr);
       		visited[nbr] = 1;
       	  }
       }
    }

    for(int i=0; i<n; i++){
    	if(visited[i] == 0){
    		return false;
    	}
    }

    return true;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<vector<int>> rooms = {
        {1},
		{2},
		{3},
		{} 
    };
   cout<<endl; 
   cout<<canVisitAllRooms(rooms)<<endl;


	return 0;
}


/*

Sample Input

 [[1],[2],[3],[]]


Sample Output

true


Explanation

We visit room 0 and pick up key 1.
 
We then visit room 1 and pick up key 2.
 
We then visit room 2 and pick up key 3.
 
We then visit room 3.
 
Since we were able to visit every room, we return true.

*/