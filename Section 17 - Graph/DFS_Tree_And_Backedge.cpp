#include<bits/stdc++.h>

using namespace std;

const int N = 1e5 + 1;

vector<int> gr[N];
int vis[N];
bool cycle = false;

void dfs(int src, int par) {
	cout<<src<<endl;
	vis[src] = 1;
	for (auto nbr : gr[src]) {
		if (!vis[nbr]) {
			dfs(nbr, src);
		}
		else if (nbr != par) {
			cycle = true;
			cout<<src<<" "<<nbr<<endl;
		}
	}
}

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int n, m;
	cin >> n >> m;

	for (int i = 0; i < m; i++) {
		int x, y;
		cin >> x >> y;
		gr[x].push_back(y);
		gr[y].push_back(x);

	}

	for (int i = 1; i <= n; i++) {
		if (!vis[i]) {
			dfs(i, 0);
		}
	}

	if (cycle) {
		cout << "Yes cycle found";
	}
	else {
		cout << "Not found";
	}
	return 0;
}

/*

input:

7 8
1 2
1 3
2 3
2 4
4 5
5 6
6 7
7 4

output:

1
2
3
3 1
4
5
6
7
7 4
4 7
1 3
Yes cycle found
*/