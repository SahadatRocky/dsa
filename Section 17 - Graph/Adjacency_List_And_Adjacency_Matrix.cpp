#include<bits/stdc++.h>
using namespace std;

int G[100][100];
vector<int> gr[100];

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int N,E;
    cin>>N>>E;
    
    for(int i=0; i<N; i++){
    	for(int j=0; j<N; j++){
    		G[i][j] = 0;
    	}
    }

    cout<<"initialize Zero Matrix :"<<endl;
    
    for(int i=0; i<N; i++){
    	for(int j=0; j<N; j++){
    		cout<<G[i][j]<<" ";
    	}
    	cout<<endl;
    } 

    int u,v;
    for(int i=0; i<E; i++){
       cin>>u>>v;
       gr[u].push_back(v);
       gr[v].push_back(u);
       G[u][v] = G[v][u] = 1;
    }

    cout<<"adjacency Matrix "<<endl;

    for(int i=0;i<N;i++){
      for(int j=0;j<N;j++){
            cout<<G[i][j]<<" ";
      }
    cout<<endl;
   } 

    cout<<"adjacency list :"<<endl;
    for(int i=0; i<N; i++){
    	cout<<i<<"->";
    	for(int j=0; j<gr[i].size(); j++){
    		cout<<gr[i][j]<<" ";
    	}
    	cout<<endl;
    }


	return 0;
}

/*
7 8

1 0
1 2
2 3
0 4
3 4
3 5
4 5
5 6

*/
