#include<bits/stdc++.h>
using namespace std;

void dfs(vector<vector<int>>& graph,vector<vector<int>>& result,vector<int>& path,int src, int dest){
    
    path.push_back(src);
    if(src == dest){
        result.push_back(path);
        return;
    }
    for(auto node : graph[src]){
        dfs(graph,result,path,node, dest);
    }
}

vector<vector<int>> allPathsSourceTarget(vector<vector<int>>& graph){
    vector<vector<int>> result;
    vector<int> path;
    int n = graph.size();
    if(n == 0){
        return result;
    }
    dfs(graph,result,path,0, n-1);
    return result;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<vector<int>> graph = {
        {4,3,1},
		{3,2,4},
		{3},
		{4},
        {}
    };

   vector<vector<int>> output = allPathsSourceTarget(graph);
   
   for(auto node : output){
      for(auto x :node){
         cout<<x<<" ";
     }  
     cout<<endl;
   }
   cout<<endl;

	return 0;
}
/*

Input: graph = [[4,3,1],[3,2,4],[3],[4],[]]

Output: [[0,4],[0,3,4],[0,1,3,4],[0,1,2,3,4],[0,1,4]]

*/