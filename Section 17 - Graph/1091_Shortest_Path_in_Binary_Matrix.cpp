#include<bits/stdc++.h>
using namespace std;

 int shortestPathBinaryMatrix(vector<vector<int>>& grid) {
        queue<pair< pair<int,int>, int> > q;
        if(grid[0][0] == 1){
            return -1;
        }

        if(grid[0][0] == 0 && grid.size() == 1 && grid[0].size() == 1){
            return 1;
        }
        q.push({{0,0},1});
        vector<vector<bool>> visited(grid.size()+1, vector<bool>(grid.size()+1, false));
        visited[0][0] = true;
        while(!q.empty()){

            pair<int, int> p = q.front().first;
            int x = p.first;
            int y = p.second;
            int lengthOfPath = q.front().second;
            q.pop();
            int dx[] = { 1, -1, 0, 0, -1, 1,  1, -1};
	        int dy[] = { 0, 0, 1, -1, -1, 1, -1,  1};

            for(int k = 0; k<8; k++){
                int nx = x + dx[k];
                int ny = y + dy[k];
                if(nx >=0 && nx < grid.size() && ny>=0 && ny < grid[0].size() && grid[nx][ny] == 0 && !visited[nx][ny]){
                    q.push({{nx,ny},lengthOfPath+1});
                    visited[nx][ny] = true;
                    if(nx == grid.size()-1 && ny == grid[0].size()-1){
                        return lengthOfPath+1;
                    }
                }
            }
        }
        return -1;
    }


int main() {
   
   freopen("input.txt","r",stdin);
   freopen("output.txt","w",stdout); 

   
   vector<vector<int>> grid = {
        {0,0,0},
        {1,1,0},
        {1,1,0}
    };
    
    cout<<shortestPathBinaryMatrix(grid)<<endl;
}



/*

Input: grid = [[0,1],[1,0]]
Output: 2

Input: grid = [[0,0,0],[1,1,0],[1,1,0]]
Output: 4

Input: grid = [[1,0,0],[1,1,0],[1,1,0]]
Output: -1

*/