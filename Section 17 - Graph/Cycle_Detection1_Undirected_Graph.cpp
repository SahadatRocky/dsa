#include<bits/stdc++.h>
using namespace std;

vector<int> gr[100];
int visited[100] = {0};
bool dfs(int src, int parent){
    
    visited[src] = 1;

    for(auto nbr : gr[src]){
    	if(!visited[nbr]){
            bool hasCycle = dfs(nbr, src);
            if(hasCycle){
            	return true;
            }
    	}else{
           //nbr is visited but nbr should not be equal to parent
    		if(nbr != parent){
                 return true;
    		}
    	}
    }
    
    return false;
}

int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int N,E;
    cin>>N>>E;
    
    int u,v;
    for(int i=0; i<E; i++){
       cin>>u>>v;
       gr[u].push_back(v);
       gr[v].push_back(u);
    }

    cout<<"Adjacency List :"<<endl;
    for(int i=0; i<N; i++){
    	cout<<i<<"->";
    	for(int j = 0; j<gr[i].size(); j++){
    		cout<<gr[i][j]<<" ";
    	}
    	cout<<endl;
    }
    
    cout<<dfs(0,-1);

    return 0;
}

/*

Sample Input

Graph is given as list of edges and total number of vertices V.

V = 3
vector<pair<int,int> > edges = {{1,2},{0,1},{2,0}}
Sample Output

true

*/