#include<bits/stdc++.h>
using namespace std;

vector<int> gr[100];

int visited[100] = {0};

void DFS(int src){
   
   visited[src] = 1;
   cout<<src<<" ";

   //make a dfs call on all it's unvisited nbr

   for(auto nbr : gr[src]){

   	if(!visited[nbr]){
   		DFS(nbr);
   	}
   }
	
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int N,E;
    cin>>N>>E;
    
    int u,v;
    for(int i=0; i<E; i++){
       cin>>u>>v;
       gr[u].push_back(v);
       gr[v].push_back(u);
    }

    cout<<"adjacency list :"<<endl;
    for(int i=0; i<N; i++){
    	cout<<i<<"->";
    	for(int j=0; j<gr[i].size(); j++){
    		cout<<gr[i][j]<<" ";
    	}
    	cout<<endl;
    }
    cout<<endl;
    cout<<"DFS :"<<endl;
    DFS(1);


	return 0;
}

/*
7 8

1 0
1 2
2 3
0 4
3 4
3 5
4 5
5 6

*/