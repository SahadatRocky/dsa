#include<bits/stdc++.h>
using namespace std;

vector<int> adj[200001];
int firstMax[200001];  // to store first-max length.
int secondMax[200001];  // to store second-max length.
int c[200001];   // to store child for path of max length.

// calculate for every node x the maximum
// length of a path that goes through a child of x
void dfs(int v, int p) {
	firstMax[v] = 0;
	secondMax[v] = 0;
	for (auto x : adj[v]) {
		if (x == p)continue;
		dfs(x, v);
		if (firstMax[x] + 1 > firstMax[v]) {
			secondMax[v] = firstMax[v];
			firstMax[v] = firstMax[x] + 1;
			c[v] = x;
		}
		else if (firstMax[x] + 1 > secondMax[v]) {
			secondMax[v] = firstMax[x] + 1;
		}
	}
}

// calculate for every node x the
// maximum length of a path through its parent p
void dfs2(int v, int p) {
	for (auto x : adj[v]) {
		if (x == p) continue;
		if (c[v] == x) {
			if (firstMax[x] < secondMax[v] + 1) {
				secondMax[x] = firstMax[x];
				firstMax[x] = secondMax[v] + 1;
				c[x] = v;
			}
			else {
				secondMax[x] = max(secondMax[x], secondMax[v] + 1);
			}
		}
		else {
			secondMax[x] = firstMax[x];
			firstMax[x] =  firstMax[v] + 1;
			c[x] = v;
		}
		dfs2(x, v);
	}
}



vector<int> treeDistances(int n, vector<vector<int>>edges)
{
    vector<int> res;
    dfs(1, -1);
	dfs2(1, -1);

	for (int i = 1; i <= n; i++) {
		res.push_back(firstMax[i]);
		// cout << firstMax[i] << " ";
	}
    return res;
}

int main() {
	
	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
	int n = 5;

	vector<vector<int>> edges{
		{1,2},
		{1,3},
		{3,4},
		{3,5}
	};

	for (int i = 0; i < edges.size(); i++) {
		int a = edges[i][0], b= edges[i][1];
		// cin >> a >> b;
		adj[a].push_back(b);
		adj[b].push_back(a);
	}

    vector<int> output = treeDistances(n, edges);
    
    for(auto x : output){
    	cout<<x<<" ";
    }
	
	return 0;
}

/*

n = 5
edges = [
    [1, 2],
    [1, 3],
    [3, 4],
    [3, 5]
]
Output

[2, 3, 2, 3, 3]

*/