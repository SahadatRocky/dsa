#include<bits/stdc++.h>
using namespace std;

vector<int> gr[100];
int N,E;
int visited[100] = {0};

void dfs(int i,stack<int>& Stack){
    
    visited[i] = 1;
    
    for(auto nbr : gr[i]){
      if(!visited[nbr]){
         dfs(nbr, Stack);
      }
    } 

   Stack.push(i); 
}

void topological_sort(){
   
    stack<int> Stack;
    for (int i = 0; i < N; i++){
        if (!visited[i]){
          dfs(i, Stack);
        }
    }

    // Print contents of stack
    while (!Stack.empty()) {
        cout << Stack.top() << " ";
        Stack.pop();
    }
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    
    cin>>N>>E;
    
    int u,v;
    for(int i=0; i<E; i++){
       cin>>u>>v;
       gr[u].push_back(v);
    }

    cout<<"adjacency list :"<<endl;
    for(int i=0; i<N; i++){
    	cout<<i<<"->";
    	for(int j=0; j<gr[i].size(); j++){
    		cout<<gr[i][j]<<" ";
    	}
    	cout<<endl;
    }
    cout<<endl;

    cout<<"Topological sort :"<<endl;
    topological_sort();


	return 0;
}

/*
7 8

1 0
1 2
2 3
0 4
3 4
3 5
4 5
5 6

*/