#include<bits/stdc++.h>
using namespace std;

vector<int> gr[100];
int visited[100] = {0};
int stacked[100] = {0};

bool dfs(int src){
   
   visited[src] = 1;
   stacked[src] = 1;

   for(auto nbr : gr[src]){
   	 if(stacked[nbr] == 1){
   	 	return true;
   	 }
   	 else if(!visited[nbr]){
        bool hasCycle = dfs(nbr);
        if(hasCycle){
        	return true;
        }   
   	 }
   } 
   
   // leave a node
   stacked[src] = 0; 
   return false;
}


int main(){

	freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    int N,E;
    cin>>N>>E;
    
    int u,v;
    for(int i=0; i<E; i++){
       cin>>u>>v;
       gr[u].push_back(v);
    }

    cout<<"Adjacency List :"<<endl;
    for(int i=0; i<N; i++){
    	cout<<i<<"->";
    	for(int j = 0; j<gr[i].size(); j++){
    		cout<<gr[i][j]<<" ";
    	}
    	cout<<endl;
    }
    
    cout<<dfs(0);

    return 0;
}

/*

Sample Input

Graph is given as list of edges and total number of vertices V. An edge (x,y) represents a directed edge from x-->y.

V = 3
vector<pair<int,int> > edges = {{1,2},{2,0},{0,1}}
Sample Output

true

*/