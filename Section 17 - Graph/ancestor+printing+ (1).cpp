#include<bits/stdc++.h>

using namespace std;

const int N = 1e5;

vector<int> gr[N];
int parent[N];

void dfs(int src, int par) {
	parent[src] = par;
	for (auto nbr : gr[src]) {
		if (nbr != par) {
			// x is child node
			dfs(nbr, src);
		}
	}
}

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int n;
	cin >> n;
	for (int i = 0; i < n - 1; i++) {
		int x, y;
		cin >> x >> y;
		gr[x].push_back(y);
		gr[y].push_back(x);
	}

	dfs(10, 0);

	int x = 6;
	// print all ancestors of 5

	while (x) {
		cout << x << '\n';
		x = parent[x];
	}
	return 0;
}

/*

input:

10
1 2
2 9
2 4
2 5
5 6
1 3
3 7
3 8
1 10

output:

6
5
10

*/