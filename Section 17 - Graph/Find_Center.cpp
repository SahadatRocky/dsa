#include<bits/stdc++.h>
using namespace std;

int findCenter(vector<vector<int>>& v) {
    
    if(v[0][0] == v[1][0]){
        return v[0][0];
    }
    else if(v[0][0] == v[1][1]){
        return v[0][0];
    }else{
        return v[0][1];
    }
        
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    vector<vector<int>> graph = {
        {1,2},
		{2,3},
		{4,2}
    };

   cout<<findCenter(graph)<<endl;

	return 0;
}
/*

Input: edges = [[1,2],[2,3],[4,2]]

Output: 2

*/