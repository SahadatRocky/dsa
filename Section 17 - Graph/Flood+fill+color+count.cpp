#include<bits/stdc++.h>
using namespace std;
int dfs(vector<vector<int> >& matrix,vector<vector<bool> > &visited,int i,int j,int m,int n,int color ){
    
    //mark the current node as visited
    //cout<<"Visiting cell "<<i<<","<<j<<endl;
    matrix[i][j] = color;
    visited[i][j] = true;
    int cs = 1;
    
    int dx[] = {1,-1,0,0};
    int dy[] = {0,0,1,-1};
    
    for(int k=0;k<4;k++){
        //go the nbr of current node
        int nx = i + dx[k];
        int ny = j + dy[k];
        
        if(nx>=0 and nx<m and ny>=0 and ny<n and matrix[nx][ny]==1 and !visited[nx][ny]){
            cs += dfs(matrix,visited,nx,ny,m,n,color);
        }
    }
    return cs;
}


int largest_island(vector<vector<int>> &matrix) {
  // Write your code here.
    int m = matrix.size();
    int n = matrix[0].size();
    
    vector<vector<bool> > visited(m+1,vector<bool>(n+1,false));
    
    int largest = 0;

    int color = 0;
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            if(!visited[i][j] and matrix[i][j]==1){
                color++;    
                int size = dfs(matrix,visited,i,j,m,n, color); 
                largest = max(largest,size);
            
            }
        }
    
    }
    
  return largest;
}

int main() {

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout); 

    vector<vector<int>> matrix = {
        {1, 0, 0, 1, 0},
        {1, 0, 1, 0, 0},
        {0, 0, 1, 0, 1},
        {1, 0, 1, 1, 1},
        {1, 0, 1, 1, 0}
    };

    cout<<largest_island(matrix)<<endl;
    
    for(int i =0; i<matrix.size(); i++){
        for(int j=0; j<matrix[0].size(); j++){
            cout<<matrix[i][j]<<" ";
        }
        cout<<endl;
    }


  return 0;
}

