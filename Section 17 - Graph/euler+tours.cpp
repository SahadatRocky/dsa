#include<bits/stdc++.h>

using namespace std;

const int N = 1e5;

vector<int> gr[N];

void dfs1(int src, int par) {
	// time in
	cout << src << " ";
	for (auto nbr : gr[src]) {
		if (nbr != par) {
			// nbr is child node
			dfs1(nbr, src);
		}
	}
	// time out
	cout << src << " ";
}

void dfs2(int src, int par) {
	cout << src << " ";
	for (auto nbr : gr[src]) {
		if (nbr != par) {
			// nbr is child node
			dfs2(nbr, src);
			cout << src << " ";
		}
	}
}

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int n;
	cin >> n;
	for (int i = 0; i < n - 1; i++) {
		int x, y;
		cin >> x >> y;
		gr[x].push_back(y);
		gr[y].push_back(x);
	}

	// dfs1(1, 0);

	dfs2(1, 0);
	return 0;
}

/*
input:

11
1 2
1 3
2 4
2 5
5 10
5 6
5 7
3 8
8 9
8 11

output:

*/
