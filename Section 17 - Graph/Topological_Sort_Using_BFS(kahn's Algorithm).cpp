#include<bits/stdc++.h>
using namespace std;

vector<int> gr[100];
int N,E;
vector<int> indegree;

void topological_sort(){
   
   //Iterate over all the edges to find the right indegree
   
   for(int i=0; i<N; i++){
      for(auto nbr : gr[i]){
         indegree[nbr]++;
      }
   }

   
   //BFS
   
   queue<int> q;
   //init the q with nodes having 0 indegree
   for(int i=0; i<N; i++){
      if(indegree[i] == 0){
         q.push(i);
      }
   }

   //start poping
   while(!q.empty()){
      int f = q.front();
      cout<<f<<" ";
      q.pop();
      
      //iterate over the nbr of this node and reduce their indegree by 1
      for(auto nbr: gr[f]){
         indegree[nbr]--;
         if(indegree[nbr] == 0){
            q.push(nbr);
         }
      }
   }
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    
    cin>>N>>E;
    
    int u,v;
    for(int i=0; i<E; i++){
       cin>>u>>v;
       gr[u].push_back(v);
    }

    cout<<"adjacency list :"<<endl;
    for(int i=0; i<N; i++){
    	cout<<i<<"->";
    	for(int j=0; j<gr[i].size(); j++){
    		cout<<gr[i][j]<<" ";
    	}
    	cout<<endl;
    }
    cout<<endl;
    indegree.resize(N,0);
    cout<<"Topological sort :"<<endl;
    topological_sort();
	return 0;
}

/*
7 8

1 0
1 2
2 3
0 4
3 4
3 5
4 5
5 6

*/