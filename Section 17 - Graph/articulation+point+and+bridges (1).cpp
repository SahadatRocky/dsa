#include<bits/stdc++.h>

using namespace std;

const int size = 1e5;

vector<int> gr[size];

int visited[size]= {0}; 
int disc[size] = {0}; 
int low[size] = {0};
int tme = 1;

vector<pair<int, int>> bridges;
set<int> arti_points;

void dfs(int src, int par) {
	visited[src] = 1;
	disc[src] = low[src] = tme++;
	int child = 0;
	for (auto nbr : gr[src]) {

		if (!visited[nbr]) {
			dfs(nbr, src);
			child++;
			// we know low and disc of x
			low[src] = min(low[src], low[nbr]);

			// bridges
			if (low[nbr] > disc[src]) {
				bridges.push_back({src, nbr});
			}

			// articulation points
			if (par != 0 && low[nbr] >= disc[src]) {
				arti_points.insert(src);
			}

		}
		else if (nbr != par) {
			// backedge
			low[src] = min(low[src], disc[nbr]);
		}
	}

	// root is an arti or not
	if (par == 0 && child > 1) {
		arti_points.insert(src);
	}

	return;
}


int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int N,E;
	cin >> N >> E;
    
    int u,v;
	for (int i = 0; i < E; i++) {
		
		cin >> u >> v;
		gr[u].push_back(v);
		gr[v].push_back(u);
	}

	dfs(1, 0);

	for (auto x : arti_points) cout << x << '\n';

	for (auto x : bridges) {
		cout << x.first << " " << x.second << '\n';
	}

	return 0;
}

/*
input:

7 8
1 2
2 3
1 3
2 4
4 5
5 6
6 7
4 7

output :

2 4 
2 4

*/