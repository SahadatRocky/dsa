#include<bits/stdc++.h>

using namespace std;

const int N = 1e5 + 1;

vector<int> gr[N];
int vis[N];
bool cycle = false;

void dfs(int src, int par) {
	// visited and in call stack
	vis[src] = 1;
	for (auto nbr : gr[src]) {
		if (vis[nbr] == 0) {
			dfs(nbr, src);
		}
		else if (nbr != par && vis[nbr] == 1) {
			// backedge
			cycle = true;
		}
	}
	// visited and not in call stack
	vis[src] = 2;
	return;
}

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int n, m;
	cin >> n >> m;

	for (int i = 0; i < m; i++) {
		int x, y;
		cin >> x >> y;
		gr[x].push_back(y);
		gr[y].push_back(x);
	}

	for (int i = 1; i <= n; i++) {
		if (vis[i] == 0) {
			dfs(i, 0);
		}
	}

	if (cycle) {
		cout << "Yes cycle found";
	}
	else {
		cout << "Not found";
	}


	return 0;
}