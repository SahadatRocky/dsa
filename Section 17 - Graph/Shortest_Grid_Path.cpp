#include <bits/stdc++.h>
using namespace std;

class Node{
    public:
        int x,y;
        int dist;

        Node(int x,int y,int dist){
            this->x = x;
            this->y = y;
            this->dist = dist;
        }

};

//comparator should return boolean value, indicating whether the element passed as first argument is considered to go before the second in the specific strict weak ordering 
class Compare{
    public:
    bool operator()(Node a,Node b){
        return a.dist <= b.dist;
    }

};

int shortest_path(vector<vector<int> > &grid){
    
    //----------------/////
    int m = grid.size();
    int n = grid[0].size();
    int i = 0;
    int j = 0;
    
    vector<vector<int> > dist(m+1,vector<int>(n+1,INT_MAX));

    dist[i][j] = grid[0][0];
    set<Node,Compare> s;
    s.insert(Node(i,j,dist[0][0]));

    int dx[] = {0,0,1,-1};
    int dy[] = {1,-1,0,0};

    while(!s.empty()){
        //get the node that is having smallest dist
        auto it = s.begin();
        int cx = it->x;
        int cy = it->y;
        int cd = it->dist;
        s.erase(it);

        //update the neigbours of this node and push them in the set
        for(int k=0;k<4;k++){
            int nx = cx + dx[k];
            int ny = cy + dy[k];
            if(nx>=0 and nx<m and ny>=0 and ny<n and dist[nx][ny] > cd + grid[nx][ny]){
                //remove the old node from the set
                Node temp(nx,ny,dist[nx][ny]);
                if(s.find(temp)!=s.end()){
                    s.erase(s.find(temp));
                }
                //insert the new node in the set
                int nd = grid[nx][ny] + cd;
                dist[nx][ny] = nd;
                s.insert(Node(nx,ny,nd));
            }
        }
    }
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            cout<<dist[i][j]<<" ";
        }
        cout<<endl;
    }
    return dist[m-1][n-1];
}

int main() {

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout); 

    vector<vector<int> > grid = {
        {31, 100, 64, 12, 18},
        {10, 13, 47, 157, 6},
        {100, 113, 174, 11, 33},
        {88, 124, 41, 20, 140},
        {99, 32, 111, 41, 20}
    };

    cout<<shortest_path(grid)<<endl;
  
  return 0;
}

/*

Input

Grid as shown above (input given as vector<vector<int> > grid ).
Hint : Use grid.size() to get rows and grid[0].size() to get columns.

31 100 64 12 18
10 13 47 157 6
100 113 174 11 33
88 124 41 20 140
99 32 111 41 20
Output

An integer denoting the minimum cost.

327 
Explanation

Cells in green are the cells which are visited to complete this route 
327 (= 31 + 10 + 13 + 47 + 65 + 12 + 18 + 
6 + 33 + 11 + 20 + 41 + 20)


*/