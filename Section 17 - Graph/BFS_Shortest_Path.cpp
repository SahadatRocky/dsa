#include<bits/stdc++.h>
using namespace std;
int N,E;
vector<int> gr[100];

int visited[100] = {0};
int dist[100] = {0};
int parent[100] = {-1};

void BFS(int source,int dest){

	queue<int> q;
	q.push(source);
	visited[source] = 1;
   parent[source] = source;
   dist[source] = 0;

	while(!q.empty()){

		int f = q.front();
		// cout<<f<<" ";
		q.pop();
		//push the nbr of the current node inside the q if 
		//they are not already visited.
		for(auto nbr : gr[f]){
			if(!visited[nbr]){
				q.push(nbr);
				visited[nbr] = 1;
				parent[nbr] = f;
				dist[nbr] = dist[f] + 1;
			}
		}
	}

	//print the shortest distance
	for(int i=0; i<N; i++){
		cout<<"shortest dist to "<<i<<" is "<<dist[i]<<endl;
	}

	//print the path from a source to any dest
	if(dest != -1){
		int temp = dest;
		while(temp != source){
			cout<<temp<<"->";
			temp = parent[temp];
		}
		cout<<source<<endl;
	}
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    
    cin>>N>>E;
    
    int u,v;
    for(int i=0; i<E; i++){
       cin>>u>>v;
       gr[u].push_back(v);
       gr[v].push_back(u);
    }

    cout<<"adjacency list :"<<endl;
    for(int i=0; i<N; i++){
    	cout<<i<<"->";
    	for(int j=0; j<gr[i].size(); j++){
    		cout<<gr[i][j]<<" ";
    	}
    	cout<<endl;
    }
    cout<<endl;
    cout<<"BFS Shortest Path :"<<endl;
    BFS(1,6);


	return 0;
}


/*
7 8

1 0
1 2
2 3
0 4
3 4
3 5
4 5
5 6

*/