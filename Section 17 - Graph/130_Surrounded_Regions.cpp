#include<bits/stdc++.h>
using namespace std;


void bfs(vector<vector<char>>& board){
    
    int m = board.size();
    int n = board[0].size();
    vector<vector<bool>> visited(m, vector<bool>(n,false));
    queue<pair<int,int>> q;
    for(int i=0; i<m; i++){
    	for(int j=0; j<n; j++){
    		 if(i == 0 || j == 0 || i == (m-1) || j == (n-1) ){
	    		if(!visited[i][j] && board[i][j] =='O'){
	    			q.push({i,j});
	    			visited[i][j] = true;
	    		}
    	   }	
    	}
    }

    int dx[] = {1,-1,0,0};
    int dy[] = {0,0,-1,1};

    while(!q.empty()){
       
       int x = q.front().first;
       int y = q.front().second;
       q.pop();

       for(int k=0; k<4; k++){

       	   int nx = x + dx[k];
       	   int ny = y + dy[k];

       	   if(nx>=0 && nx < m && ny>=0 && ny<n && !visited[nx][ny] && board[nx][ny] == 'O' ){
            visited[nx][ny] = true;
            q.push({nx,ny});
          }
       }
    }

    for (int i=0; i<n; i++){
            for (int j=0; j<m; j++){
                if (board[i][j]=='O' && !visited[i][j]){
                	board[i][j]='X';
                }
            }
        }
}


void dfs(vector<vector<char>>& board,vector<vector<bool>>& visited,int i,int j,int m,int n){

    visited[i][j] = true;
    int dx[] = {1,-1,0,0};
    int dy[] = {0,0,-1,1};

    for(int k = 0; k<4; k++){
        int nx = i + dx[k];
        int ny = j + dy[k];

        if(nx>=0 && nx < m && ny>=0 && ny<n && !visited[nx][ny] && board[nx][ny] == 'O' ){
            dfs(board, visited,nx,ny,m,n);
        }
    }
}

/*
void dfs1(vector<vector<char>>& board, int i,int j,int m,int n){
    
    if(i<0 || i>=m || j<0 || j>=n || board[i][j] != 'O'){
    	return;
    }
    
    if(board[i][j] == 'O'){
	    char ch = board[i][j];
	    board[i][j] = '#';

	    dfs1(board, visited,i+1,j,m,n);
	    dfs1(board, visited,i,j+1,m,n);
		dfs1(board, visited,i-1,j,m,n);
	    dfs1(board, visited,i,j-1,m,n);
	    
	    //backtrack
	    board[i][j] = ch;    
    }

} 
*/


void surrounded_regions(vector<vector<char>>& board){
      
    int m = board.size();
    int n = board[0].size();

    vector<vector<bool>> visited(m, vector<bool>(n,false));

    for(int i=0; i<m; i++){
     	for(int j=0; j<n; j++){
            if(i == 0 || j == 0 || i == (m-1) || j == (n-1) ){
               if(!visited[i][j] && board[i][j] =='O'){
                 	
                    dfs(board, visited,i,j,m,n);

                    // visited[i][j] = true;
                    // dfs1(board,i,j,m,n);
               }    
            } 
     	}
    }
    
    for(int i=0; i<m; i++){
     	for(int j=0; j<n; j++){
            if(!visited[i][j] && board[i][j] =='O'){
                board[i][j] ='X';
            }    
     	}
    }

}

int main() {

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout); 

    vector<vector<char>> board = {
        {'X','X','X','X'},
        {'X','O','O','X'},
        {'X','X','O','X'},
        {'X','O','X','X'}
    };

    // surrounded_regions(board);
    bfs(board);
    
    for(int i =0; i<board.size(); i++){
        for(int j=0; j<board[0].size(); j++){
            cout<<board[i][j]<<" ";
        }
        cout<<endl;
    }


  return 0;
}