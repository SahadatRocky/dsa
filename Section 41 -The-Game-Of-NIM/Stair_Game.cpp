#include<bits/stdc++.h>
using namespace std;

string solve(int n, vector<int>& balls){
    int xr=0;
    for(int i=0;i<n;i++)
    {
        if(i%2)
        {
            xr^=balls[i];
        }
    }
    if(xr)
    {
        return "first";
    }
    else{
        return "second";
    }
}

int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n = 4;
    vector<int> balls{1, 1, 1, 1};
    cout<<solve(n,balls)<<endl;

    return 0; 
}

/*

Input

n = 4
balls= {1, 1, 1, 1}
Output

second

*/