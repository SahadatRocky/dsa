#include<bits/stdc++.h>
using namespace std;

string solve(int n, vector<int>& A)
{
    int val=0;
    for(int i=0;i<n;i++)
    {
        val^=A[i];
    }
    if(val==0)
    {
        return "Isa";
    }
    else{
        return "Gaitonde";
    }
}


int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n = 2;
    vector<int> A{1,1};

    cout<<solve(n,A)<<endl;

    return 0; 
}

/*

Input

n = 2
A= [1, 1]
Output

Isa
Explanation:

Gaitonde starts the game by removing 1 bullet from any of the piles, and then Isa removes the only bullet in the other pile. Now Gaitonde is left without a valid move and hence Isa wins the game.

*/