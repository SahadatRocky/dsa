#include<bits/stdc++.h>
using namespace std;

string solve(int n, vector<int>& heaps){
    for(int i=0;i<n;i++)
    {
        heaps[i]%=4;
    }
    int xr=0;
    for(int i=0;i<n;i++)
    {
        xr^=heaps[i];
    }
    if(xr)
    {
        return "first";
    }
    else{
        return "second";
    }
}

int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n = 4;
    vector<int> heaps{5, 7, 2, 5};
    cout<<solve(n,heaps)<<endl;

    return 0; 
}

/*

Input

n = 4
heaps = [5, 7, 2, 5]
Output

first

*/