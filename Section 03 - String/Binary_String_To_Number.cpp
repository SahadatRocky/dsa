
#include<bits/stdc++.h>
using namespace std;

int convertStringToInt(string s){
    
    stringstream  ss;
    int temp;
    ss<< s;
    ss>> temp;
    
    return temp;
}


 int decimalToBinary(int n){
     
     int binary = 0;
     int p = 1;
     
     while(n!=0){
         int last_digit = n%10;
         binary += p * last_digit;
         p = p * 2;
         n = n/10;
     }
     
     return binary;
     
 }
 
 
int binaryToDecimal(string s)
{
   int n = convertStringToInt(s);
  return decimalToBinary(n);
}



int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);

    string s;
    cin>>s;

    cout<<binaryToDecimal(s)<<endl;
    
   
    return 0;
}

/*

Sample Input

111


Sample Output

7

*/