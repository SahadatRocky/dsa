
#include<bits/stdc++.h>
using namespace std;

string removeDuplicate(string s){
    // your code goes here
    set<char> ss(s.begin(), s.end());
    string str;
 
    for (auto x : ss)
       str.push_back(x);
 
    return str;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);

    string s;
    cin>>s;

    cout<<removeDuplicate(s)<<endl;
    
   
    return 0;
}


/*

Sample Input

string s = "geeksforgeeks"


Sample Output

"efgkors"

*/