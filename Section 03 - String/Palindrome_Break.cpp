#include<bits/stdc++.h>
using namespace std;

string breakPalindrome(string palindrome) {
        
        int n = palindrome.size();
        
        if(n == 1){
            return "";
        }
        
        for(int i=0; i<n/2;i++){
            if(palindrome[i] != 'a'){
                palindrome[i] = 'a';
                return palindrome;
            }
        }
        
        palindrome[n-1] = 'b';
        
        return palindrome;
    }

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    string s;
    cin>>s;
    cout<<breakPalindrome(s)<<endl;

    return 0;
}


/*
 
 input
  abcd

 output
  aacd

*/