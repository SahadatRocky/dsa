#include<bits/stdc++.h>
using namespace std;

string to_string(int a) {
    string ans;
    stringstream ss;
    ss << a;
    ss >> ans;
    return ans;
}

string convert_to_digital_time(int mins){
    int hours = mins/60;
    int minutes = mins%60;
    string hrs_s = to_string(hours);
    string min_s = minutes < 10 ? "0" + to_string(minutes) : to_string(minutes);
    return hrs_s + ":" + min_s;
}
int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n;
    cin>>n;
    cout<<convert_to_digital_time(n)<<endl;

    return 0;
}

/*
  
  Input

Input is a single integer.

1180
Output

Output is a string denoting the digital clock time.

19:40

*/
