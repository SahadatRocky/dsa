#include <bits/stdc++.h>
using namespace std;

//stack 

bool solve(string s,string t){
    
    stack<char> s1;
    stack<char> s2;

    for(auto ch : s){
        if(ch == '#'){
            if(s1.empty()){
            	continue;
            }
            s1.pop();
        }else{
           s1.push(ch);	
        }  
    }

    for(auto ch : t){
    	if(ch == '#'){
              if(s2.empty()){
              	continue;
              }
              s2.pop();
    	}else{
    		s2.push(ch);
    	}
    }

    while(!s1.empty() && !s2.empty()){
    	if(s1.top() != s2.top()){
    		return false;
    	}

    	s1.pop();
    	s2.pop();
    }

    if(!s1.empty() || !s2.empty()){
    	return false;
    }

    return true;
}

/*

//2 pointer approch
bool solve(string s,string t){
    
    int i = s.size() - 1;
    int j = t.size() - 1;

    int s_c = 0;
    int t_c = 0;

    while(i>=0 || j>=0){
         
        while(i>=0 && ( s_c || s[i] == '#') ){
             s_c += s[i] == '#' ? 1 : -1;
             i--; 
        }  

        while(j>=0 && ( t_c || t[j] == '#')){
        	t_c += t[j] == '#' ? 1 : -1;
        	j--;
        }

        if(i>=0 && j>=0 && s[i] == t[j]){
        	i--;
        	j--;
        }else{
        	return i==-1 && j == -1;
        }
    }

    return true;
}
*/

int main(){
    
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    string  s = "ab#c";
    string t = "ad#c";

    cout<<solve(s,t)<<endl;

	return 0;

}