
#include<bits/stdc++.h>
using namespace std;

bool isPalindrome(string str)
{
    // Start from leftmost and rightmost corners of str
    int s = 0;
    int e = str.length() - 1;
 
    // Keep comparing characters while they are same
    while (s < e)
    {
        if (str[s] != str[e])
        {
            return false;
        }
        s++;
        e--;
    }
    return true;
}
int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);

    string s;
    cin>>s;

    cout<<isPalindrome(s)<<endl;
    
   
    return 0;
}

/*

Sample Input

abcdcba


Sample Output

true


*/

