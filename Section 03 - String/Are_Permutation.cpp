
#include<bits/stdc++.h>
using namespace std;

bool arePermutation(string A, string B)
{
    int a = A.size();
    int b = B.size();
    
    if(a != b){
        return false; 
    }
    
    sort(A.begin(),A.end());
    sort(B.begin(),B.end());
    
    return A == B ? true : false;
}



int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);

    string s1,s2;
    cin>>s1>>s2;

    cout<<arePermutation(s1,s2)<<endl;
    
   
    return 0;
}

/*

Sample Input

string A = "test", B = "ttew"


Sample Output

NO


*/
