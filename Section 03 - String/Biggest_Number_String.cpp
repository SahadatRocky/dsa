#include<bits/stdc++.h>
using namespace std;

bool compare(string s1,string s2){
    return s1 + s2 > s2 + s1;
}


string to_string(int a) {
    string ans;
    stringstream ss;
    ss << a;
    ss >> ans;
    return ans;
}


string concatenate(vector<int> numbers){
    vector<string> output;

    for(int no:numbers){
        output.push_back(to_string(no));
    }
    sort(output.begin(),output.end(),compare);

    string ans = "";
    for(string x:output){
        ans += x;
    }
    return ans;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    vector<int> arr{10,11,20,30,3};
    cout<<concatenate(arr)<<endl;

    return 0;
}


/*

Input

10,11,20,30,3
Output

330201110 

*/