#include<bits/stdc++.h>
using namespace std;

string to_string(int a) {
    string ans;
    stringstream ss;
    ss << a;
    ss >> ans;
    return ans;
}

//str is the input the string
string compressString(const string &str){   
    int n = str.length();
    string output;
    for (int i = 0; i < n; i++) {
 
        // Count occurrences of current character
        int count = 1;
 
        while (i < n - 1 && str[i] == str[i + 1]) {
            count++;
            i++;
        }
 
        // Store the Character and its count
        output += str[i];
        output += to_string(count);
    }
    if(output.length()>str.length()){
        return str;
    }
    return output;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    string s;
    cin>>s;
    cout<<compressString(s)<<endl;

    return 0;
}

/*

Sample Inputs

bbbaaaadexxxxxx
abc
Sample Outputs

b3a4d1e1x6
abc

*/