
#include<bits/stdc++.h>
using namespace std;

string vowel(string S){
    // your code goes here
    int n = S.size();
    string temp;
    for(int i=0; i<n; i++){
        if(S[i] == 'a' ||
            S[i] == 'e' ||
            S[i] == 'i' ||
            S[i] == 'o' ||
            S[i] == 'u'){
                temp += S[i];
            }
    }
    return temp;
} 

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);

    string s;
    cin>>s;

    cout<<vowel(s)<<endl;
    
   
    return 0;
}

/*

Sample Input

S = "aeoibsddaeioudb"


Sample Output

"aeoiaeiou"

*/