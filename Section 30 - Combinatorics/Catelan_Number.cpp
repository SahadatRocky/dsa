#include <bits/stdc++.h>
using namespace std;

int catelan(int n,vector<int>& dp){
    
    if(n == 0 || n == 1){
    	return 1;
    }

    if(dp[n] != 0){
    	return dp[n];
    }

    int ans = 0;
    for(int i=1; i<=n; i++){
        int x = catelan(i-1,dp);
        int y = catelan(n-i,dp);

        ans += x*y;
    }

    return dp[n] = ans;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 5;
    vector<int> dp(n+1, 0);

    cout<<catelan(n,dp)<<endl;

    return 0;
}    