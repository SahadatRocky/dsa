#include <bits/stdc++.h>
using namespace std;

long long solve(int n){
   
   vector<vector<long long>> p(n+1, vector<long long>(n+1, 1));
   for(int i=1; i<=n; i++){
   	  for(int j=1; j<i; j++){
          p[i][j] = p[i-1][j] + p[i-1][j-1];  
   	  }
   }  

   long long ans =  p[n][5] + p[n][6] + p[n][7];

   return ans; 	
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 7;
    cout<<solve(n)<<endl;

    return 0;
}

/*

Input: n= 7
 
Output: 29

*/