#include<bits/stdc++.h>
using namespace std;

int tillingProblem(int n,int m){
   //base case
   if(n<m){
    return 1;
   }

   return tillingProblem(n-1,m) + tillingProblem(n-m,m);
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n,m;
    cin>>n>>m;

    cout<<tillingProblem(n,m)<<endl;

    return 0;
}


/*
  Sample Input:

4 3

Sample Output:

3

*/