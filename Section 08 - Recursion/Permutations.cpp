#include<bits/stdc++.h>
using namespace std;

void permute(string s,int i){
     
     //base case
     if(i == s.size()){
      cout<<s<<endl;
      return;
     } 

     for(int k=i; k<s.size(); k++){
        swap(s[i],s[k]);
        permute(s,i+1);
        swap(s[i],s[k]);
     }
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    string s;
    cin>>s;
    permute(s,0);

    return 0;
}