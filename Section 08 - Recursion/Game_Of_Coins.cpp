#include<bits/stdc++.h>
using namespace std;
#define ll long long int

int MaxValue(int n, vector<int> v, int s, int e){
    
    //base case
    if(s == e || s == e-1){
        return max(v[s],v[e]);
    }

    //rec case

    int op1 = v[s] + min(MaxValue(n,v,s+2,e), MaxValue(n,v,s+1,e-1));
    int op2 = v[e] + min(MaxValue(n,v,s,e-2),MaxValue(n,v,s+1,e-1));
    
    return max(op1,op2);

}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n = 4;
    
    vector<int> v{1,2,3,4};

    cout<<MaxValue(n,v,0,n-1)<<endl;
    

    return 0;
}

/*
Input:

4

1 2 3 4

Output:

6

Explanation:

Oswald will pick up coin with value 4, Henry will pick coin with value 3, Oswald will pick 2 and Henry will pick 1. Hence 4+2=6.


*/