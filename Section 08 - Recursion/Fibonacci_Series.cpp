#include<bits/stdc++.h>
using namespace std;

int fibonacci(int n){
   
   //base case
   if(n == 0 || n == 1){
      return n;
   }

   return fibonacci(n-1) + fibonacci(n-2);
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n;
    cin>>n; //5

    cout<<fibonacci(n)<<endl;
    return 0;
}
