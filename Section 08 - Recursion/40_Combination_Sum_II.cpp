#include<bits/stdc++.h>
using namespace std;

void solve(vector<int>& candidates,int target,vector<int>& current,vector<vector<int>>& result, int i){
           
    if(target == 0){
        result.push_back(current);
        return;
    }

    for(int k=i; k<candidates.size(); k++){
        if(candidates[k] > target){
            break;
        }

        if(k > i && candidates[k] == candidates[k-1]){
            continue;
        }

        current.push_back(candidates[k]);
        solve(candidates, target-candidates[k], current, result, k);
        current.pop_back();
     }
}


vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
    vector<vector<int>> result;
    vector<int> current;
    sort(candidates.begin(), candidates.end());
    solve(candidates, target, current, result, 0); 
    return result;
    }


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    vector<int> candidates{2,3,6,7};
    int target = 7;
    vector<vector<int>> output = combinationSum(candidates, target);
    
    for(auto v1 : output){
    	for(auto x : v1){
    		cout<<x<<" ";
    	}
    	cout<<endl;
    }

    return 0;
}


/*

Input: candidates = [10,1,2,7,6,1,5], target = 8
Output: 
[
[1,1,6],
[1,2,5],
[1,7],
[2,6]
]


Input: candidates = [2,5,2,1,2], target = 5
Output: 
[
[1,2,2],
[5]
]

*/