#include<bits/stdc++.h>
using namespace std;
#define ll long long int

ll solve(int a,int b,ll mod){
    
    if(a == 0){
        return 0;
    }
    if(b == 0){
        return 1;
    }

    ll y;

    if(b&1){
       //b is odd
       y = ( (a % mod) * solve(a,b-1,mod) %mod) % mod;
    }else{
      //b is even
       y =  solve(a,b/2,mod);
       y = (y*y)%mod;
    } 

    return y;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int a,b;
    cin>>a>>b; // 3 5 

    ll mod = 1e9+7;
    cout<<solve(a,b,mod)<<endl;

    return 0;
}