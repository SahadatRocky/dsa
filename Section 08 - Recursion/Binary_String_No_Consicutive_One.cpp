#include<bits/stdc++.h>
using namespace std;


void generateAllStrings(int n,vector<string>& ans,char str[], int i){
     
     if(i == n){
      str[i] = '\0';
      ans.push_back(str);
      return;
     }

      // If previous character is '1' then we put
      // only 0 at end of string
      //example str = "01" then new string be "010"
     if(str[i-1] == '1'){
        str[i] = '0';  
        generateAllStrings(n,ans,str,i+1);
     }

      // If previous character is '0' then we put
      //both 0 and 1 at the end of the string
      //example str = "00" then new string "000" and "001"
     if(str[i-1] == '0'){
        str[i] = '0';  
        generateAllStrings(n,ans,str,i+1);
        str[i] = '1';  
        generateAllStrings(n,ans,str,i+1);   
     }
}

void generateAllStrings(int n,char str[],vector<string>& ans){
    
    if(n == 0){
      return;
    }


   //generate all binary string start with 0
    str[0] = '0';
    generateAllStrings(n,ans,str,1);
   //generate all binary string start with 1
    str[0]='1';
    generateAllStrings(n,ans,str,1);    
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n;
    cin>>n;
    char str[n];
    vector<string> ans;
    ans.clear();
    generateAllStrings(n,str,ans); 
    
    for(auto x : ans){
       cout<<x<<" "; 
    }
    cout<<endl;

    return 0;
}

/*
  Sample Input:

3

Sample Output:

000
001
010
100
101

*/