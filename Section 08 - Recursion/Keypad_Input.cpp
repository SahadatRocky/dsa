#include<bits/stdc++.h>
using namespace std;

void printKeypadOutput(vector<string>keypad,string input,string output, int i){
    //base case
    if(i == input.size()){
      cout<<output<<endl;
      return;
    }
    
    //rec case
    int current_digit = input[i] - '0';
    if(current_digit == 0 || current_digit == 1){
        printKeypadOutput(keypad,input,output,i+1);
    }

    for(int k=0; k<keypad[current_digit].size(); k++){
        printKeypadOutput(keypad,input,output + keypad[current_digit][k] ,i+1);
    }

}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    
    vector<string> keypad{"", "", "abc", "def", "ghi", "jkl", "mno","pqrs", "tuv", "wxyz"}; 
    string input,output;
    cin>>input;
   
    printKeypadOutput(keypad,input,output,0);

    return 0;
}