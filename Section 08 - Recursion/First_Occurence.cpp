#include<bits/stdc++.h>
using namespace std;

/*
int first_occr(int arr[],int n,int key){
    
    //base case
    if(n == 0){
        return -1;
    }
    
    if(arr[0] == key){
        return 0;
    } 

    int subIndex = first_occr(arr+1, n-1, key);
    if(subIndex != -1){
        return subIndex + 1;
    }

    return -1;
}

*/

int solve(vector<int>& arr,int n,int key, int i){
      
    if(i>=n){
      return -1;
    }

    if(arr[i] == key){
      return 0;
    }

    int index = solve(arr,n,key,i+1);
    if(index != -1){
      return index + 1;
    }
    return -1;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    // int arr[] = {1,3,5,7,6,2,11,21};
    // int n = sizeof(arr)/sizeof(int);
   

    // int f_occr =  first_occr(arr,n,key);
    // f_occr != -1 ? cout<<f_occr<<endl : cout<<"-1"<<endl; 

    vector<int> arr1{1,3,5,7,6,2,7,11,7,21};
    int n = sizeof(arr)/sizeof(int);
    int key = 7;
    int f = solve(arr1,n,key,0);
    cout<<f<<endl; 


    return 0;
}
