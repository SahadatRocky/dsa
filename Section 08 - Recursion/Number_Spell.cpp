#include<bits/stdc++.h>
using namespace std;

string spell[] = {"zero","one","two","three","four","five","six","seven","eight","nine"};

void printSpell(int n){
    
    //base case
    if(n == 0){
        return;
    }
 
    //rec case
    int last_digit = n%10;
    printSpell(n/10);
    cout<<spell[last_digit]<<" ";
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n;
    cin>>n;

     printSpell(n);

    return 0;
}
