#include<bits/stdc++.h>
using namespace std;

 int solve(string haystack,int i,string needle,int  j){
        if(i < haystack.size() && j < needle.size() && haystack[i] == needle[j]){
           return solve(haystack,i+1, needle, j+1); 
        }
        else if(j ==needle.size()){
              return 1;
        }
        return 0;
    }

    int strStr(string haystack, string needle) {
        int n = haystack.size();
        for(int i=0; i < n; i++){
          if(solve(haystack,i, needle, 0)){
              return i;
          }
        }
        return -1;

    }

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    string haystack = "sadbutsad";
    string needle = "sad";
    cout<<strStr(haystack, needle)<<endl; 


    return 0;
}


/*

Input: haystack = "sadbutsad", needle = "sad"
Output: 0
Explanation: "sad" occurs at index 0 and 6.
The first occurrence is at index 0, so we return 0.


Input: haystack = "leetcode", needle = "leeto"
Output: -1
Explanation: "leeto" did not occur in "leetcode", so we return -1.


*/