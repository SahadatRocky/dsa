#include<bits/stdc++.h>
using namespace std;

int power(int a,int n){
    //base case
    if(n == 0){
       return 1; 
    }

    //rec case
    return a * power(a,n-1);

}

int fast_power(int a,int n){


    
   //base case
    if(n == 0){
        return 1;
    }

    //rec case
    int subprob = fast_power(a,n/2);
    int subprobSq = subprob * subprob;

    if(n&1){
        return a * subprobSq;
    }

    return subprobSq;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int a,n;
    cin>>a>>n;

    cout<<power(a,n)<<endl;

    cout<<fast_power(a,n)<<endl;

    return 0;
}


/*
double myPow(double x, int n) {
        if(n == 0){
            return 1;
        }

        double subprob = myPow( x, n/2);
        double subprobsq = subprob * subprob;

        if(n&1){
            if(n<0){
               return subprobsq * (1/x);
            }
            return x * subprobsq;
        }

        return subprobsq;
    }
*