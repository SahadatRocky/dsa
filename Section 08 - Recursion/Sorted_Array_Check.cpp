#include<bits/stdc++.h>
using namespace std;

bool isSorted(int arr[],int n){
     
     //base case
     if(n == 0 || n == 1){
      return true;
     }
     
     //rec case
     if(arr[0] < arr[1] && isSorted(arr+1, n-1)){
       return true; 
     }
     return false;
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int arr[] = {1,2,3,5,4,6};
    int n = sizeof(arr)/sizeof(int);

    isSorted(arr,n) == 0 ? cout<<"false"<<endl : cout<<"true"<<endl;
    return 0;
}
