#include<bits/stdc++.h>
using namespace std;

int countSubSet(vector<int>& arr,int n,int i,int X){
     
     //base case

     if(i == n){
        if(X == 0){
            return 1;
        }
        return 0;
     }

    int include = countSubSet(arr, n, i+1, X - arr[i]);
    int exclude = countSubSet(arr, n, i+1, X );
     
    return include + exclude; 
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    vector<int> arr{1,2,3,4,5};
    int n = arr.size();

    int X = 6;

    cout <<countSubSet(arr,n,0,X) <<endl;  //3 possible subset

    return 0;
}