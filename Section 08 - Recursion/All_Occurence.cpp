#include<bits/stdc++.h>
using namespace std;

vector<int> ans;
void all_occurence(vector<int>& arr,int key,int i){
    //base case
    if(i == arr.size()){
        return; 
    } 
    
    if(arr[i] == key){
        ans.push_back(i);
    }
    
    all_occurence(arr,key,i+1);
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    vector<int> arr{1,3,5,7,6,2,7,11,7,21};
    int key = 7;

    all_occurence(arr,key,0);

    for(auto x : ans){
        cout<<x<<" ";
    }

    return 0;
}
