#include<bits/stdc++.h>
using namespace std;

void findSubset(char input[],char output[], int i,int j){
    
   //base case
   if(input[i] == '\0'){
      output[j] = '\0';
      if(output[0] == '\0'){
        cout<<"NULL";
      }
      cout<<output<<endl;
      return;
   } 

    output[j] = input[i];

    //include 
findSubset(input,output, i+1,j+1);

    //exclude
findSubset(input,output, i+1,j);

}


void findSubset1(string s,string o, vector<string>& v ){
    
    if(s.size() == 0){
        if(o[0] == '\0'){
            v.push_back("NULL");
        }
        v.push_back(o);
        return; 
    }
    
    
    char ch = s[0];
    string reduce_input = s.substr(1); 
 
    findSubset1(reduce_input,o+ch,v);
    findSubset1(reduce_input,o,v); 

}

void findSubset2(string ss,string oo, int i){
     
     if(i == ss.size()){
        if(oo[0] == '\0'){
            cout<<"NULL";
        }
        cout<<oo<<endl;
        return;
     }

     findSubset2(ss,oo+ss[i], i+1);
     findSubset2(ss,oo,i+1);
}
int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    char input[100];
    cin>>input;
    char output[100];

    findSubset(input, output, 0,0); 
    string s = "abc";
    string o;
    vector<string> v;
    findSubset1(s,o,v);

    for(auto x: v){
        cout<<x<< " ";
    }
    cout<<endl;

    string ss = "abc";
    string oo;
    findSubset2(ss,oo,0);

    return 0;
}


/*
input:

abc

output:

abc
ab
ac
a
bc
b
c
NULL

*/