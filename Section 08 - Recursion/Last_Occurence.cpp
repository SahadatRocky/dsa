#include<bits/stdc++.h>
using namespace std;

/*
int last_occr(int arr[],int n,int key){
    
    //base case
    if(n == 0){
        return -1;
    }
    
    int subIndex = last_occr(arr+1, n-1, key);
    if(subIndex != -1){
        return subIndex + 1;
    }
    else{
        if(arr[0] == key){
               return 0;
        }
    }
    return -1;
}

*/

int solve(vector<int>& arr,int n,int key, int i){
     
     if(i>=n){
        return -1;
     }

     int index = solve(arr,n,key,i+1);
     if(index != -1){
        return index + 1;
     }
     else{
        if(arr[i] == key){
            return 0;
        }
     }
     return -1;
}

int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    // int arr[] = {1,3,5,7,6,2,7,11,21};
    // int n = sizeof(arr)/sizeof(int);
    // int key = 7;

    // int l_occr =  last_occr(arr,n,key);
    // l_occr != -1 ? cout<<l_occr<<endl : cout<<"-1"<<endl; 
    
    vector<int> arr{1,3,5,7,6,2,7,11,7,21};
    int n = arr.size();
    int key = 7;

    int l = solve(arr,n,key,0);
    cout<<l<<endl; 

    return 0;
}
