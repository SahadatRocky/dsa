#include<bits/stdc++.h>
using namespace std;

void solve(vector<int>& candidates,int target,vector<int>& current,vector<vector<int>>& result, int i){
           
    if(target == 0){
        result.push_back(current);
        return;
    }

    for(int k=i; k<candidates.size(); k++){
        if(candidates[k] <= target){
            current.push_back(candidates[k]);
            solve(candidates, target-candidates[k], current, result, k);
            current.pop_back(); 
        }
     }
}


vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
    vector<vector<int>> result;
    vector<int> current;
    sort(candidates.begin(), candidates.end());
    solve(candidates, target, current, result, 0); 
    return result;
    }


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
    vector<int> candidates{2,3,6,7};
    int target = 7;
    vector<vector<int>> output = combinationSum(candidates, target);
    
    for(auto v1 : output){
    	for(auto x : v1){
    		cout<<x<<" ";
    	}
    	cout<<endl;
    }

    return 0;
}


/*

Input: candidates = [2,3,6,7], target = 7
Output: [[2,2,3],[7]]
Explanation:
2 and 3 are candidates, and 2 + 2 + 3 = 7. Note that 2 can be used multiple times.
7 is a candidate, and 7 = 7.
These are the only two combinations.


Input: candidates = [2,3,5], target = 8
Output: [[2,2,2,2],[2,3,3],[3,5]]

*/