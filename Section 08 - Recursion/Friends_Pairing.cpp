#include<bits/stdc++.h>
using namespace std;

int friendsPairing(int n){
   
   //base case
    if(n == 0 || n == 1){
        return 1;
    }

    return 1 * friendsPairing(n-1) + (n-1) * friendsPairing(n-2); 
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n;
    cin>>n;

    cout<<friendsPairing(n)<<endl;

    return 0;
}

/*
Sample Input

3
Sample Output

4

*/