#include<bits/stdc++.h>
using namespace std;

int facorial(int n){
   
   //base case
   if(n == 0 || n == 1){
      return 1;
   }

   return n * facorial(n-1);
}


int main(){
     
    freopen("input.txt","r",stdin);
    freopen("output.txt","w", stdout);
     
    int n;
    cin>>n; //5

    cout<<facorial(n)<<endl;  //120
    return 0;
}
