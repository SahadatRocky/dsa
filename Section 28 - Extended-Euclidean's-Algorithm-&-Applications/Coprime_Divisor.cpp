#include <bits/stdc++.h>
using namespace std;

int gcd(int a, int b){
    
    if(a < b){
        swap(a,b);
    }
    
    if(b == 0){
        return a;
    } 
    
    return gcd(b,a%b);
}

int solve(int a,int b){
    
    while(gcd(a,b) != 1){
        a = a / gcd(a,b); 
    }
    
    return a;
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int a,b;
    cin>>a>>b;

    cout<<solve(a,b)<<endl;

    return 0;
}

/*

Input
a= 30, b= 12
 
Output
 5

*/