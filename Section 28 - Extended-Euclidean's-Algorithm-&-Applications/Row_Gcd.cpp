#include <bits/stdc++.h>
using namespace std;

int GCD(int a, int b){
   
   if(b == 0){
   	return a;
   }

   return GCD(b,a%b);
}

vector<int> solve(int n,int m, vector<int> &a, vector<int> &b){
    vector<int> ans(m,0);
    int gcd = 0;

    for(int i = 1; i<n; i++){
    	a[i] = abs(a[i] - a[0]);
    	gcd =  GCD(gcd, a[i]);
    }

    for(int i =0; i<m; i++){
    	ans[i] = GCD(gcd, a[0] + b[i]);
    }
    
    return ans;
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int n = 4, m = 4;
    vector<int> a{1, 25, 121, 169};
    vector<int> b{1, 2, 7, 23};

    vector<int> output = solve(n,m,a,b);
    
    for(auto x: output){
    	cout<<x<<" ";
    }

    return 0;
}

/*

Input: n= 4, m= 4, a= {1, 25, 121, 169}, b= {1, 2, 7, 23}
 
Output: {2, 3, 8, 24}

*/