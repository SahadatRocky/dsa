#include <bits/stdc++.h>
using namespace std;

//Extended Euclid Agorithms  ax + by = GCD(a,b)
vector<int> extendedGCD(int a, int b){
    
    if(b == 0){
    	return {1,0};
    }

    vector<int> result = extendedGCD(b, a%b);

    int smallX = result[0];
    int smallY = result[1]; 

    int x = smallY;
    int y = smallX - (a/b) * smallY;

    return {x,y};
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int a,b;
    cin>>a>>b;

    
    vector<int> result = extendedGCD(a, b);

    cout<<result[0]<<" and "<<result[1]<<endl;



    return 0;
}

/*

input
18 12

output
1 and -1

input
18 30

output
2 and -1

*/