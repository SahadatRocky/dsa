#include <bits/stdc++.h>
using namespace std;

int GCD_II(int a,int b){
    
    if(b == 0){
    	return a;
    }

    return GCD_II(b,a%b);
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int t,a;
    cin>>t;
    while(t--){
       string b;
       cin>>a>>b;

       if(a == 0){
	       	 cout<<b<<endl;
	       	 continue;
       }else{
	       	int rem =0;
	        for(int i=0; i<b.length(); i++){
	          
	          int digit = b[i] - '0';
	          rem = ((rem*10)%a + digit%a)%a;
	       }
	       cout<<GCD_II(a,rem)<<endl;
       }
    }

    return 0;
}


/*

input
1
10 11

output
1

*/