#include <bits/stdc++.h>
using namespace std;

vector<int> extendedGCD(int a,int b){
   
   if(b == 0){
   	   return {1,0,a};
   }

   vector<int> result = extendedGCD(b,a%b);
   int smallX = result[0];
   int smallY = result[1];
   int gcd = result[2];

   int x = smallY;
   int y = smallX - (a/b) * smallY;

   return {x,y,gcd};
}

// multiplicationModuleInversion

int MMI(int a,int m){
    
    vector<int> result = extendedGCD(a,m);
    int x = result[0];
    int gcd = result[2];
    
    if(gcd != 1){
    	cout<<"multiplication module inverse doesn't exist"<<endl;
    	return -1;
    }

    int ans = (x%m + m)%m;
    return ans;
}

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int a,m;
    cin>>a>>m;
    
    cout<<MMI(a,m)<<endl;

    return 0;
}

/*

input 
6 7

output 
6

*/