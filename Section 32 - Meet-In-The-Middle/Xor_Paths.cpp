#include<bits/stdc++.h>
using namespace std;
#define ll long long int
ll n, m, k;

void backtracking(ll i, ll j, ll zor, ll &ans, vector<vector<ll>>& mat)
{
    // If the bottom-right cell is reached
    if (i == n - 1 && j == m - 1) {
         
        // If XOR value is k
        if (zor == k)
            ans++;
        return;
    }
 
    // Move rightwards
    if (j + 1 < m)
        backtracking(i, j + 1,
                     zor ^ mat[i][j + 1],
                     ans, mat);
   
    // Move downwards
    if (i + 1 < n)
        backtracking(i + 1, j,
                     zor ^ mat[i + 1][j],
                     ans, mat);
}
 
// Function to calculate all possible paths
ll countPaths(int N, int M, ll K,  vector<vector<ll>>& mat)
{
    ll ans = 0;
    n = N; m = M; k = K;
    if (N == 1 && M == 1
        && mat[0][0] == K) {
        return 1;
    }
   
    // Calling the backtracking function
    backtracking(0, 0, mat[0][0], ans, mat);
    return ans;
}
ll solve(int N, int M, ll K, vector<vector<ll>> mat ){
    ll ans = countPaths(N, M, K, mat);
    return ans;
}

int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int N= 3, M= 3, K= 11;
    vector<vector<ll>> mat{
        {2, 1, 5},
        {7, 10, 0},
        {12, 6, 4}
    };

    cout<<solve(N,M,K, mat)<<endl;

    return 0; 
}

/*

Input

n= 3, m= 3, k= 11, a= [[2, 1, 5], [7, 10, 0], [12, 6, 4]]
Output

3
Explanation

All the paths from the example:

(1,1)→(2,1)→(3,1)→(3,2)→(3,3);

(1,1)→(2,1)→(2,2)→(2,3)→(3,3);

(1,1)→(1,2)→(2,2)→(3,2)→(3,3).
*/