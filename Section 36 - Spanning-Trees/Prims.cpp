#include<bits/stdc++.h>
using namespace std;

vector<pair<int,int>> gr[100];
int N,E;
int visited[100] = {0};
int prim_mst(){
		//most important stuff
		//Init a Min Heap
		priority_queue<pair<int,int>, vector<pair<int,int> > , greater<pair<int,int> > > Q; 
		int ans = 0;

		//begin 
		Q.push({0,0}); // weight, node

		while(!Q.empty()){
			//pick out the edge with min weight
			auto best = Q.top();
			Q.pop();

			int node = best.second;
			int weight = best.first;

			if(visited[node]){
				//discard the edge, and continue
				continue;
			}

			//otherwise take the current edge
			ans += weight;
			visited[node] = 1;

			//add the new edges in the queue
			for(auto nbrPair : gr[node]){
				if(visited[nbrPair.first]== 0){
					Q.push({nbrPair.second,nbrPair.first});
				}
			}
		}
		return ans;
	}

int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    cin>>N>>E;
    
    int u,v,w;
    for(int i=0; i<E; i++){
       cin>>u>>v>>w;
       gr[u].push_back(make_pair(v,w));
       gr[v].push_back(make_pair(u,w));
    }

    cout<<prim_mst()<<endl;
    return 0; 
}

/*

input:
4 6
0 1 1
0 2 2
0 3 2
1 3 3
2 3 3
1 2 2

output:
5

*/