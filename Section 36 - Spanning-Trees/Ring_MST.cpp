#include<bits/stdc++.h>
using namespace std;

bool comp(const vector<int>&a, const vector<int>&b){
    return a[1]<b[1];
}

long long solve(int n,int m,vector<vector<int>>& operation){
    sort(operation.begin(),operation.end(),comp);
    vector<int>component(m+1);
    component[0]=n;
    int val=0;
    for(int i=0;i<m;i++){
        val=__gcd(component[i],operation[i][0]);
        component[i+1]=val;
    }
    if(component[m]!=1){
        return -1;
    }
    long long ans=0;
    for(int i=1;i<=m;i++){
        ans+=(long long)(component[i-1]-component[i])*(long long)operation[i-1][1];
    }
    return ans;
}

int main(){
    
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n = 4, m = 2;
    vector<vector<int>> operation{
        {2,3},
        {3,5}
    };
    cout<<solve(n,m,operation)<<endl;

    return 0; 
}

/*

Input: n= 4, m= 2, operation= [[2,3], [3,5]]
Output: 11
 
Explanation: 
If we first do the operation of the first kind to connect Vertices 0 and 2, then do it again to connect Vertices 1 and 3, and finally the  operation of the second kind to connect Vertices 1 and 0, the graph will be connected. The total cost incurred here is 3+3+5=11 yen, which is the minimum possible.


*/