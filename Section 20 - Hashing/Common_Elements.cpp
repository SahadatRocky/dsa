#include<bits/stdc++.h>
using namespace std;

vector<int> commonElements(vector<int>& v1,vector<int>& v2){
    
    sort(v1.begin(), v1.end());
    sort(v2.begin(), v2.end());
    vector<int> v;
    unordered_map<int, bool> map;

    for(auto x : v1){
        map[x] = true;
    }

    for(auto x : v2){
       if(map[x] == true){
          v.push_back(x);
       }
    }

    return v;
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> v1{1, 45, 54, 71, 76, 12};
    vector<int> v2{1, 7, 5, 4, 6, 12};
    
    vector<int> output = commonElements(v1, v2); 
    
    for(auto x: output){
    	cout<<x<<" ";
    }	
    cout<<endl;
  
	return 0;
}
/*

Input:

v1[] = {1, 45, 54, 71, 76, 12},

v2[] = {1, 7, 5, 4, 6, 12}

Output:

{1, 12}

*/