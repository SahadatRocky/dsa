#include<bits/stdc++.h>
using namespace std;

vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        
    map<int,int> mp;
    vector<int> v;
    
    for(int i=0; i<nums1.size(); i++){
        mp[nums1[i]]++;
    }
    
    for(int i=0; i<nums2.size(); i++){
        if(mp[nums2[i]] > 0){
           v.push_back(nums2[i]);
           mp[nums2[i]] = 0;
        }
    }
    sort(v.begin(), v.end());
    return v;
    
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> nums1{1,2,2,1};
    vector<int> nums2{2,2};

    vector<int> output = intersection( nums1, nums2);
    
    for(auto x : output){
        cout<<x<<" ";
    }
    cout<<endl;

	return 0;
}
/*

Sample Input

Input: nums1 = [1,2,2,1], nums2 = [2,2]

Sample Output

Output: [2]


*/