#include<bits/stdc++.h>
using namespace std;

int longestSubarrayKSum(vector<int>& arr,int k){
   map<int,int> mp;
   int n = arr.size(); 
   int currentSum = 0;
   int ans = 0;

   for(int i=0; i<n; i++){
       currentSum += arr[i];

       if(currentSum == k){
           ans = max(ans, i+1);
       }
       else if(mp.find(currentSum - k) != mp.end()){
            ans = max(ans, i - mp[currentSum - k]); 
       }else{
          mp[currentSum] = i; 
       }
   }

	return ans;
}


int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    vector<int> arr{0,-2,1,2,3,4,5,15,10,5};
    int k = 15;
    cout<<longestSubarrayKSum(arr,k)<<endl;
    
	return 0;
}
/*

Sample Input

arr = { 0,-2,1,2,3,4,5,15,10,5 }

K = 15

Sample output

5

Explanation

The following subarray has the sum 15 and is the longest.

1,2,3,4,5

*/