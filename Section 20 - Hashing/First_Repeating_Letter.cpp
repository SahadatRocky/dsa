#include<bits/stdc++.h>
using namespace std;


char firstRepeatChar(string input){
    
    unordered_map<char,bool> m;
    for(char ch : input){
    	
        if(m.count(ch)==0){
            m[ch] = 1;
        }
        else{
            return ch;
        }
    }

    return '\0';
}

int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    string s = "codingminutes";
    cout<<firstRepeatChar(s)<<endl;
  
	return 0;
}
/*

Input:

codingminutes

Output:

i


*/