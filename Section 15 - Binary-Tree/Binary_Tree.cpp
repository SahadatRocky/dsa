#include<bits/stdc++.h>
using namespace std;


 
class node{

public:
    int data;
    node* left;
    node* right;

    node(int data){
       this->data = data;
       this->left = NULL;
       this->right = NULL;
    } 
};

int idx = -1;

node* buildTree(int nodes[]){
   idx++;
   if(nodes[idx] == -1){
    return NULL;
   }

   node* newNode = new node(nodes[idx]);
   newNode->left = buildTree(nodes);
   newNode->right = buildTree(nodes);
   
   return newNode;
}

void inOrder(node* root){
    if(root == NULL){
        return;
    }

    inOrder(root->left);
    cout<<root->data<<" ";
    inOrder(root->right);
}

void preOrder(node* root){
   if(root == NULL){
        return;
    }
    cout<<root->data<<" ";
    preOrder(root->left);
    preOrder(root->right);
}

void postOrder(node* root){
   if(root == NULL){
        return;
    }
    
    postOrder(root->left);
    postOrder(root->right);
    cout<<root->data<<" ";
}

void levelOrderPrint(node* root){
    
    queue<node*> q;
    q.push(root);
    q.push(NULL);

    while(!q.empty()){
        node* temp = q.front();
        if(temp == NULL){
            cout<<endl;
            q.pop();

            //insert a new null for the next level
            if(!q.empty()){
                q.push(NULL);
            }
        }else{
            cout<<temp->data<<" ";
            q.pop();

            if(temp->left){
                q.push(temp->left);
            }
            
            if(temp->right){
                q.push(temp->right);
            }
        }
    }
    return;
}

int height(node* root){
    if(root == NULL){
        return 0;
    }

    int h1 = height(root->left);
    int h2 = height(root->right);

    return max(h1,h2) + 1;
}

int diameter(node* root){
   if(root == NULL){
      return 0;
   }
   int D1 = height(root->left) + height(root->right);
   int D2 = diameter(root->left);
   int D3 = diameter(root->right);

   return max(D1, max(D2,D3));   
}


vector<int> printKthLevel(node* root, int k){
    
    queue<node*> q;
    q.push(root);

    node *temp;
    vector<int> v;
    int level = 0;
    int flag = 0;
    
    while(!q.empty()){
       
       int size = q.size();
       
       while(size--){
           temp = q.front();
           q.pop();
           
           if(level == k){
               
               flag = 1;
               v.push_back(temp->data);
           }
           else{
               if(temp->left){
                   q.push(temp->left);
               }
               if(temp->right){
                   q.push(temp->right);
               }
               
           }
       }
       
       level++;
        
        if(flag == 1){
            break;
        }
        
    }
    return v;
}

int sumBT(node* root)
{
    int res = 0;
    queue<node*> q;
    q.push(root);
    node* temp;
    while(!q.empty()){
         
         temp = q.front();
         q.pop();
         res += temp->data;
         
         if(temp->left){
            q.push(temp->left); 
         }
         if(temp->right){
             q.push(temp->right);
         }
    }
    
    return res;
}

int minDepth(node *root) {
       
    if(root == NULL){
           return 0;
    }
       
    if(root->left == NULL &&  root->right == NULL){
           return 1;
    }
    else if(root->left == NULL){
           return 1 + minDepth(root->right);
    }
    else if(root->right == NULL){
           return 1 + minDepth(root->left);
    }
    return 1 + min(minDepth(root->left),minDepth(root->right));    
}

bool checkSymmetricTree(node* root1,node* root2){
        
    if(root1 == NULL && root2 == NULL){
            return true;
    }
    else if( (root1 && root2 == NULL) || (root1 == NULL && root2) ){
            return false;
    }
    else{
        if(root1->data != root2->data){
            return false;
        }
            
    }
        
    return (checkSymmetricTree(root1->left, root2->right) && checkSymmetricTree(root1->right, root2->left)); 
}

bool isSymmetric(node* root) {
        
        if(root->left == NULL && root->right == NULL){
            return true;
        }
        return checkSymmetricTree(root->left, root->right);
        
}

//Expression Tree

// bool isOp(string data)
// {
//     if(data == "+" or data == "-" or data == "*" or data == "/")
//         return true;
//     return false;
// }
// int evalTree(node* root){
//     if(root == NULL) return 0;
//     if(!isOp(root->data)) return stoi(root->data);
    
//     if(root->data == "+") return evalTree(root->left)+evalTree(root->right);
//     if(root->data == "-") return evalTree(root->left)-evalTree(root->right);
//     if(root->data == "*") return evalTree(root->left)*evalTree(root->right);
//     if(root->data == "/") return evalTree(root->left)/evalTree(root->right);
// }


////path Sum

vector<vector<int>> vv;
void help(node* root, int a, vector<int> &v, int b)
{
    if(root == NULL) return;
    if(root->left == NULL && root->right == NULL)
    {
        
        if(a == b+root->data) 
        {
            v.push_back(root->data);
            vv.push_back(v);
            v.pop_back();
        }
        return;
    }
    if(root->left)
    {
        v.push_back(root->data);
        help(root->left, a, v, b+root->data);
        v.pop_back();
    }
    if(root->right)
    {
        v.push_back(root->data);
        help(root->right, a, v, b+root->data);
        v.pop_back();
    }
}
vector<vector<int>> pathSum(node* root, int targetSum) {
    vv.clear();
    vector<int> v;
    help(root, targetSum, v, 0);
    return vv;
}


// Replace with Descendant sum leaving leaf nodes intact

int replaceWithSum(node* root){
    //base case
    if(root == NULL){
        return 0;
    }
    
    if(root->left == NULL && root->right == NULL){
        return root->data;
    }

    //rec case
    
    int LS =  replaceWithSum(root->left);
    int RS =  replaceWithSum(root->right);

    int temp = root->data;
    root->data = LS + RS;
    
    return root->data + temp;
}



int main(){

    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);
    
    int nodes[] = {1, 2, 4, -1, -1, 5, 7, -1, -1, -1, 3, -1, 6, -1, -1};
    
    node *root = buildTree(nodes);
    cout<<"inOrder : ";
    inOrder(root);
    cout<<endl;
    cout<<"preOrder : ";
    preOrder(root);
    cout<<endl;
    cout<<"postOrder : ";
    postOrder(root);
    cout<<endl;
    cout<<"levelOrder : "<<endl;
    levelOrderPrint(root);
    cout<<endl;
    cout<<"height : "<<height(root)<<endl;
    cout<<endl;
    cout<<"diameter : "<<diameter(root)<<endl;
    cout<<endl;
    cout<<"replaceSum : "<<endl;
    replaceWithSum(root);
    cout<<endl;
    levelOrderPrint(root);
    return 0;
}

/*
input:
 
1 2 4 -1 -1 5 7 -1 -1 -1 3 -1 6 -1 -1

*/