#include<bits/stdc++.h>
using namespace std;

#define ll long long int
int solve(int n,int m, vector<int>&a){
    ll ans = 1;
    if(n>m)ans=0;
    else
    for(int i=0; i<n; i++)
    {
        for(int j=0; j<i; j++)  ans*=abs(a[i]- a[j] )%m, ans%=m;
    }


   return ans;
}


int main() {

    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n = 2,m = 10;
    vector<int> a{8, 5};
    cout<<solve(n,m, a)<<endl;
   
    return 0;
} 


/*

Input: n= 2, m= 10, a= [8, 5]
 
Output: 3

*/