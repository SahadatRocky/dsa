
#include<bits/stdc++.h>
using namespace std;

int solve(int n, vector<int> &nums){
    vector<int>mod(n);
    mod[0]++;
    int sum=0;
    for(int i=0;i<n;i++){
        sum+=nums[i];
        mod[sum%n]++;
    }
    int ans=0;
    for(int i=0;i<n;i++){
        int x=mod[i];
        ans+=(x*(x-1))/2;
    }
    return ans;
}


int main() {

    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int n = 5;
    vector<int> nums{4, 5, 0, 4, 2};
    cout<<solve(n,nums)<<endl;
   
    return 0;
}    

/*
Input: n= 5, nums= [4, 5, 0, 4, 2]
 
Output: 4

*/