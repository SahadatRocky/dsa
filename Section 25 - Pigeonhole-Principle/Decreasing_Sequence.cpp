#include<bits/stdc++.h>
using namespace std;

int solve(int L,int R){
    if(R-L+1 > L){
        return -1;
    }
    return R;
}


int main(){
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    
    int L = 4,R = 6;
    cout<<solve(L,R)<<endl;
    
    return 0;
}

/*

Input: L= 4 , R= 6
 
Output: 6
 
Explanation:
N=6 satisfies the given condition, since 6%4(=2)>6%5(=1)>6%6(=0). Notice that N=7 also satisfies the condition, but it is larger.
Example 2:

Input: L= 1 , R= 2
 
Output: -1
 
Explanation:
It is impossible to find a valid solution because for any non-negative integer N

*/